<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('l_name')->nullable();
            $table->string('email')->unique();
            $table->string('email2')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('phone')->nullable();
            $table->string('phone2')->nullable();
            $table->text('industry')->nullable();
            $table->text('c_name')->nullable();
            $table->text('e_size')->nullable();
            $table->text('company_id')->nullable();
            $table->text('facebook_id')->nullable();
            $table->text('google_id')->nullable();
            $table->text('country')->nullable();
            $table->text('website')->nullable();
            $table->text('logo')->nullable();
            $table->text('revenue')->nullable();
            $table->integer('is_first')->default(0);
            $table->text('address')->nullable();
            $table->text('email_verify')->nullable();
            $table->text('role')->nullable();
            $table->integer('sidebar')->nullable();
            $table->text('main_id')->nullable();
            $table->text('modules')->nullable();
            $table->integer('branch_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('unit_id')->nullable();
            $table->text('image')->nullable();
            $table->text('branch_head')->nullable();
            $table->text('department_head')->nullable();
            $table->text('unit_head')->nullable();
            $table->text('role_id')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->timestamp('last_activity')->nullable();
            $table->timestamp('otp_time')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
