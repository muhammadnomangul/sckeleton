<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
// Company Controller

use App\Http\Controllers\company\CompanyController;
use App\Http\Controllers\company\ClaimController;
use App\Http\Controllers\company\MarketplaceController;
use App\Http\Controllers\company\UserController;

// Employee Controller

use App\Http\Controllers\employee\ClaimController as Claim;
use App\Http\Controllers\employee\ProfileController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/',function(){
  return view('index');
});

Route::get('/check/user', [ForgotPasswordController::class,'checkuser'])->name('check.user');

Route::get('/admin/login', [LoginController::class,'showLoginForm'])->name('admin.login');
Route::post('admin/login', [LoginController::class, 'Adminlogin'])->name('admin.login');
Route::get('/admin/password/reset', [ForgotPasswordController::class,'showLinkRequestForm'])->name('admin.forget');

Route::get('/company/register', [RegisterController::class,'showRegistrationForm'])->name('company.register');
Route::post('/create_company', [CompanyController::class,'create_company'])->name('create.company');
Route::get('/company_verify', [CompanyController::class,'company_verify'])->name('company.verify.email');
Route::get('/company/get_start', [CompanyController::class,'get_start'])->name('get_started');

Route::get('/company/login', [LoginController::class,'showLoginForm'])->name('company.login');
Route::post('company/login', [LoginController::class, 'Companylogin'])->name('Company.login');
Route::get('/company/password/reset', [ForgotPasswordController::class,'showLinkRequestForm'])->name('company.forget');


Route::get('/user/register', [RegisterController::class,'showRegistrationForm'])->name('user.register');
Route::get('/user/login', [LoginController::class,'showLoginForm'])->name('user.login');
Route::post('user/login', [LoginController::class, 'Userlogin'])->name('user.login');
Route::get('/user/password/reset', [ForgotPasswordController::class,'showLinkRequestForm'])->name('user.forget');


Route::get('verify', [HomeController::class,'verify'])->name('verify');
Route::post('verify/email', [HomeController::class,'verify_email'])->name('verify_email');


Route::group(['middleware'=>['auth','admin'],'prefix' => 'admin','as' => 'admin.'],function() {


  Route::get('/dashboard',[AdminController::class,'dashboard'])->name('dashboard');
  Route::get('/approved/claim',[AdminController::class,'approve_claim'])->name('approve_claim');
  Route::get('/claim/type',[AdminController::class,'claim_type'])->name('claim_type');
  Route::get('/manage/claim',[AdminController::class,'manage_claim'])->name('manage_claim');
  Route::get('/chart/data', [AdminController::class ,'chart_data'])->name('chart_data');
  Route::get('/custom/data', [AdminController::class ,'custom_data'])->name('custom_data');

});


// Company Routes
Route::group(['middleware'=>['auth','company'],'prefix' => 'company','as' => 'company.'],function() {
  Route::get('/dashboard',[CompanyController::class,'dashboard'])->name('dashboard');

  // Claims
  Route::get('/setup/claim',[ClaimController::class,'setup_claim'])->name('setup_claim');
  Route::post('/create/claim',[ClaimController::class,'create_claim'])->name('create_claim');
  Route::get('/claim/edit/{id}',[ClaimController::class,'edit_claim'])->name('edit_claim');
  Route::post('/claim/update/{id}',[ClaimController::class,'update_claim'])->name('update_claim');
  Route::get('/claim/delete/{id}',[ClaimController::class,'delete_claim'])->name('delete_claim');


  // Claimed Claim
  Route::get('/manage/claim', [ClaimController::class ,'manage_claim'])->name('manage_claim');
  Route::get('/employee/claim/delete/{id}', [ClaimController::class ,'delete_employee_claim'])->name('delete_employee_claim');
  Route::get('/employee/claim/edit/{id}', [ClaimController::class ,'edit_employee_claim'])->name('edit_employee_claim');
  Route::post('/employee/claim/update/{id}', [ClaimController::class ,'update_employee_claim'])->name('update_employee_claim');

  // Market Place
  Route::get('/marletplace', [MarketplaceController::class ,'index'])->name('market');
  Route::get('/chart/data', [CompanyController::class ,'chart_data'])->name('chart_data');

  // User

  Route::get('/employee',[UserController::class , 'index'])->name('user');
  Route::post('/create/employee',[UserController::class , 'insert'])->name('create.employee');

});


// Employee Route
Route::group(['middleware'=>['auth','employee'],'prefix' => 'employee','as' => 'employee.'],function() {

  //Employee Claims
  Route::get('/claim', [Claim::class ,'claim'])->name('claim');
  Route::post('/make/claim', [Claim::class ,'make_claim'])->name('make_claim');


  // Employee Claimed Claim
  Route::post('/claim/update/{id}', [Claim::class ,'update_claim'])->name('update_claim');
  Route::get('/claim/history', [Claim::class ,'claim_history'])->name('claim_history');
  Route::get('/claim/delete/{id}', [Claim::class ,'delete_claim'])->name('delete_claim');
  Route::get('/claim/edit/{id}', [Claim::class ,'edit_claim'])->name('edit_claim');

// Profile
  Route::get('profile', [ProfileController::class ,'index'])->name('profile');

});
// End Employee RouteServiceProvider    

Auth::routes();

Route::get('/migrate-database', function () {
    // Run the database migration
    Artisan::call('config:clear');
    return 'Database migration completed.';
});