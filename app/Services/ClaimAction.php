<?php

namespace App\Services;

use Carbon\Carbon; 
use Illuminate\Http\Request;
use Auth;
use App\Models\ApprovalType;
use Illuminate\Support\Collection;

class ClaimAction
{
    public function processClaims($claims)
    {
        $processedClaims = [];
        $type = ApprovalType::where('company_id',id())->first();
        foreach ($claims as $claim) {
            if ($claim->type == 'head') {
                $claim->processed = $claim->action_by !== null ? $claim->action_by : 'No Action';
            } elseif ($claim->status == "declined" || $claim->status == "approved") {
                $claim->processed = $claim->action_by;
            } elseif ($type->type == 'single') {
                if (Auth::user()->role == 'company_admin') {
                    $claim->processed = $claim->action_by ?? 'Awaiting Supervisor Confirmation or dispute';
                } else {
                    $claim->processed = $claim->action_by ?? (
                        ($claim->type == 'branch' && $claim->branch_head == Auth::user()->main_id) ||
                        ($claim->type == 'department' && $claim->depart_head == Auth::user()->main_id) ||
                        ($claim->type == 'unit' && $claim->unit_head == Auth::user()->main_id) ? 'no action' : 'Awaiting Supervisor Confirmation or dispute'
                    );
                }
            } else {
                if ($claim->type == 'branch') {
                    if (Auth::user()->role == 'company_admin') {
                        $claim->processed = $claim->branch !== null ? $claim->action_by : 'Branch Head Confirmation Needed';
                    } else {
                        $claim->processed = $claim->action_by !== null ? $claim->action_by : 'No Action';
                    }
                } elseif ($claim->type == 'department') {
                    if (Auth::user()->role == 'company_admin') {
                        $claim->processed = ($claim->branch !== null && $claim->department !== null) ? $claim->action_by : (
                            ($claim->branch == null && $claim->department !== null) ? $claim->action_by . '(Branch Head Confirmation Needed)' : 'Branch and Department Head Confirmation Needed'
                        );
                    } else {
                        $claim->processed = ($claim->branch_head == Auth::user()->main_id && $claim->department) ? $claim->action_by : (
                            ($claim->branch_head == Auth::user()->main_id && !$claim->department) ? 'Department Head Confirmation Needed' : (
                                ($claim->depart_head == Auth::user()->main_id && $claim->department) ? $claim->action_by : 'No Action'
                            )
                        );
                    }
                } elseif ($claim->type == 'unit') {
                    if (Auth::user()->role == 'company_admin') {
                        $claim->processed = ($claim->branch !== null && $claim->department !== null && $claim->unit !== null) ? $claim->action_by : (
                            ($claim->branch == null && $claim->department == null && $claim->unit == null) ? 'Branch, Department and Unit Head Confirmation Needed ' : (
                                ($claim->branch == null && $claim->department == null && $claim->unit !== null) ? $claim->action_by . ' (Branch and Department Head Confirmation Needed) ' : (
                                    ($claim->branch == null && $claim->department !== null && $claim->unit !== null) ? $claim->action_by . ' (Branch Head Confirmation Needed) ' : 'Branch Head Confirmation Needed'
                                )
                            )
                        );
                    } else {
                        $claim->processed = ($claim->branch_head == Auth::user()->main_id && $claim->department !== null && $claim->unit !== null) ? $claim->action_by : (
                            ($claim->branch_head == Auth::user()->main_id && $claim->department == null && $claim->unit !== null) ? $claim->action_by . '(Department Head Confirmation Needed)' : (
                                ($claim->branch_head == Auth::user()->main_id && $claim->department == null && $claim->unit == null) ? 'Department and Unit Head Confirmation Needed' : (
                                    ($claim->depart_head == Auth::user()->main_id && $claim->unit == null) ? 'Unit Head Confirmation Needed' : (
                                        ($claim->depart_head == Auth::user()->main_id && $claim->unit != null) ? $claim->action_by : (
                                            ($claim->unit_head == Auth::user()->main_id && $claim->unit !== null) ? $claim->action_by : 'No Action'
                                        )
                                    )
                                )
                            )
                        );
                    }
                }
            }
            $processedClaims[] = $claim;
        }
        $processedClaimsCollection = $this->checkClaimStatus($processedClaims, $type);
        $processedClaimsCollection = $this->checkClaimStatusAndRenderDropdown($type , $processedClaimsCollection);
        $processedClaimsCollection = collect($processedClaimsCollection);
        return $processedClaimsCollection;
    }
    public function checkClaimStatus($claims,$type)
    {
        $statuscheckedClaim = [];
        foreach($claims as $claim){
            if ($type->type == 'multi' &&
                (
                    (Auth::user()->role == "company_admin" && $claim->branch !== null && $claim->approval == 0) ||
                    (Auth::user()->role == "company_admin" && $claim->type == 'head' && $claim->approval == 0) ||
                    (Auth::user()->main_id == $claim->branch_head && $claim->branch == null && $claim->type == "branch") ||
                    (Auth::user()->main_id == $claim->branch_head && $claim->branch == null && $claim->type == "department" && $claim->department !== null) ||
                    (Auth::user()->main_id == $claim->branch_head && $claim->branch == null && $claim->type == "unit" && $claim->department !== null && $claim->unit !== null) ||
                    ($claim->type == "department" && Auth::user()->main_id == $claim->depart_head && $claim->department == null) ||
                    ($claim->type == "unit" && Auth::user()->main_id == $claim->depart_head && $claim->department == null && $claim->unit != null) ||
                    (Auth::user()->main_id == $claim->unit_head && $claim->unit == null)
                )
            ) {
                $claim->newStatus = 'Action';
            }
            elseif ($type->type == 'single' && $claim->approval == 0 && Auth::user()->role == "company_admin" &&
                (
                    ($claim->type == 'department' && $claim->department !== null) ||
                    ($claim->type == 'branch' && $claim->branch !== null) ||
                    ($claim->type == 'unit' && $claim->unit !== null)
                )
            ) {
             $claim->newStatus = 'Action';
     }
     elseif ($type->type == 'single' && Auth::user()->role !== "company_admin" &&
        (
            ($claim->type == 'department' && $claim->department == null && $claim->depart_head == Auth::user()->main_id) ||
            ($claim->type == 'branch' && $claim->branch == null && $claim->branch_head == Auth::user()->main_id) ||
            ($claim->type == 'unit' && $claim->unit == null && $claim->unit_head == Auth::user()->main_id)
        )
    ) {
        $claim->newStatus = 'Action';
}
elseif ($claim->status == "declined") {
 $claim->newStatus = 'Dispute';
} elseif ($claim->status == 'approved') {
 $claim->newStatus = 'Approved';
} elseif ($claim->status == 'pending') {
    if ((Auth::user()->main_id == $claim->branch_head && $claim->branch) ||
        (Auth::user()->main_id == $claim->depart_head && $claim->department) ||
        (Auth::user()->main_id == $claim->unit_head && $claim->unit)
    ) {
        $claim->newStatus = 'Pending';
} else {
    $claim->newStatus = 'Pending';
}
}
$statuscheckedClaim[] = $claim;
}
return $statuscheckedClaim;
}
public function checkClaimStatusAndRenderDropdown($type, $claims)
{
    $confirmButton = [];
    foreach($claims as $claim){
        if ($type->type == 'multi' &&
            (
                (Auth::user()->role == "company_admin" && $claim->type == 'head') ||
                (Auth::user()->role == "company_admin" && $claim->branch !== null) ||
                (Auth::user()->main_id == $claim->branch_head && $claim->branch == null && $claim->type == "branch") ||
                (Auth::user()->main_id == $claim->branch_head && $claim->branch == null && $claim->type == "department" && $claim->department !== null) ||
                (Auth::user()->main_id == $claim->branch_head && $claim->branch == null && $claim->type == "unit" && $claim->department !== null && $claim->unit !== null) ||
                ($claim->type == "department" && Auth::user()->main_id == $claim->depart_head && $claim->department == null) ||
                ($claim->type == "unit" && Auth::user()->main_id == $claim->depart_head && $claim->department == null && $claim->unit != null) ||
                (Auth::user()->main_id == $claim->unit_head && $claim->unit == null)
            )
        ) {
           $claim->confirm =  true;
       }
       elseif ($type->type == 'single' && Auth::user()->role == "company_admin" &&
        (
            ($claim->type == 'head') ||
            ($claim->branch !== null && $claim->type == 'branch') ||
            ($claim->department !== null && $claim->type == 'department') ||
            ($claim->unit !== null && $claim->type == 'unit')
        )
    ) {
        $claim->confirm =  true;
}
elseif ($type->type == 'single' && Auth::user()->role !== "company_admin" &&
    (
        ($claim->branch_head == Auth::user()->main_id && $claim->type == 'branch' && $claim->branch == null) ||
        ($claim->depart_head == Auth::user()->main_id && $claim->type == 'department' && $claim->department == null) ||
        ($claim->unit_head == Auth::user()->main_id && $claim->type == 'unit'  && $claim->unit == null)
    )
) {
    $claim->confirm =  true;
}else{
    $claim->confirm =  false;
}
$confirmButton[] = $claim;
}
return $confirmButton;
}

}
