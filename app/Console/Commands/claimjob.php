<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Claim;
use App\Models\EmployeeClaim;
use App\Models\User;
use App\Models\Notification;
use App\Models\Window;
use App\Services\PaymentDeduct;
use Illuminate\Support\Facades\Log;
use App\Mail\ClaimMail;
use App\Mail\ClaimStart;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Mail\job;
class claimjob extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */ 
    protected $signature = 'claim:claim-cron-job';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will send monthly claim mail to user';

    /**
     * Execute the console command.
     */ 
    public function handle(): void
    {
        $yesterday = Carbon::yesterday()->toDateString();
        $companies = Window::where('end', $yesterday)->pluck('company_id');
        foreach($companies as $company){
            $companyAdmin = User::find($company);
            $companyData = companyModule($companyAdmin->company_id);
            $claims = Claim::where('company_id', $company)->pluck('id');
            EmployeeClaim::whereIn('claim_id',$claims)->update(['is_send'=>1]);
            $userID = [];
            foreach($companyData['branches'] as $branch){
                foreach($companyData['users'] as $branchuser){
                    if($branchuser['id'] == $branch['user_id']){
                        if(!in_array($branchuser['id'], $userID)){
                         $subject = "Claim Window Ends";
                         $notification = new Notification;
                         $notification->content ="Claim Window has been ended.";
                         $notification->status = "pending";
                         $notification->user_id = $branchuser['id'];
                         $notification->save();
                         $link = $branchuser['type'] == 'company_employee' ? route('employee.manage.claim',['id'=>encrypt($branchuser['id'])]) : route('company.login');
                         $content = [
                            'name' => $branchuser['name'] .' '. $branchuser['last_name'],
                            'link'=> $link,
                        ];
                        Mail::to($branchuser['email'])->send(new ClaimMail($subject,$content));
                        $userID[] = $branchuser['id'];
                    }
                }
            }
        }
        foreach($companyData['departments'] as $department){
            foreach($companyData['users'] as $departmentuser){
                if($departmentuser['id'] == $department['user_id']){
                    if(!in_array($departmentuser['id'], $userID)){
                     $subject = "Claim Window Ends";
                     $notification = new Notification;
                     $notification->content ="Claim Window has been ended.";
                     $notification->status = "pending";
                     $notification->user_id = $departmentuser['id'];
                     $notification->save();
                     $link = $departmentuser['type'] == 'company_employee' ? route('employee.manage.claim',['id'=>encrypt($departmentuser['id'])]) : route('company.login');
                     $content = [
                        'name' => $departmentuser['name'] .' '. $departmentuser['last_name'],
                        'link'=> $link,
                    ];
                    Mail::to($departmentuser['email'])->send(new ClaimMail($subject,$content));
                    $userID[] = $departmentuser['id'];
                }
            }
        }
    }
    foreach($companyData['units'] as $unit){
        foreach($companyData['users'] as $unituser){
            if($unituser['id'] == $unit['user_id']){
                if(!in_array($unituser['id'], $userID)){
                 $subject = "Claim Window Ends";
                 $notification = new Notification;
                 $notification->content ="Claim Window has been ended.";
                 $notification->status = "pending";
                 $notification->user_id = $unituser['id'];
                 $notification->save();
                 $link = $unituser['type'] == 'company_employee' ? route('employee.manage.claim',['id'=>encrypt($unituser['id'])]) : route('company.login');
                 $mail = [
                    'name' => $unituser['name'] .' '. $unituser['last_name'],
                    'link'=> $link,
                ];
                Mail::to($unituser['email'])->send(new ClaimMail($subject,$mail));
                $userID[] = $unituser['id'];
            }
        }
    }
}
}

$tomorrow = Carbon::tomorrow()->toDateString();
$claimStarts = Window::where('start', $tomorrow)->pluck('company_id');
$subject = 'It is 24 hours to the start of the claim window Opening'; 
foreach($claimStarts as $claimStart){
    $companyAdmin = User::find($claimStart);
    $users = companyModule($companyAdmin->company_id);
    foreach ($users['users'] as $user) {
        if ($user['type'] === "company_employee") {
            $notification = new Notification;
            $notification->content ="Claim Window will Start in 24 Hours";
            $notification->status = "pending";
            $notification->user_id = $user['id'];
            $notification->save();
            $name =  $user['name'].' '. $user['last_name'];
            Mail::to($user['email'])->send(new ClaimStart($subject,$name));
        }
    }
    $userID = [];
    foreach($users['branches'] as $branch){
        foreach($users['users'] as $branchuser){
            if($branchuser['id'] == $branch['user_id'] && $branchuser['type'] == "company_user"){
                if(!in_array($branchuser['id'], $userID)){
                   $notification = new Notification;
                   $notification->content ="It is 24 hours to the start of the claim window Opening";
                   $notification->status = "pending";
                   $notification->user_id = $branchuser['id'];
                   $notification->save();
                   $mail = [
                    'name' =>$branchuser['name'] .' '.$branchuser['last_name'],
                    'view'=>'email.supervisor',
                ];
                Mail::to($branchuser['email'])->send(new job($mail,$subject));
                $userID[] = $branchuser['id'];
            }
        }
    }
}
foreach($users['departments'] as $department){
    foreach($users['users'] as $departmentuser){
        if($departmentuser['id'] == $department['user_id'] && $departmentuser['type'] == "company_user"){
            if(!in_array($departmentuser['id'], $userID)){
             $subject = "Claim Window Ends";
             $notification = new Notification;
             $notification->content ="Claim Window has been ended.";
             $notification->status = "pending";
             $notification->user_id = $departmentuser['id'];
             $notification->save();
             $mail = [
                'name' =>$departmentuser['name'] .' '.$departmentuser['last_name'],
                'view'=>'email.supervisor',
            ];
            Mail::to($departmentuser['email'])->send(new job($mail,$subject));
            $userID[] = $departmentuser['id'];
        }
    }
}
}
foreach($users['units'] as $unit){
    foreach($users['users'] as $unituser){
        if($unituser['id'] == $unit['user_id'] && $unituser['type'] == "company_user"){
            if(!in_array($unituser['id'], $userID)){
             $subject = "Claim Window Ends";
             $notification = new Notification;
             $notification->content ="Claim Window has been ended.";
             $notification->status = "pending";
             $notification->user_id = $unituser['id'];
             $notification->save();
             $mail = [
                'name' =>$unituser['name'] .' '.$unituser['last_name'],
                'view'=>'email.supervisor',
            ];
            Mail::to($unituser['email'])->send(new job($mail,$subject));
            $userID[] = $unituser['id'];
        }
    }
}
}
}
}
}
