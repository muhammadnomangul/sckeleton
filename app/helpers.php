<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use App\Models\User;
use App\Models\Notification;

function apiUrl()
{
    return 'https://main.cleonhrdev.com.ng/api/';
}
function syncUserData($username, $password) {
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'verify-user', [
        'username' => $username,
        'password' => $password,
    ]);
    if ($response->successful()) {
        $userData = $response->json();   
        return $userData;
    }
    return null;
}
function resetPassword($email) {
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'reset', [
        'email' => $email,
    ]);

    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function updatePassword($email,$otp,$password) {
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'change-password', [
        'email' => $email,
        'otp' => $otp,
        'new_password' => $password,
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function branches($id){
    $response = \Illuminate\Support\Facades\Http::get(apiUrl() . 'branches/' . $id);
    if ($response->successful()) {
        $userData = $response->json();
        $bran = $userData['data']['branch'];
        $branchId= null;
        if (is_array($bran) && !empty($bran)) {
            $branchId = $userData['data']['branch']['id'];
        }
        // $departmentIds = [];
        // foreach ($userData['data']['department'] as $department) {
        //     $departmentIds[] = $department['id'];
        // }
        // $unitIds = [];
        // foreach ($userData['data']['unit'] as $departments) {
        //     foreach ($departments as $unit) {
        //         $unitIds[] = $unit['id'];
        //     }
        // }
        // $data = [];
        // $users = User::where('company_id', Auth::user()->company_id)->where('role', 'company_employee')->get();

        // $branchIds = $users->where('branch_id', $branchId)->pluck('id')->toArray();
        // $departmentIds = $users->whereIn('department_id', $departmentIds)->whereNotIn('id', $branchIds)->pluck('id')->toArray();
        // $unitIds = $users->whereIn('unit_id', $unitIds)->whereNotIn('id', array_merge($branchIds, $departmentIds))->pluck('id')->toArray();
        // $data = array_merge($branchIds, $departmentIds, $unitIds);
        return $branchId;
    }
    return null;
}
function syncDepartments($id) {
    $response = \Illuminate\Support\Facades\Http::get(apiUrl() . 'departments/' . $id);
    if ($response->successful()) {
        $userData = $response->json();
        $departmentIds = $userData['data']['department']['id'];
        $unitIds = [];
        foreach ($userData['data']['unit'] as $unit) {
            $unitIds[] = $unit['id'];
        }
        $data = [];
        $users = User::where('company_id', Auth::user()->company_id)->where('role', 'company_employee')->get();
        $departmentIds = $users->whereIn('department_id', $departmentIds)->pluck('id')->toArray();
        $unitIds = $users->whereIn('unit_id', $unitIds)->whereNotIn('id', $departmentIds)->pluck('id')->toArray();
        $data = array_merge($departmentIds, $unitIds);
        return $data;
    }
    return [];
}
function syncUnits($id) {
    $response = \Illuminate\Support\Facades\Http::get(apiUrl() . 'units/' . $id);
    if ($response->successful()) {
        $userData = $response->json();
        $unitIds = $userData['data']['object']['id'];
        $data = [];
        $users = User::where('company_id', Auth::user()->company_id)->where('role', 'company_employee')->get();
        $unitIds = $users->whereIn('unit_id', $unitIds)->pluck('id')->toArray();
        $data = $unitIds;
        return $data;
    }
    return [];
}
function syncEmployees() {

    $company_id = \Illuminate\Support\Facades\Auth::user()->company_id;
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'get-company-modules', [
        'company_id' => 115,
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function syncAnnouncements() {
    $user_id = \Illuminate\Support\Facades\Auth::user()->main_id;
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'get-announcements', [
        'user_id' => $user_id
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function registerCompany($data) {
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'register3', [
        "name" => $data['name'],
        "email" => $data['email'],
        "password" => $data['password'],
        "phone" => $data['phone'],
        "company_name" => $data['c_name'],
        "industry_id" => $data['industry'],
        "employee_size_id" => $data['e_size'],
        "company_phone" => $data['phone'],
        "company_email" => $data['email'],
        "company_website" => $data['website'],
        "company_revenue_id" => $data['revenue'],
        "company_address" => $data['address'],
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function id(){
    $user = Auth::user();
    $id = $user->role == 'company_admin' ? $user->id : User::where('company_id',$user->company_id)->where('role','company_admin')->pluck('id')->first();
    return $id;
}
function can_access($feature) {
    if(Auth::user()->role == "company_admin"){
     return true; 
 }
 $user_id = Auth::user()->main_id;
 $user_email = Auth::user()->email;
 $has_access = false;

 $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'get-access', [
    'user_id' => $user_id,
    'user_email' => $user_email,
]);
 if ($response->successful()) {
    $userData = $response->json();
    foreach ($userData as $permissions)
    {
        if($permissions['guard_name'] == $feature){
            $has_access = true;
            return $has_access;
        }
    }

    return $has_access;
}
return $has_access;
}
function data(){
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::get(apiUrl() . 'data-set');
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function getUser(){
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'get-user', [
        'user_id' => Auth::user()->main_id,
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function companyUsers($id){
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'get-company-users', [
        'company_id' => $id,
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function financialUsers($id){
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'financial-user', [
        'company_id' => Auth::user()->company_id,
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function companyModule($id){
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'get-company-modules', [
        'company_id' => $id,
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}

function updateUser($request){
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'update-user', [
        'user_id' => Auth::user()->main_id,
        'first_name' => $request->input('first_name'),
        'last_name' => $request->input('last_name'),
        'name' => $request->input('first_name'),
        'email' => $request->input('email'),
        'password' => $request->input('password'),
        'phone' => $request->input('phone'),
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function notification(){
    $notifications = Notification::where('user_id',Auth::user()->main_id)->orderBy('created_at','desc')->get();
    return $notifications;
}
function Countnoti(){
    $notifications = Notification::where('user_id',Auth::user()->main_id)->where('status','pending')->count();
    return $notifications;
}
function Check_email($email){
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'verify-user-email', [
        'email' => $email,
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function onboard($request,$password){
    $response = \Illuminate\Support\Facades\Http::post(apiUrl() . 'employee-register', [
        'email' => $request->email,
        'first_name' => $request->name,
        'last_name' => $request->last_name,
        'phone' => $request->phone,
        'password' => $password,
        'company_id' => Auth::user()->company_id,
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        return $userData;
    }
    return null;
}
function pingsso() {
    if (!\Illuminate\Support\Facades\Auth::check()) {
        $user_id = null;
    }else{
        $user_id = \Illuminate\Support\Facades\Auth::user()->main_id;
    }
    $user_ip = request()->ip();
    $user_mac = exec('getmac');
    $user_browser = $_SERVER['HTTP_USER_AGENT'];
    $apiUrl = apiUrl();
    $response = \Illuminate\Support\Facades\Http::post($apiUrl . 'putsso', [
        'user_id' => $user_id,
        'user_ip' => $user_ip,
        'user_mac' => $user_mac,
        'user_browser' => $user_browser
    ]);
    if ($response->successful()) {
        $userData = $response->json();
        
    }else{ 
        return null;
    }
}