<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
     if (is_int($this->resource)) {
        $user = User::find($this->resource);

        return [
            'id' => $this->resource,
            'employee_name' => $user ? $user->name : null,
            'employee_email' => $user ? $user->email : null,
            'employee_phone' => $user ? $user->phone : null,
                // Include other properties as needed
        ];
    }

    return [
        'id' => $this->id,
        'employee_name' => $this->name,
        'employee_email' => $this->email,
        'employee_phone' => $this->phone,
            // Include other properties as needed
    ];
}
}
