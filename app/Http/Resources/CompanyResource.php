<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\User;
class CompanyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        if (is_int($this->resource)) {
            $company = User::find($this->resource);

            return [
                'id' => $this->resource,
                'company_name' => $company ? $company->c_name : null,
                'company_country' => $company ? $company->country : null,
            ];
        }

        return [
            'id' => $this->id,
            'company_name' => $this->c_name,
            'company_country' => $this->country,
        ];
    }
}
