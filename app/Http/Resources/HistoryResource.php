<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ClaimsResource;
class HistoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
     return [
        'id' => $this->id,
        'Claim'=> new ClaimsResource($this->claim_id),
        'claim_time' => $this->t_hour,
        "document" => $this->document,
        "status" => $this->status,
        "action_by" => $this->action_by,
    ];
}
}
