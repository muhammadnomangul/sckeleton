<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\CompanyResource;
use App\Models\Claim;
class ClaimResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
     if (is_int($this->resource)) {
        $claim = Claim::find($this->resource);

        return [
            'id' => $this->resource,
            'claim_title' => $claim ? $claim->title : null,
            'claim_value' => $claim ? $claim->value : null,
            'company' => $claim ? new CompanyResource($claim->company_id) : null,
        ];
    }

    return [
        'id' => $this->id,
        'title' => $this->name,
        'claim_value' => $this->value,
    ];
}
}
