<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Claim;
class ClaimsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
     if (is_int($this->resource)) {
      $claim = Claim::find($this->resource);

      return [
        'id' => $this->resource,
        'claim_title'=> $claim->title,
        'claim_schedule'=> $claim->schedule,
        'payment_value' => $claim->value,
        "window_start_date" => $claim->time_start,
        "window_end_date" => $claim->time_end,
        "last_update" => $claim->updated_at,
      ];
    }
    return [
      'id' => $this->id,
      'claim_title'=> $this->title,
      'claim_schedule'=> $this->schedule,
      'payment_value' => $this->value,
      "window_start_date" => $this->time_start,
      "window_end_date" => $this->time_end,
      "last_update" => $this->updated_at,
      "company" => new CompanyResource($this->company_id),
    ];
  }
}
