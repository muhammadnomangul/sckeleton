<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\ClaimResource;
use App\Http\Resources\UserResource;
use App\Models\User;
class EmployeeClaimResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {

      return [
        'id' => $this->id,
        'employee'=> new UserResource($this->user_id),
        'title'=> new ClaimResource($this->user_id),
        'claim_time' => $this->t_hour,
        "document" => $this->document,
        "status" => $this->status,
        "action_by" => $this->action_by,
    ];
}
}
