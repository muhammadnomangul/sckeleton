<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Claim;
use App\Models\User;
use App\Models\EmployeeClaim;
use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use App\Models\Country;
use App\Models\Survey;
use App\Models\SurveyOption;
use App\Models\SurveyAnswer;
use Auth;
use Validator;
use Hash;
use Illuminate\Validation\Rule;
class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }
    public function custom_data(Request $request){
      $startDate = now()->startOfMonth();
      if ($request->data === 'today') {
          $startDate = now()->subHours(24);
          $endDate = now();
      } elseif ($request->data === 'week') {
        $startDate = now()->subDays(7);
        $endDate = now();
    } elseif ($request->data === 'month') {
        $startDate = now()->subDays(30);
        $endDate = now();
    } elseif ($request->data !== 'all') {
        $startDate = \Carbon\Carbon::parse($request->data)->startOfDay();
        $endDate = $startDate->copy()->endOfDay();
    }

    $partners = User::where('role','company_admin')->where('created_at', '>=', $startDate)
    ->where('created_at', '<=', $endDate)->count();
    $approved_claims = EmployeeClaim::where('status','approved')->where('created_at', '>=', $startDate)
    ->where('created_at', '<=', $endDate)->count();
    $declined_claims = EmployeeClaim::where('status','declined')->where('created_at', '>=', $startDate)
    ->where('created_at', '<=', $endDate)->count();
    $requested_claims = EmployeeClaim::where('created_at', '>=', $startDate)
    ->where('created_at', '<=', $endDate)->count();
    $claim_types = Claim::where('created_at', '>=', $startDate)
    ->where('created_at', '<=', $endDate)->count();
    $approved_claim_amount = EmployeeClaim::where('status', 'approved')->where('created_at', '>=', $startDate)
    ->where('created_at', '<=', $endDate)
    ->with('employee_claim') // Load the related Claim model
    ->get()
    ->sum(function ($employeeClaim) {
        return $employeeClaim->t_hour * $employeeClaim->employee_claim->value;
    });
    $declined_claim_amount = EmployeeClaim::where('status', 'declined')->where('created_at', '>=', $startDate)
    ->where('created_at', '<=', $endDate)
    ->with('employee_claim')
    ->get()
    ->sum(function ($employeeClaim) {
        return $employeeClaim->t_hour * $employeeClaim->employee_claim->value;
    });
    $total_claim_amount =  EmployeeClaim::with('employee_claim')->where('created_at', '>=', $startDate)
    ->where('created_at', '<=', $endDate)
    ->get()
    ->sum(function ($employeeClaim) {
        return $employeeClaim->t_hour * $employeeClaim->employee_claim->value;
    });
    $response = [
        'approved_claims' => $approved_claims,
        'declined_claims' => $declined_claims,
        'requested_claims' => $requested_claims,
        'claim_types' => $claim_types,
        'approved_claim_amount' => $approved_claim_amount,
        'declined_claim_amount' => $declined_claim_amount,
        'total_claim_amount' => $total_claim_amount,
        'partners'=> $partners,
    ];
    return response()->json($response);
}
public function chart_data(Request $request){
    $approved_claims = EmployeeClaim::where('status','approved')->count();
    $declined_claims = EmployeeClaim::where('status','declined')->count();
    $declined_claim_amount = EmployeeClaim::where('status', 'declined')
    ->with('employee_claim')
    ->get()
    ->sum(function ($employeeClaim) {
        return $employeeClaim->t_hour * $employeeClaim->employee_claim->value;
    });
    $approved_claim_amount = EmployeeClaim::where('status', 'approved')
    ->with('employee_claim') // Load the related Claim model
    ->get()
    ->sum(function ($employeeClaim) {
        return $employeeClaim->t_hour * $employeeClaim->employee_claim->value;
    });
    $currentYear = now()->format('Y');
    $yearly_request = [];
    $yearly_approved = [];
    $yearly_declined = [];
    $yearly_claim_vlaue = [];
    $yearly_approve_vlaue = [];
    $yearly_decline_vlaue = [];
    $partners = [];
    for ($i = 1; $i <= 12; $i++) {
        $monthStart = now()->setYear($currentYear)->setMonth($i)->startOfMonth();
        $monthEnd = now()->setYear($currentYear)->setMonth($i)->endOfMonth();
        $requested = EmployeeClaim::whereBetween('created_at', [$monthStart, $monthEnd])->count();
        $approved = EmployeeClaim::where('status','approved')->whereBetween('created_at', [$monthStart, $monthEnd])->count();
        $declined = EmployeeClaim::where('status','approved')->whereBetween('created_at', [$monthStart, $monthEnd])->count();
        $total_claim_amount =  EmployeeClaim::whereBetween('created_at', [$monthStart, $monthEnd])->with('employee_claim')
        ->get()
        ->sum(function ($employeeClaim) {
            return $employeeClaim->t_hour * $employeeClaim->employee_claim->value;
        });
        $total_approve_amount =  EmployeeClaim::where('status','approved')->whereBetween('created_at', [$monthStart, $monthEnd])->with('employee_claim')
        ->get()
        ->sum(function ($employeeClaim) {
            return $employeeClaim->t_hour * $employeeClaim->employee_claim->value;
        });
        $total_decline_amount =  EmployeeClaim::where('status','declined')->whereBetween('created_at', [$monthStart, $monthEnd])->with('employee_claim')
        ->get()
        ->sum(function ($employeeClaim) {
            return $employeeClaim->t_hour * $employeeClaim->employee_claim->value;
        });
        $partner =  User::where('role','company_admin')->whereBetween('created_at', [$monthStart, $monthEnd])->count();
        $dateStr = now()->setYear($currentYear)->setMonth($i)->format('Y-m-d');

        $yearly_request[] += $requested;
        $yearly_approved[] += $approved;
        $yearly_declined[] += $declined;
        $yearly_claim_vlaue[] += $total_claim_amount;
        $yearly_approve_vlaue[] += $total_approve_amount;
        $yearly_decline_vlaue[] += $total_decline_amount;
        $partners[] += $partner;
    }
    $response = [
        'approved_claims' => $approved_claims,
        'declined_claims' => $declined_claims,
        'declined_claim_amount' => $declined_claim_amount,
        'approved_claim_amount' => $approved_claim_amount,
        'yearly_request'=> $yearly_request,
        'yearly_approved'=> $yearly_approved,
        'yearly_declined'=> $yearly_declined,
        'yearly_claim_vlaue'=>$yearly_claim_vlaue,
        'yearly_approve_vlaue'=> $yearly_approve_vlaue,
        'yearly_decline_vlaue'=> $yearly_decline_vlaue,
        'partners' => $partners,
    ];
    return response()->json($response);
}
public function approve_claim(Request $request)
{
    $claims = EmployeeClaim::where('status','approved')->orderBy('created_at','desc')->get(); 
    $countries = Country::all();
    $id = $request->id;
    if($id){
       $claim = EmployeeClaim::where('status','approved')->where('id',$id)->first();
       return view('admin.approved_claim',compact('claim','countries'));
   }
   return view('admin.approved_claim',compact('claims','countries'));
}
public function claim_type(Request $request)
{
    $claims = Claim::orderBy('created_at','desc')->get();
    $count = Claim::count();
    $comapnies = User::where('role','company_admin')->get();
    $countries = Country::all();
    $today = Carbon::now()->toDateString();
    $id = $request->id;
    if($id){
       $claim = Claim::find($id);
       return view('admin.ClaimTypeManagement',compact('claim','count','comapnies','countries','today'));
   }
   return view('admin.ClaimTypeManagement',compact('claims','count','comapnies','countries','today'));
}
public function manage_claim(Request $request)
{
    $claims = EmployeeClaim::where('status','pending')->orderBy('created_at','desc')->get();
    $countries = Country::all();
    $id = $request->id;
    if($id){
       $claim = EmployeeClaim::where('status','pending')->where('id',$id)->first();
       return view('admin.ManageClaimRequests',compact('claim','countries'));
   }
   return view('admin.ManageClaimRequests',compact('claims','countries'));
}
public function filter(Request $request){
  $startDate = now()->startOfMonth();
  if ($request->date === 'last24') {
      $startDate = now()->subHours(24);
      $endDate = now();
  } elseif ($request->date === 'last7') {
    $startDate = now()->subDays(7);
    $endDate = now();
} elseif ($request->date === 'last30') {
    $startDate = now()->subDays(30);
    $endDate = now();
} elseif ($request->date !== 'all') {
    $startDate = \Carbon\Carbon::parse($request->date)->startOfDay();
    $endDate = $startDate->copy()->endOfDay();
}
$claims = EmployeeClaim::where('status',$request->value)->where('created_at', '>=', $startDate)
->where('created_at', '<=', $endDate)->get();
$countries = Country::all();
$response = [
    'component' => view('admin.approved', compact('claims','countries'))->render(),
];
return response()->json($response);
}
public function pendingfilter(Request $request){
  $startDate = now()->startOfMonth();
  if ($request->date === 'last24') {
      $startDate = now()->subHours(24);
      $endDate = now();
  } elseif ($request->date === 'last7') {
    $startDate = now()->subDays(7);
    $endDate = now();
} elseif ($request->date === 'last30') {
    $startDate = now()->subDays(30);
    $endDate = now();
} elseif ($request->date !== 'all') {
    $startDate = \Carbon\Carbon::parse($request->date)->startOfDay();
    $endDate = $startDate->copy()->endOfDay();
}
$countries = Country::all();
$claims = EmployeeClaim::where('status',$request->value)->where('created_at', '>=', $startDate)
->where('created_at', '<=', $endDate)->get();
$response = [
    'component' => view('admin.pending', compact('claims','countries'))->render(),
];
return response()->json($response);
}
public function typefilter(Request $request){
  $startDate = now()->startOfMonth();
  if ($request->date === 'last24') {
      $startDate = now()->subHours(24);
      $endDate = now();
  } elseif ($request->date === 'last7') {
    $startDate = now()->subDays(7);
    $endDate = now();
} elseif ($request->date === 'last30') {
    $startDate = now()->subDays(30);
    $endDate = now();
} elseif ($request->date !== 'all') {
    $startDate = \Carbon\Carbon::parse($request->date)->startOfDay();
    $endDate = $startDate->copy()->endOfDay();
}
$claims = Claim::where('created_at', '>=', $startDate)
->where('created_at', '<=', $endDate)->get();
$countries = Country::all();
$response = [
    'component' => view('admin.type', compact('claims','countries'))->render(),
];
return response()->json($response);
}
public function create_claim(Request $request){
    $claim = new Claim;
    $claim->title = $request->title;
    $claim->value = $request->value;
    $claim->company_id = $request->company_id;
    $claim->schedule = $request->schedule;
    $claim->desc = $request->desc;
    $claim->time_start = $request->start_date;
    $claim->time_end = $request->end_date;
    $claim->save();
    return redirect('/admin/claim/type')->with('success','Claim Added Successfully');
}
public function index(){
    $token_creation_url = "https://api.flutterwave.com/v3/tokenized-charges?type=card";
    $headers = [
        'Authorization' => 'Bearer FLWSECK_TEST-e9ea7d916925194e8b9d085afba5d2e0-X',
        'Content-Type' => 'application/json',
    ];


    $data = [
        'tx_ref' => 'unique_transaction_reference',
        'amount' => 5000,
        'currency' => 'NGN',
        'payment_type' => 'card',
        'redirect_url' => 'https://your-redirect-url.com',
        'order_id' => 'unique_order_id',
        'email' => 'mumer28517@gmail.com',
        'token' => 'FLWSECK_TEST-e9ea7d916925194e8b9d085afba5d2e0-X', // Make sure to include this line
    ];
    $response = Http::withHeaders($headers)
    ->timeout(60) // Set a higher timeout value in seconds
    ->post($token_creation_url, $data);
    if ($response->failed()) {
    dd($response->json()); // Print the error details
}
dd($response);
// Process the response and store the token securely
$token = $response->json()['data']['chargeToken']['embed_token'];
dd($token);
}
public function suggestion(Request $request){
    $approvedclaims = EmployeeClaim::where('status','approved') 
    ->whereHas('employee_claim', function ($query) use ($request) {
        $query->where('title', 'LIKE', '%' . $request->val . '%')
        ->orWhere('schedule', 'LIKE', '%' . $request->val . '%')
        ->orWhere('desc', 'LIKE', '%' . $request->val . '%');
    })->orderBy('created_at','desc')->get(); 
    $declinedclaims = EmployeeClaim::where('status','pending') 
    ->whereHas('employee_claim', function ($query) use ($request) {
        $query->where('title', 'LIKE', '%' . $request->val . '%')
        ->orWhere('schedule', 'LIKE', '%' . $request->val . '%')
        ->orWhere('desc', 'LIKE', '%' . $request->val . '%');
    })->orderBy('created_at','desc')->get();
    $claims = Claim::where(function ($query) use ($request) {
        $query->where('title', 'LIKE', '%' . $request->val . '%')
        ->orWhere('schedule', 'LIKE', '%' . $request->val . '%')
        ->orWhere('desc', 'LIKE', '%' . $request->val . '%');
    })
    ->get();
    $response = [
      'component' => view('admin.component', compact('approvedclaims', 'declinedclaims','claims'))->render(),
  ];
  return response()->json($response);
}
public function search(Request $request){
    $claims = EmployeeClaim::where('status','approved') 
    ->whereHas('employee_claim', function ($query) use ($request) {
        $query->where('title', 'LIKE', '%' . $request->val . '%')
        ->orWhere('schedule', 'LIKE', '%' . $request->val . '%')
        ->orWhere('desc', 'LIKE', '%' . $request->val . '%');
    })->orderBy('created_at','desc')->get(); 
    $requests = EmployeeClaim::where('status','pending') 
    ->whereHas('employee_claim', function ($query) use ($request) {
        $query->where('title', 'LIKE', '%' . $request->val . '%')
        ->orWhere('schedule', 'LIKE', '%' . $request->val . '%')
        ->orWhere('desc', 'LIKE', '%' . $request->val . '%');
    })->orderBy('created_at','desc')->get();
    $typeclaims = Claim::where(function ($query) use ($request) {
        $query->where('title', 'LIKE', '%' . $request->val . '%')
        ->orWhere('schedule', 'LIKE', '%' . $request->val . '%')
        ->orWhere('desc', 'LIKE', '%' . $request->val . '%');
    })
    ->get();
    $countries = Country::all();
    return view('admin.search',compact('typeclaims','claims','requests','countries'));
}
public function manageComSurvey(){
    $surveys = Survey::where('type','company')->get();
    return view('admin.managecom',compact('surveys'));
} 
public function compSurvey(){
    $surveys = SurveyAnswer::select('user_id')->where('type', 'company')->groupBy('user_id')->get();
    return view('admin.comsurvey',compact('surveys'));
}
public function storeSurvey(Request $request){
    $questions = $request->questions;
    Survey::where('type',$request->type)->delete();
    foreach($questions as $question){
        $ques = Survey::create([
            'question'=>$question['question'],
            'type'=> $request->type,
        ]);
        foreach($question['options'] as $option){            
            $survey = SurveyOption::create([
                'survey_option_id' => $ques->id,
                'option'=>$option,
            ]);
        }
    }
    return redirect()->back()->with('success','Record Created Successfully');
}
public function Survey(Request $request){
    $surveys = SurveyAnswer::where('user_id',$request->id)->get();
    return response()->json(['survey'=>$surveys]);
}
public function profile(){
    return view('admin.profile');
}
public function changePassword(){
    return view('admin.password');
}
public function Updatepassword(Request $request){
 $validator = Validator::make($request->all(), [
     'old_password' => ['required'],
     'password' => ['required', 'string', 'min:8', 'confirmed'],
 ]);

 $user = Auth::user();
 if (!Hash::check($request->old_password, $user->password)) {
     return redirect()->back()->with('error','Old Password is Incorrect.');
 }
 if ($validator->fails()) {
    $errors = $validator->errors()->all();
    return redirect()
    ->back()
    ->withErrors($validator,'password')
    ->withInput()
    ->with('toast_error', $errors);
}

$user->password = Hash::make($request->password);
$user->save();
return redirect()->back()->with('success','Password Updated Successfully');
}
public function updateProfile(Request $request){
    $user = User::find(Auth::user()->id);
    $validator = Validator::make($request->all(), [
        'email' => [
            'required',
            'email',
            Rule::unique('users')->ignore($user->id),
        ],
    ]);
    if ($validator->fails()) {
        $errors = $validator->errors()->all();
        return redirect()
        ->back()
        ->withErrors($validator)
        ->withInput()
        ->with('toast_error', $errors);
    }

    $user->name = $request->name; 
    $user->l_name = $request->l_name; 
    $user->email = $request->email;
    if ($file = $request->hasfile('image')) {
      $file = $request->file('image');
      $fileName = uniqid() . $file->getClientOriginalName();
      $destinationPath ='profile/';
      if (!empty($user->image)) {
        $oldImagePath = $destinationPath . $user->image;
        if (file_exists($oldImagePath)) {
          unlink($oldImagePath);
      }
  }
  $file->move($destinationPath, $fileName);
  $request->image = 'profile/'.$fileName;
  $user->image = $request->image;
} 
$user->save(); 
return redirect()->back()->with('success','Profile Updated Successfully');
}

}
