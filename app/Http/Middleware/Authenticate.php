<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Session;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     */
    protected function redirectTo(Request $request): ?string
    {
     if (!Auth::check()) {
        $url = $request->fullUrl();
        $currentRoute = $request->route()->getName();
        if (strpos($currentRoute, 'company') !== false) {
            $companyroute = [
                'company.branch.accept',
                'company.branch.decline',
                'company.department.accept',
                'company.department.decline',
                'company.unit.accept',
                'company.unit.decline',
            ];

            if (in_array($currentRoute, $companyroute)) {
                Session::put('url' ,$url);
            }else{
                Session::remove('url' ,$url);
            }
            return 'company/login'; 
        } elseif (strpos($currentRoute, 'employee') !== false) {
            $employeeroute = [
                'employee.manage.claim',
            ];

            if (in_array($currentRoute, $employeeroute)) {
                Session::put('url' ,$url);
            }else{
                Session::remove('url' ,$url);
            }
            return 'user/login'; 
        }
    }
    return $request->expectsJson() ? null : route('login');
}
}
