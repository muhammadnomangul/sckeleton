<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array<int, string>
     */
    protected $except = [
        // Admin 
        "/api/admin/auth",

        // Company
        '/api/company/login',
        '/api/company/submit/claim',
        "/api/company/delete/claim/*",
        '/api/company/update/claim/*',
        "/api/company/claimed/claims",
        "/api/company/delete/claim/*",
        "/api/company/delete/claimed/claim/*",
        "/api/company/edit/claimed/claim/*",
        '/api/company/update/claimed/claim/*',

        // Company Employee
        '/api/company/add/employee',

        // Empployee
        '/api/employee/auth',
        '/api/employee/make/claim',
        '/api/employee/claim/delete/*',
        '/api/employee/claim/update/*',

    ];
}

