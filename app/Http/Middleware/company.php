<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Auth;
use Illuminate\Support\Facades\Session;

class company
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
      if(Auth::user()->role == 'company_admin' || Auth::user()->role === 'company_user'){
       if(Auth::user()->role == 'company_admin' && Auth::user()->is_first != 1){
        return to_route('company.first');
      }
      $url = Session::get('url');
      if($url){
        Session::remove('url');
        return redirect($url['intended']);
      }
      return $next($request);
    }
    return redirect('/');
  }
}
