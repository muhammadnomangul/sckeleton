<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Auth;
class adminapi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
      if (Auth::user()->role === 'admin') {
        return $next($request);
    } else {
        $response = [
            'success' => false,
            'message' => 'User is not a Admin',
            'status' => 404,
        ];
        return response()->json($response);
    }
}
}
