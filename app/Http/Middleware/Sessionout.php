<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Session;

class Sessionout
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(Auth::check()){
          $user = Auth::user();

          if ($user) {
            $lastActivity = $user->last_activity;
            if ($lastActivity && Carbon::now()->diffInMinutes($lastActivity) > 10) {
                if ($user->role == 'company_employee') {
                        $route = 'user.login';
                    } elseif ($user->role == 'admin') { // Corrected
                        $route = 'admin.login'; // Corrected
                    } else {
                        $route = 'company.login';
                    }
                    if ($request->method() === 'GET') {
                Session::put('previous_get_route', $request->url());
            }
                Auth::logout();
                if ($request->ajax()) {
                    return response()->json(['session' => 'Session Expired'],419);
                }
                return redirect()->route($route)->with('timeout', 'Your session has expired due to inactivity.');
            }
            $user->last_activity = Carbon::now();
            $user->save();
        }
    }
    return $next($request);
}
}
