<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth; // Added use statement for Auth

class companyapi
{
    /**
     * Handle an incoming request.
     *
     * @param \Closure $next
     */
    public function handle(Request $request, Closure $next): Response
    {
      if(Auth::user()->role == 'company_admin' || Auth::user()->role === 'company_user'){
            return $next($request);
        } else {
            $response = [
                'success' => false,
                'message' => 'User is not a Company',
                'status' => 404,
            ];
            return response()->json($response);
        }
    }
}

