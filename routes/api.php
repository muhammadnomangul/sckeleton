<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
// Admin
use App\Http\Controllers\Api\admin\AdminController;
// Company
use App\Http\Controllers\Api\company\ClaimController;
use App\Http\Controllers\Api\company\DashboardController;
use App\Http\Controllers\Api\company\UserController;
use App\Http\Controllers\Api\Company\CompanyController;

// Employee
use App\Http\Controllers\Api\employee\ClaimController as Claim;
use App\Http\Controllers\Api\employee\EmployeeController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::post('/company/login', [CompanyController::class, 'loginUser'])->name('company.login');
Route::post('/admin/auth', [AdminController::class, 'loginAdmin'])->name('admin.auth');
Route::post('/employee/auth', [EmployeeController::class, 'loginEmployee'])->name('employee.auth');



// ['prefix' => 'v1'] ,
Route::middleware('auth:sanctum')->group(function () {

// Admin Routes
  Route::middleware('adminapi')->prefix('admin')->group( function(){

    Route::post('/dashboard',[AdminController::class,'dashboard']);
    Route::get('/chart/data', [AdminController::class ,'chart_data']);
    Route::get('approved/claim',[AdminController::class,'approveClaim'])->name('admin.approved.claim');
    Route::get('claim/type',[AdminController::class,'claim_type'])->name('admin.claim.type');
    Route::get('manage/claim',[AdminController::class,'manage_claim'])->name('admin.manage.claim');
    Route::post('approved/filter', [AdminController::class ,'filter']);
    Route::post('/pendingfilter', [AdminController::class ,'pendingfilter']);
    Route::post('/typefilter', [AdminController::class ,'typefilter']);
    Route::post('/submit/claim',[AdminController::class,'createClaim'])->name('admin.submit.claim');

  });


    // Company Routes

  Route::middleware('companyapi')->prefix('company')->group(function () {

    Route::post('dashboard', [DashboardController::class, 'chart_data']);
    Route::get('setup/claims', [ClaimController::class, 'setupClaim'])->name('company.setup.claim');



    
    Route::post('submit/claim', [ClaimController::class, 'submitClaim'])->name('company.submit.claim');
    Route::get('/edit/claim/{id}', [ClaimController::class, 'editClaim'])->name('company.edit.claim');
    Route::post('update/claim/{id}', [ClaimController::class, 'updateClaim'])->name('company.update.claim');
    Route::post('delete/claim/{id}', [ClaimController::class, 'deleteClaim'])->name('company.delete.claim');
    Route::get('claimed/claims', [ClaimController::class, 'claimedClaim'])->name('company.claimed.claim');
    Route::post('delete/claimed/claim/{id}', [ClaimController::class, 'deleteClaimedClaim'])->name('company.delete.claimed.claim');
    Route::get('edit/claimed/claim/{id}', [ClaimController::class, 'editClaimedClaim'])->name('company.edit.claimed.claim');
    Route::post('update/claimed/claim/{id}', [ClaimController::class, 'updateClaimedClaim'])->name('company.update.claimed.claim');
    Route::post('add/employee', [UserController::class, 'addEmployee'])->name('company.add.employee');

  });

  Route::middleware('userapi')->prefix('employee')->group(function () {
        //Employee Claims
    Route::get('claim', [Claim::class ,'getClaim'])->name('employee.claim');
    Route::post('make/claim', [Claim::class ,'makeClaim'])->name('employee.make.claim');


  // Employee History
    Route::post('claim/update/{id}', [Claim::class ,'update_claim'])->name('employee.update.claim');
    Route::get('claim/history', [Claim::class ,'claimHistory'])->name('employee.claim.history');
    Route::post('claim/delete/{id}', [Claim::class ,'deleteClaim'])->name('employee.delete.claim');
    Route::get('claim/edit/{id}', [Claim::class ,'editClaim'])->name('employee.edit.claim');

  });

});



Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
  return $request->user();
});
