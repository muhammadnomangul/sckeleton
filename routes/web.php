<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\admin\AdminController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ForgotPasswordController;
// Company Controller

use App\Http\Controllers\company\CompanyController;
use App\Http\Controllers\company\ClaimController;
use App\Http\Controllers\company\MarketplaceController;
use App\Http\Controllers\company\UserController;
use App\Http\Controllers\ApprovalController;
// Employee Controller

use App\Http\Controllers\employee\ClaimController as Claim;
use App\Http\Controllers\employee\ProfileController;
use App\Http\Controllers\employee\EmployeeController;


use App\Http\Controllers\FacebookController;
use App\Http\Controllers\GoogleController;

use App\Http\Controllers\FlutterwaveController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/dashboard',[AdminController::class,'dashboard'])->name('dashboard');
Route::get('/',function(){
  return view('index');
});
