document.getElementById("profile1-id").innerHTML =`
<div class="d-flex align-items-center ms-3 dropdown1">
								<img  src="{{url('styling/image/user.jpg')}}" class="img-fluid rounded-pill w-25s dropbtn1" onclick="toggleDropdown()">
							
							<div class="dropdown-content" id="profiledrop">
								<a href="#"><i class="fas fa-user"></i> My Account</a>
								<a href="#"><i class="fas fa-credit-card"></i> Subscription</a>
								<a href="#"><i class="fas fa-cog"></i> Settings</a>
								<a href="/candidate-signup/login-page-candi.html"><i class="fas fa-sign-out-alt"></i> Logout</a>
							</div> 
							<div class="ms-2">
								<span class="fw-bold d-block fs-14">Remi Olunlonyo</span>
								<span class="fw-bold d-block fs-14">Dangote Plc</span>
							</div>
						</div>
`

document.getElementsByTagName('head')[0].innerHTML += `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<style>
.dropdown1 {
    position: relative;
    display: inline-block;
}

.dropbtn1 {
    background-color: transparent;
    border: none;
    cursor: pointer;
    outline: none;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #FFFFFF;
    min-width: 160px;
    top: 50px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
}

.dropdown-content a {
    color: #000000;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align:left
}

.dropdown-content a:hover {
    background-color: #E82583;
    color: #FFFFFF;
}

.showup {
    display: block;
}
</style>
`

function toggleDropdown() {
    document.getElementById("profiledrop").classList.toggle("showup");
}

window.onclick = function (event) {
    if (!event.target.matches('.dropbtn1')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        for (var i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('showup')) {
                openDropdown.classList.remove('showup');
            }
        }
    }
}