<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='https://fonts.googleapis.com/css?family=Poppins' rel='stylesheet'>
    <title>Document</title>

    <style>
        .foor {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 40px;
            background-color: #000;
            display: flex;
            font-family: 'Poppins';
            font-size: 13px;
        }

        .foor-title {
            flex: 1.5;
            color: white;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .foor-body {
            flex: 10;
            color: white;
            display: flex;
            align-items: center;
        }



        .cir {
            width: 1.5rem;
            height: 1.5rem;
            border-radius: 50%;
            background-color: aliceblue;
            position: absolute;
            border: 1px solid white;
            background-image: url('/Slide/IMG_0908.JPG');
            background-size: cover;
        }

        .pice {
            display: flex;
            flex-direction: column;
            flex: 2;
            align-items: start;
            justify-content: center;

        }

        .pic-side {
            display: flex;
            flex-direction: column;
            align-items: start;
            justify-content: center;
        }

        .cir:first-child {
            right: 1rem;
            bottom: 0.5rem;
            background-image: url('/Slide/employee6.png');
        }

        .cir:nth-child(2) {
            right: 2.3rem;
            bottom: 0.5rem;
            background-image: url('/Slide/employee7.png');

        }

        .cir:nth-child(3) {
            right: 3.6rem;
            bottom: 0.5rem;
            background-image: url('/Slide/employee8.png');
        }

        .cir:last-child {
            right: 6rem;
            bottom: 0.5rem;
            background-image: url('/Slide/poke.png');
        }

        .nots {
            width: 0.8rem;
            height: 0.8rem;
            background-color: red;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            color: white;
            font-size: 0.6rem;
            position: relative;
            bottom: 0.2rem;
            left: 0.2rem;
        }

        .popupme {
            width: 100px;
            height: 50px;
            background-color: white;
            position: absolute;
            bottom: 2.3rem;
            left: -2.3rem;
            z-index: 100;
            box-shadow: 2px 4px 6px 0px #1616168a;
            border-radius: 8px;
            display: none;
            align-items: center;
            justify-content: center;
            font-weight: bolder;
            font-size: 10px;

        }
    </style>
</head>

<body>

    <div class="foor">
        <div class="foor-title">
            Announcement:
        </div>
        <div class="foor-body">
            <marquee behavior="" direction="">
                <span style="margin-right: 7rem;">
                    Introducing our latest product, the XYZ Widget! It's packed with advanced features and designed to
                    enhance your productivity. Don't miss out, check it out now! </span>
                We are excited to announce our upcoming
                event, the Annual Conference 2023! Join industry experts and thought leaders for insightful discussions
                and networking opportunities. Save the date!
            </marquee>
        </div>
        <div class="pice">
            <p style="color: aliceblue; padding-left: 1.7rem;">
                Pokes:
            </p>
            <div>

                <div class="cir">
                </div>
                <div class="cir"></div>
                <div class="cir"></div>
                <div class="cir" style="display: flex; align-items: end; flex-direction: column; cursor: pointer;"
                    onclick="pp()">
                    <div class=" nots">
                        12
                    </div>
                    <div class="popupme">
                        <div
                            style="background-color: #E82583; color: white; padding: 3px 4px; border-radius: 4px; cursor: pointer;">

                            Poke All Back
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script>
        const pp = () => {
            if (document.querySelector('.popupme').style.display == 'none') {
                document.querySelector('.popupme').style.display = 'flex'
            } else {

                document.querySelector('.popupme').style.display = 'none'
            }
        }
    </script>

    <!-- Bottom slider script -->
    <script src="/Slide/slider.js"></script>

<script src="/Notification/noty.js"></script>

</body>

</html>