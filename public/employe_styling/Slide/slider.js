let body = document.getElementsByTagName('body')[0];
let head = document.getElementsByTagName('head')[0];

body.innerHTML += `
<div class="upper justify-content-center align-items-center d-flex peach">
<i class="fas fa-bars"></i>
</div>
<div class="foor peach">
<div class="foor-title">
    Announcement:
</div>
<div class="foor-body">
    <marquee behavior="" direction="">
        <span style="margin-right: 7rem;">
            Great news! We surpassed our revenue target. Let's recognize and appreciate your outstanding work. Thank you for your commitment!
            </span>
        The payroll for each staff has been modified to meet new government requirements!
    </marquee>
</div>
<div class="pice">
    <p style="color: aliceblue; padding-left: 1.7rem;">
        Pokes:
    </p>
    <div>

        <div class="cir">
        </div>
        <div class="cir"></div>
        <div class="cir"></div>
        <div class="cir" style="display: flex; align-items: end; flex-direction: column; cursor: pointer;"
            onclick="pp()">
            <div class=" nots">
                12
            </div>
            <div class="popupme">
                <div
                    style="background-color: #E82583; color: white; padding: 3px 4px; border-radius: 4px; cursor: pointer;">

                    Poke All Back
                </div>
            </div>
        </div>
    </div>

</div>
</div>`;

head.innerHTML += `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<style>
        .foor {
            position: fixed;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 40px;
            background-color: #000;
            display: flex;
            font-family: 'Poppins';
            font-size: 13px;
            z-index: 100;
            margin-top: 20px;
        }

        .foor-title {
            flex: 1.5;
            color: white;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
        }

        .foor-body {
            flex: 10;
            color: white;
            display: flex;
            align-items: center;
        }

        .cir {
            width: 1.5rem;
            height: 1.5rem;
            border-radius: 50%;
            background-color: aliceblue;
            position: absolute;
            border: 1px solid white;
            background-image: url('/Slide/IMG_0908.JPG');
            background-size: cover;
        }

        .pice {
            display: flex;
            flex-direction: column;
            flex: 2;
            align-items: start;
            justify-content: center;

        }

        .pic-side {
            display: flex;
            flex-direction: column;
            align-items: start;
            justify-content: center;
        }

        .cir:first-child {
            right: 1rem;
            bottom: 0.5rem;
            background-image: url('/Slide/employee6.png');
        }

        .cir:nth-child(2) {
            right: 2.3rem;
            bottom: 0.5rem;
            background-image: url('/Slide/employee7.png');

        }

        .cir:nth-child(3) {
            right: 3.6rem;
            bottom: 0.5rem;
            background-image: url('/Slide/employee8.png');
        }

        .cir:last-child {
            right: 6rem;
            bottom: 0.5rem;
            background-image: url('/Slide/poke.png');
        }

        .nots {
            width: 0.8rem;
            height: 0.8rem;
            background-color: red;
            border-radius: 50%;
            display: flex;
            align-items: center;
            justify-content: center;
            color: white;
            font-size: 0.6rem;
            position: relative;
            bottom: 0.2rem;
            left: 0.2rem;
        }

        .popupme {
            width: 100px;
            height: 50px;
            background-color: white;
            position: absolute;
            bottom: 2.3rem;
            left: -2.3rem;
            z-index: 100;
            box-shadow: 2px 4px 6px 0px #1616168a;
            border-radius: 8px;
            display: none;
            align-items: center;
            justify-content: center;
            font-weight: bolder;
            font-size: 10px;

        }

        .disa{
            animation: slideBout 2s;
            bottom: -2.3rem
        }

        .peach{
            animation: slideBin 2s;
            
        }

        @keyframes slideBout {
            0% {
                bottom: 0rem
            }

            

            100% {
                bottom: -2.3rem
            }
        }

        @keyframes slideBin {
            0% {
                bottom: -2.3rem
            }

            

            100% {
                bottom: 0rem
            }
        }

        .upper{
            width: 20px;
            height: 20px;
            background-color: #E82583;
            position: fixed;
            bottom: 35px;
            right: 5px;
            z-index: 999;
        }
    </style>
`


function pp() {
    if (document.querySelector('.popupme').style.display != 'flex') {
        document.querySelector('.popupme').style.display = 'flex'
    } else {

        document.querySelector('.popupme').style.display = 'none'
    }
}

function disa(){

    document.getElementsByClassName('foor')[0].classList.toggle('disa');
    document.getElementsByClassName('foor')[0].classList.toggle('peach');
}

document.getElementsByClassName('upper')[0].addEventListener('click', disa)