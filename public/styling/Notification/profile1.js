
document.getElementsByTagName('head')[0].innerHTML += `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<style>
.dropdown1 {
    position: relative;
    display: inline-block;
}

.dropbtn1 {
    background-color: transparent;
    border: none;
    cursor: pointer;
    outline: none;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #FFFFFF;
    min-width: 160px;
    top: 50px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    z-index: 1;
}

.dropdown-content a {
    color: #000000;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
    text-align: left;
}

.dropdown-content a:hover {
    background-color: #E82583;
    color: #FFFFFF;
}

.show {
    display: block;
}
</style>
`

function toggleDropdown() {
    document.getElementById("profiledrop").classList.toggle("show");
}

window.onclick = function (event) {
    if (!event.target.matches('.dropbtn1')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        for (var i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}