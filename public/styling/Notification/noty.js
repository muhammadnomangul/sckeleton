

    document.getElementById("noty-id").innerHTML = `
<div class="circle-bg d-flex align-items-center justify-content-center" id="tapps">
                            <div class="notification">
                                <i class="fas fa-bell" onclick="drop()"></i>
                                <div class="dropdown" id="dropdownAbout" style="z-index: 999;">
                                    <div style="padding: 1rem;" class="dropdown-item" onclick="por(0)">
                                        <div class="dropdown-item">
                                            <div class="d-flex align-items-center">
                                                <i class="fas fa-comment"></i>
                                                <span>&nbsp;&nbsp;&nbsp;You have a new message from
                                                    Oluwaseyi</span>
                                            </div>
                                        </div>
                                        <div class="notification-buttons">
                                            <div class="respond-button notification-button " onclick="actionOne()">
                                                Dismiss</div>
                                            <div class="respond-button notification-button" onclick="actionTwo()">
                                                Respond</div>
                                        </div>
                                        <div style="text-align: end; font-size: 9px">
                                            3 minutes ago
                                        </div>
                                    </div>
                                    <div style="padding: 1rem;" class="dropdown-item" onclick="por(1)">
                                        <div style="padding: 1rem;" class="dropdown-item">
                                            <i class="fas fa-thumbs-up"></i>&nbsp;&nbsp;&nbsp;It's bonus
                                            time! Get
                                            rewarded soon
                                        </div>
                                        <div class="notification-buttons">
                                            <div class="respond-button notification-button " onclick="actionOne()">
                                                Dismiss</div>
                                            <div class="respond-button notification-button" onclick="actionTwo()">Bonus
                                            </div>
                                        </div>
                                        <div style="text-align: end; font-size: 9px">
                                            3 minutes ago
                                        </div>
                                    </div>
                                    <div style="padding: 1rem;" class="dropdown-item" onclick="por(2)">
                                        <div style="padding: 1rem;" class="dropdown-item">
                                            <i class="fas fa-file"></i>&nbsp;&nbsp;&nbsp;Say Hello! to our
                                            new policy
                                            update.
                                        </div>
                                        <div class="notification-buttons">
                                            <div class="respond-button notification-button " onclick="actionOne()">
                                                Dismiss</div>
                                            <div class="respond-button notification-button" onclick="actionTwo()">Policy
                                            </div>
                                        </div>
                                        <div style="text-align: end; font-size: 9px">
                                            3 minutes ago
                                        </div>
                                    </div>
                                    <div style="padding: 1rem;" class="dropdown-item" onclick="por(3)">
                                        <div style="padding: 1rem;" class="dropdown-item">
                                            <i class="fas fa-calendar"></i>&nbsp;&nbsp;&nbsp;Mandatory
                                            training. Save
                                            the date!
                                        </div>
                                        <div class="notification-buttons">
                                            <div class="respond-button notification-button " onclick="actionOne()">
                                                Dismiss</div>
                                            <div class="respond-button notification-button" onclick="actionTwo()">
                                                Calendar</div>
                                        </div>
                                        <div style="text-align: end; font-size: 9px">
                                            3 minutes ago
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

`

document.getElementsByTagName('head')[0].innerHTML += `
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
<style>
.notification {
    position: relative;
    display: inline-block;
    background-color: #f9f9f9;
    color: #333;
}

.dropdown {
    position: absolute;
    top: 31px;
    right: -31px;
    z-index: 1;
    display: none;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    background-color: #fff;
    border-radius: 5px;
}



.dropdown-item {
    padding: 12px 16px;
    color: #333;
    text-decoration: none;
    display: block;
}

.dropdown-item:hover {
    background-color: #E82583;
    cursor: pointer;
}

.notification-buttons {
    display: none;
    justify-content: space-around;
    align-items: center;
    flex-direction: row-reverse;
    cursor: pointer;
    margin-bottom: 4px;
}

.respond-button {
    background-color: #FFFFFF;
    color: #E82583;
    border: 2px solid #E82583;
}

.notification-button {
    border-radius: 5px;
    padding: 3px 5px;
}

.dropdown-item:hover .notification-button {
    background-color: #E82583;
    color: #FFFFFF;
    border: 2px solid #fff;
}
</style>
`



        const clearAll = () => {
            document.getElementById('dropdownAbout').style.display = 'none';
        }

        const drop = async () => {
            if (document.getElementById('dropdownAbout').style.display == 'block') {
                document.getElementById('dropdownAbout').style.display = 'none';
                console.log("YES")
            } else {
                clearAll();
                document.getElementById('dropdownAbout').style.display = 'block';
                console.log("NO")
            }
            await  document.querySelectorAll('.notification-buttons').forEach((e) => {
                e.style.display = "none";
            });
        }
        const por = async (n) => {

            await document.querySelectorAll('.notification-buttons').forEach((e) => {
                e.style.display = "none";
            });

            document.querySelectorAll('.notification-buttons')[n].style.display = 'flex';
        }

        document.addEventListener('click', function(event) {
            let div = document.getElementById('dropdownAbout');
            let button = document.querySelector('.fas.fa-bell')
            // Check if the target element is outside of the div
            if (!div.contains(event.target) && div.style.display === 'block' && !button.contains(event.target)) {
              div.style.display = 'none';
            }
          });
