    
    // 1st dropdown js
    $(document).ready(function(){
        $("#colheadone").click(function(){
            $("#collapseone").toggle();
            $(".colorchangeone").toggleClass("active");
            $(".hr-two").toggleClass("active");
            $(".hr-one").toggleClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapsethree").hide();
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");
            $(".colorchangethree").removeClass("active");

            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");


            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");

            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");


            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

        })
    });

    // 2nd dropdown js
    $(document).ready(function(){
        $("#colheadtwo").click(function(){
            $("#collapsetwo").toggle();
            $(".colorchangetwo").toggleClass("active");
            $(".hr-four").toggleClass("active");
            $(".hr-three").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsethree").hide();
            $(".hr-one-drop").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".colorchangethree").removeClass("active");

            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");

            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");

            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");

            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");

            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })
       
    });

    // 3rd dropdown js
    $(document).ready(function(){
        $("#colheadthree").click(function(){
            $("#collapsethree").toggle();
            $(".colorchangethree").toggleClass("active");
            $(".hr-one-drop").toggleClass("active");
            $(".hr-two-drop").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");

            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");


            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");

            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");

            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");

            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })
       
    });

    // 4th dropdown js
    $(document).ready(function(){
        $("#colheadfour").click(function(){
            $("#collapsefour").toggle();
            $(".colorchangefour").toggleClass("active");
            $(".hr-five").toggleClass("active");
            $(".hr-six").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-one-drop").removeClass("active");
            $(".hr-two-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");

            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");

            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");

            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })
       
    });

    // fifth dropdown js
    $(document).ready(function(){
        $("#colheadfive").click(function(){
            $("#collapsefive").toggle();
            $(".colorchangefive").toggleClass("active");
            $(".hr-seven").toggleClass("active");
            $(".hr-eight").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");


            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");

            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");

            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");

            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })
       
    });



    // sixth dropdown js
    $(document).ready(function(){
        $("#colheadsix").click(function(){
            $("#collapsesix").toggle();
            $(".colorchangesix").toggleClass("active");
            $(".hr-nine").toggleClass("active");
            $(".hr-ten").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");


            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");

            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");


            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");

            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })
       
    });


    // seven dropdown js
    $(document).ready(function(){
        $("#colheadseven").click(function(){
            $("#collapseseven").toggle();
            $(".colorchangeseven").toggleClass("active");
            $(".hr-11").toggleClass("active");
            $(".hr-12").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-one-drop").removeClass("active");
            $(".hr-two-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");

            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");


            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");


            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })
       
    });



    // eight dropdown js
    $(document).ready(function(){
        $("#colheadeight").click(function(){
            $("#collapseeight").toggle();
            $(".colorchangeeight").toggleClass("active");
            $(".hr-13").toggleClass("active");
            $(".hr-14").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");

            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");

            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");


            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })
       
    });


     // nine dropdown js
    $(document).ready(function(){
        $("#colheadnine").click(function(){
            $("#collapsenine").toggle();
            $(".colorchangenine").toggleClass("active");
            $(".hr-15").toggleClass("active");
            $(".hr-16").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");

            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");

            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })
       
    });


    // ten dropdown js
    $(document).ready(function(){
        $("#colheadten").click(function(){
            $("#collapsenten").toggle();
            $(".colorchangeten").toggleClass("active");
            $(".hr-17").toggleClass("active");
            $(".hr-18").toggleClass("active");


            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");


            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-one-drop").removeClass("active");
            $(".hr-two-drop").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");

            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");
        })
       
    });



    // 11 dropdown js
    $(document).ready(function(){
        $("#colhead11").click(function(){
            $("#collapsen11").toggle();
            $(".colorchange11").toggleClass("active");
            $(".hr-19").toggleClass("active");
            $(".hr-20").toggleClass("active");


            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");


            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");


            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");
        })

        $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");
       
    });


    // 12 dropdown js
    $(document).ready(function(){
        $("#colhead12").click(function(){
            $("#collapsen12").toggle();
            $(".colorchange12").toggleClass("active");
            $(".hr-21").toggleClass("active");
            $(".hr-22").toggleClass("active");


            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");


            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");


            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");


            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");

            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

        })
       
    });



    // 13 dropdown js
    $(document).ready(function(){
        $("#colhead13").click(function(){
            $("#collapsen13").toggle();
            $(".colorchange13").toggleClass("active");
            $(".hr-23").toggleClass("active");
            $(".hr-24").toggleClass("active");


            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");


            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");


            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");


            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");

            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");

        })
       
    });



    // 14 dropdown js
    $(document).ready(function(){
        $("#colhead14").click(function(){
            $("#collapsen14").toggle();
            $(".colorchange14").toggleClass("active");
            $(".hr-25").toggleClass("active");
            $(".hr-26").toggleClass("active");

            $("#collapsen18").hide();
            $(".colorchange18").removeClass("active");
            $(".hr-33").removeClass("active");
            $(".hr-34").removeClass("active");

            $("#collapsen17").hide();
            $(".colorchange17").removeClass("active");
            $(".hr-31").removeClass("active");
            $(".hr-32").removeClass("active");

            $("#collapsen16").hide();
            $(".colorchange16").removeClass("active");
            $(".hr-29").removeClass("active");
            $(".hr-30").removeClass("active");

            $("#collapsen15").hide();
            $(".colorchange15").removeClass("active");
            $(".hr-27").removeClass("active");
            $(".hr-28").removeClass("active");


            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");


            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");


            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");


            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");
        })
       
    });

     // 15 dropdown js
     $(document).ready(function(){
        $("#colhead15").click(function(){
            $("#collapsen15").toggle();
            $(".colorchange15").toggleClass("active");
            $(".hr-27").toggleClass("active");
            $(".hr-28").toggleClass("active");

            $("#collapsen18").hide();
                $(".colorchange18").removeClass("active");
                $(".hr-33").removeClass("active");
                $(".hr-34").removeClass("active");

                $("#collapsen17").hide();
                $(".colorchange17").removeClass("active");
                $(".hr-31").removeClass("active");
                $(".hr-32").removeClass("active");

                $("#collapsen16").hide();
                $(".colorchange16").removeClass("active");
                $(".hr-29").removeClass("active");
                $(".hr-30").removeClass("active");
           
            $("#collapsen14").hide();
            $(".colorchange14").removeClass("active");
            $(".hr-25").removeClass("active");
            $(".hr-26").removeClass("active");

            $("#collapsen13").hide();
            $(".colorchange13").removeClass("active");
            $(".hr-23").removeClass("active");
            $(".hr-24").removeClass("active");


            $("#collapsen12").hide();
            $(".colorchange12").removeClass("active");
            $(".hr-21").removeClass("active");
            $(".hr-22").removeClass("active");


            $("#collapsen11").hide();
            $(".colorchange11").removeClass("active");
            $(".hr-19").removeClass("active");
            $(".hr-20").removeClass("active");


            $("#collapsenten").hide();
            $(".colorchangeten").removeClass("active");
            $(".hr-17").removeClass("active");
            $(".hr-18").removeClass("active");


            $("#collapsenine").hide();
            $(".colorchangenine").removeClass("active");
            $(".hr-15").removeClass("active");
            $(".hr-16").removeClass("active");


            $("#collapseeight").hide();
            $(".colorchangeeight").removeClass("active");
            $(".hr-13").removeClass("active");
            $(".hr-14").removeClass("active");

            $("#collapseseven").hide();
            $(".colorchangeseven").removeClass("active");
            $(".hr-11").removeClass("active");
            $(".hr-12").removeClass("active");

            $("#collapsesix").hide();
            $(".colorchangesix").removeClass("active");
            $(".hr-nine").removeClass("active");
            $(".hr-ten").removeClass("active");


            $("#collapsefive").hide();
            $(".colorchangefive").removeClass("active");
            $(".hr-seven").removeClass("active");
            $(".hr-eight").removeClass("active");


            $("#collapsefour").hide();
            $(".colorchangefour").removeClass("active");
            $(".hr-five").removeClass("active");
            $(".hr-six").removeClass("active");


            $("#collapsethree").hide();
            $(".colorchangethree").removeClass("active");
            $(".hr-two-drop").removeClass("active");
            $(".hr-one-drop").removeClass("active");

            $("#collapsetwo").hide();
            $(".colorchangetwo").removeClass("active");
            $(".hr-four").removeClass("active");
            $(".hr-three").removeClass("active");

            $("#collapseone").hide();
            $(".colorchangeone").removeClass("active");
            $(".hr-two").removeClass("active");
            $(".hr-one").removeClass("active");
        })
       
    });

         // 16 dropdown js
         $(document).ready(function(){
            $("#colhead16").click(function(){
                $("#collapsen16").toggle();
                $(".colorchange16").toggleClass("active");
                $(".hr-29").toggleClass("active");
                $(".hr-30").toggleClass("active");
                
                $("#collapsen18").hide();
                $(".colorchange18").removeClass("active");
                $(".hr-33").removeClass("active");
                $(".hr-34").removeClass("active");

                $("#collapsen17").hide();
                $(".colorchange17").removeClass("active");
                $(".hr-31").removeClass("active");
                $(".hr-32").removeClass("active");

                
                $("#collapsen15").hide();
                $(".colorchange15").removeClass("active");
                $(".hr-27").removeClass("active");
                $(".hr-28").removeClass("active");
    
                $("#collapsen14").hide();
                $(".colorchange14").removeClass("active");
                $(".hr-25").removeClass("active");
                $(".hr-26").removeClass("active");
    
                $("#collapsen13").hide();
                $(".colorchange13").removeClass("active");
                $(".hr-23").removeClass("active");
                $(".hr-24").removeClass("active");
    
    
                $("#collapsen12").hide();
                $(".colorchange12").removeClass("active");
                $(".hr-21").removeClass("active");
                $(".hr-22").removeClass("active");
    
    
                $("#collapsen11").hide();
                $(".colorchange11").removeClass("active");
                $(".hr-19").removeClass("active");
                $(".hr-20").removeClass("active");
    
    
                $("#collapsenten").hide();
                $(".colorchangeten").removeClass("active");
                $(".hr-17").removeClass("active");
                $(".hr-18").removeClass("active");
    
    
                $("#collapsenine").hide();
                $(".colorchangenine").removeClass("active");
                $(".hr-15").removeClass("active");
                $(".hr-16").removeClass("active");
    
    
                $("#collapseeight").hide();
                $(".colorchangeeight").removeClass("active");
                $(".hr-13").removeClass("active");
                $(".hr-14").removeClass("active");
    
                $("#collapseseven").hide();
                $(".colorchangeseven").removeClass("active");
                $(".hr-11").removeClass("active");
                $(".hr-12").removeClass("active");
    
                $("#collapsesix").hide();
                $(".colorchangesix").removeClass("active");
                $(".hr-nine").removeClass("active");
                $(".hr-ten").removeClass("active");
    
    
                $("#collapsefive").hide();
                $(".colorchangefive").removeClass("active");
                $(".hr-seven").removeClass("active");
                $(".hr-eight").removeClass("active");
    
    
                $("#collapsefour").hide();
                $(".colorchangefour").removeClass("active");
                $(".hr-five").removeClass("active");
                $(".hr-six").removeClass("active");
    
    
                $("#collapsethree").hide();
                $(".colorchangethree").removeClass("active");
                $(".hr-two-drop").removeClass("active");
                $(".hr-one-drop").removeClass("active");
    
                $("#collapsetwo").hide();
                $(".colorchangetwo").removeClass("active");
                $(".hr-three").removeClass("active");
                $(".hr-four").removeClass("active");
    
                $("#collapseone").hide();
                $(".colorchangeone").removeClass("active");
                $(".hr-two").removeClass("active");
                $(".hr-one").removeClass("active");
            })
           
        });

        // 17 dropdown js
        $(document).ready(function(){
            $("#colhead17").click(function(){
                $("#collapsen17").toggle();
                $(".colorchange17").toggleClass("active");
                $(".hr-31").toggleClass("active");
                $(".hr-32").toggleClass("active");


                $("#collapsen18").hide();
                $(".colorchange18").removeClass("active");
                $(".hr-33").removeClass("active");
                $(".hr-34").removeClass("active");

                $("#collapsen16").hide();
                $(".colorchange16").removeClass("active");
                $(".hr-29").removeClass("active");
                $(".hr-30").removeClass("active");
                
                $("#collapsen15").hide();
                $(".colorchange15").removeClass("active");
                $(".hr-27").removeClass("active");
                $(".hr-28").removeClass("active");
    
                $("#collapsen14").hide();
                $(".colorchange14").removeClass("active");
                $(".hr-25").removeClass("active");
                $(".hr-26").removeClass("active");
    
                $("#collapsen13").hide();
                $(".colorchange13").removeClass("active");
                $(".hr-23").removeClass("active");
                $(".hr-24").removeClass("active");
    
    
                $("#collapsen12").hide();
                $(".colorchange12").removeClass("active");
                $(".hr-21").removeClass("active");
                $(".hr-22").removeClass("active");
    
    
                $("#collapsen11").hide();
                $(".colorchange11").removeClass("active");
                $(".hr-19").removeClass("active");
                $(".hr-20").removeClass("active");
    
    
                $("#collapsenten").hide();
                $(".colorchangeten").removeClass("active");
                $(".hr-17").removeClass("active");
                $(".hr-18").removeClass("active");
    
    
                $("#collapsenine").hide();
                $(".colorchangenine").removeClass("active");
                $(".hr-15").removeClass("active");
                $(".hr-16").removeClass("active");
    
    
                $("#collapseeight").hide();
                $(".colorchangeeight").removeClass("active");
                $(".hr-13").removeClass("active");
                $(".hr-14").removeClass("active");
    
                $("#collapseseven").hide();
                $(".colorchangeseven").removeClass("active");
                $(".hr-11").removeClass("active");
                $(".hr-12").removeClass("active");
    
                $("#collapsesix").hide();
                $(".colorchangesix").removeClass("active");
                $(".hr-nine").removeClass("active");
                $(".hr-ten").removeClass("active");
    
    
                $("#collapsefive").hide();
                $(".colorchangefive").removeClass("active");
                $(".hr-seven").removeClass("active");
                $(".hr-eight").removeClass("active");
    
    
                $("#collapsefour").hide();
                $(".colorchangefour").removeClass("active");
                $(".hr-five").removeClass("active");
                $(".hr-six").removeClass("active");
    
    
                $("#collapsethree").hide();
                $(".colorchangethree").removeClass("active");
                $(".hr-two-drop").removeClass("active");
                $(".hr-one-drop").removeClass("active");
    
                $("#collapsetwo").hide();
                $(".colorchangetwo").removeClass("active");
                $(".hr-three").removeClass("active");
                $(".hr-four").removeClass("active");
    
                $("#collapseone").hide();
                $(".colorchangeone").removeClass("active");
                $(".hr-two").removeClass("active");
                $(".hr-one").removeClass("active");
            })
           
        });

         // 18 dropdown js
         $(document).ready(function(){
            $("#colhead18").click(function(){
                $("#collapsen18").toggle();
                $(".colorchange18").toggleClass("active");
                $(".hr-33").toggleClass("active");
                $(".hr-34").toggleClass("active");

                $("#collapsen17").hide();
                $(".colorchange17").removeClass("active");
                $(".hr-31").removeClass("active");
                $(".hr-32").removeClass("active");

                $("#collapsen16").hide();
                $(".colorchange16").removeClass("active");
                $(".hr-29").removeClass("active");
                $(".hr-30").removeClass("active");
                
                $("#collapsen15").hide();
                $(".colorchange15").removeClass("active");
                $(".hr-27").removeClass("active");
                $(".hr-28").removeClass("active");
    
                $("#collapsen14").hide();
                $(".colorchange14").removeClass("active");
                $(".hr-25").removeClass("active");
                $(".hr-26").removeClass("active");
    
                $("#collapsen13").hide();
                $(".colorchange13").removeClass("active");
                $(".hr-23").removeClass("active");
                $(".hr-24").removeClass("active");
    
    
                $("#collapsen12").hide();
                $(".colorchange12").removeClass("active");
                $(".hr-21").removeClass("active");
                $(".hr-22").removeClass("active");
    
    
                $("#collapsen11").hide();
                $(".colorchange11").removeClass("active");
                $(".hr-19").removeClass("active");
                $(".hr-20").removeClass("active");
    
    
                $("#collapsenten").hide();
                $(".colorchangeten").removeClass("active");
                $(".hr-17").removeClass("active");
                $(".hr-18").removeClass("active");
    
    
                $("#collapsenine").hide();
                $(".colorchangenine").removeClass("active");
                $(".hr-15").removeClass("active");
                $(".hr-16").removeClass("active");
    
    
                $("#collapseeight").hide();
                $(".colorchangeeight").removeClass("active");
                $(".hr-13").removeClass("active");
                $(".hr-14").removeClass("active");
    
                $("#collapseseven").hide();
                $(".colorchangeseven").removeClass("active");
                $(".hr-11").removeClass("active");
                $(".hr-12").removeClass("active");
    
                $("#collapsesix").hide();
                $(".colorchangesix").removeClass("active");
                $(".hr-nine").removeClass("active");
                $(".hr-ten").removeClass("active");
    
    
                $("#collapsefive").hide();
                $(".colorchangefive").removeClass("active");
                $(".hr-seven").removeClass("active");
                $(".hr-eight").removeClass("active");
    
    
                $("#collapsefour").hide();
                $(".colorchangefour").removeClass("active");
                $(".hr-five").removeClass("active");
                $(".hr-six").removeClass("active");
    
    
                $("#collapsethree").hide();
                $(".colorchangethree").removeClass("active");
                $(".hr-two-drop").removeClass("active");
                $(".hr-one-drop").removeClass("active");
    
                $("#collapsetwo").hide();
                $(".colorchangetwo").removeClass("active");
                $(".hr-four").removeClass("active");
                $(".hr-three").removeClass("active");
    
                $("#collapseone").hide();
                $(".colorchangeone").removeClass("active");
                $(".hr-two").removeClass("active");
                $(".hr-one").removeClass("active");
            })
           
        });