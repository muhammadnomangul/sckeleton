$(document).ready(function(){

    // emplement slider
$('#casestudy').owlCarousel({
    loop:true,
    margin:30,
    items:1,
    dots:true,
    nav:false,
    smartSpeed:1500,
    autoplay:true,
    autoplayTimeout:3000,
    responsive:{
        0:{
          items: 1,
        },
        576:{
          items: 2,
        },
        768: {
          items: 2,
      },
      1000:{
        items:5,
      }
    }
  })

});