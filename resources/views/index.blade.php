<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">
	<title>Home</title>
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
	<!-- owl carousel css -->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.theme.default.min.css')}}">
	<script src="https://kit.fontawesome.com/00b231478f.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
</head>
<style>
	#getStartedBtn {
		cursor: pointer;
		background-color: #3498db;
		color: #fff;
		padding: 10px;
		border-radius: 5px;
	}

	#dropdownContent {
		display: none;
		position: absolute;
		z-index: 1;
		background-color: #fff;
		box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
		padding: 10px;
		right: 0;
		border-radius: 5px; /* Add border-radius for a curved look */
	}

	#dropdownContent ul {
		list-style: none;
		margin: 0;
		padding: 0;
	}

	#dropdownContent ul li {
		margin-bottom: 10px;
	}

	#dropdownContent ul li a {
		display: block;
		padding: 8px;
		color: #333;
		text-decoration: none;
		border-radius: 3px; 
		transition: background-color 0.3s;
	}

	#dropdownContent ul li a:hover {
		background-color: #E82583;
		color: #fff;
	}
</style>
<body>
	<!-- header start from here -->
	<header class="header">
		<nav class="navbar navbar-expand-lg">
			<div class="container-fluid">
				<a class="navbar-brand" href="#">Cleon<span>HR</span></a>
				<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav me-auto ms-auto mb-2 mb-lg-0">
						<li class="nav-item">
							<a class="nav-link active" aria-current="page" href="#">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">About</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Pricing</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">FAQs</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Case Studies</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Testimonials</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Modules</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#">Support</a>
						</li>
					</ul>
					<div>
						@auth
						@if(Auth::user()->role == 'admin')
						<a href="{{route('admin.dashboard')}}" class="btn btn_started">
							Get me Back In
						</a>
						@elseif(Auth::user()->role == 'company_employee')
						<a href="{{route('employee.claim')}}" class="btn btn_started">
							Get me Back In
						</a>
						@else
						<a href="{{route('company.start')}}" class="btn btn_started">
							Get me Back In
						</a>
						@endif
						@else
						<a href="javascript:void(0);" class="btn btn_started" id="getStartedBtn">Login
							<span class="ms-2">
								<i class="fa-solid fa-caret-down" aria-hidden="true"></i>
							</span>
						</a>

						<div id="dropdownContent" style="display: none;">
							<ul>
								<li>
									<a target="_blank" href="{{route('user.login')}}">I am a Employee</a></li>
									<li><a target="_blank" href="{{route('company.login')}}">I am a Company</a></li>

								</ul>
							</div> 
							@endauth
						</div>
					</div>
				</div>
			</nav>
		</header>
		<!-- header end here -->

		<!-- hero section start from here -->
		<section class="hero_section">
			<div class="container">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-9 col-lg-6">
						<div>
							<h1 class="hero_section_heading">Plug The Gaps from <span>Claims</span></h1>
							<p class="text-white hero_section_para">
								Integrate our bespoke technology into your day-to-day operations for claims and out-of-pocket expenses
							</p>
							<div class="mt-5">
								<div class="search">
									<input placeholder="Discover HR in our knowledgebase" class="form-control">
									<a href="{{url('home/search')}}"></a><button class="btn btn_started py-1"><span class="me-3"><i class="fa-solid fa-magnifying-glass"></i></span> Search</button></a>
								</div>
							</div>
							@auth
							@else
							<div class="col-12">
								<div class="d-flex" style="margin-top:4rem;">
									<div class="bottons_start">
										<a href="{{route('company.register')}}">
											<button class="btn btn_started" style="padding:10px 20px">Get Started </button></a>
										</div>
									</div>
								</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- hero section end here -->

			<!-- solve hr section start from here -->
			<section class="solve_hr_section py-5">
				<div class="container pb-5">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<div class="position-relative">
								<div class="row g-3">
									<div class="col-5">
										<div class="firstimg">
											<img src="assets/images/m-1.png" class="img-fluid">
										</div>
									</div>
									<div class="col-5">
										<div class="position-relative">
											<div class="dots">
												<img src="assets/images/line-dash.png" class="img-fluid my-4">
												<img src="assets/images/line-dash.png" class="img-fluid my-4">
												<img src="assets/images/line-dash.png" class="img-fluid my-4">
												<img src="assets/images/line-dash.png" class="img-fluid my-4">
											</div>
										</div>
									</div>
									<div class="col-5">
										<div class="secondimg">
											<img src="assets/images/m-2.png" class="img-fluid">
										</div>
									</div>
									<div class="col-5">
										<div class="thirdimg">
											<img src="assets/images/m-3.png" class="img-fluid">
										</div>
									</div>
								</div>
								<div class="possearchbar">
									<div class="d-flex align-items-center w-100">
										<input type="text" class="form-control shadow border fs-12 inputsar me-3" placeholder="Recruit great talents" name="">
										<a href="#" class="text-decoration-none btn bg-pink rounded-3 p-2 widthsear">
											<img src="assets/images/star-search.png" class="img-fluid w-30s">
										</a>
									</div>
								</div>
								<div class="checkpost shadow-lg p-3 rounded-3 bg-white">
									<div class="d-flex align-items-center">
										<div class="flex-shrink-0">
											<img src="assets/images/cir-ch.png" class="img-fluid">
										</div>
										<div class="flex-grow-1 ">
											<p class="mb-0 fs-14 ms-3">Manage overtime <br> claims and performance</p>
										</div>
									</div>
								</div>
								<div class="groupphoto shadow-lg bg-whitep-3 text-center">
									<span class="fs-12 ">Connect with colleagues</span>
									<div class="d-flex align-items-center">
										<div>
											<img src="assets/images/user.jpg" class="img-fluid w-80s rounded-pill border border-white border-4">
										</div>
										<div class="marginleft">
											<img src="assets/images/user.jpg" class="img-fluid w-80s rounded-pill border border-white border-4">
										</div>
										<div class="marginleft">
											<img src="assets/images/user.jpg" class="img-fluid w-80s rounded-pill border border-white border-4">
										</div>
										<div class="marginleft">
											<img src="assets/images/user.jpg" class="img-fluid w-80s rounded-pill border border-white border-4">
										</div>
										<a href="#" class="text-decoration-none d-block marginleft">
											<img src="assets/images/plus-cir.png" class="img-fluid w-80s rounded-pill">
										</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-12 col-md-6 col-lg-6">
							<div class="mt-5 pt-5 mt-lg-0 pt-lg-0">
								<h3 class="text-pink heading">Solve all HR headaches <br> and automate all your processes <br>  with ISO certified processes</h3>
								<h5 class="fw-semibold mt-3">The best HR practice at the touch of a button</h5>
								<div class="mt-3">
									<ul class="list-unstyled solve_hr_section_list">
										<li>
											<span><i class="fa-solid fa-check"></i></span><span class="ms-3 fw-semibold text-capitalize">Recruit talent</span>
										</li>
										<li>
											<span><i class="fa-solid fa-check"></i></span><span class="ms-3 fw-semibold text-capitalize">manage health</span>
										</li>
										<li>
											<span><i class="fa-solid fa-check"></i></span><span class="ms-3 fw-semibold text-capitalize">manage claims </span>
										</li>
										<li>
											<span><i class="fa-solid fa-check"></i></span><span class="ms-3 fw-semibold">KYC Verification </span>
										</li>
										<li>
											<span><i class="fa-solid fa-check"></i></span><span class="ms-3 fw-semibold text-capitalize">manage performance </span>
										</li>
										<li>
											<span><i class="fa-solid fa-check"></i></span><span class="ms-3 fw-semibold text-capitalize">promte internal learning </span>
										</li>
										<li>
											<span><i class="fa-solid fa-check"></i></span><span class="ms-3 fw-semibold text-capitalize">Automate payroll </span>
										</li>
										<li>
											<span><i class="fa-solid fa-check"></i></span><span class="ms-3 fw-semibold text-capitalize">automate internal control </span>
										</li>
									</ul>

									<div>
										<button class="btn btn_started px-5">Get Started</button>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</section>
			<!-- solve hr section end here -->

			<!-- third section start from here -->
			<section class="third_section py-5">
				<div class="container mt-5">
					<div class="row">
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div>
								<ul class="list-unstyled third_section_list py-3 px-4">
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">Recruitment</span>
									</li>
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">Self Service</span>
									</li>
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">Cleon Time (Claims)</span>
									</li>
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">Health Management</span>
									</li>
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">Internal Control</span>
									</li>
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">HR Administration</span>
									</li>
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">Performance </span>
									</li>
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">Payroll Manager</span>
									</li>
									<li>
										<span><img src="assets/images/whitestar.png" alt="" class="img-fluid"></span>
										<span class="ms-2">Verification</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="card bg-transparent border-0 text-center third_section_card">
								<div class="card-body third_section_card_body">
									<div class="third_section_card_img">
										<img src="assets/images/Recuritment.png" alt="" class="img-fluid">
										<h5 class="text-white mt-3 text-capitalize">Recruitment</h5>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="card bg-transparent border-0 text-center third_section_card">
								<div class="card-body third_section_card_body">
									<div class="third_section_card_img">
										<img src="assets/images/payrollmodule.png" alt="" class="img-fluid">
										<h5 class="text-white mt-3 text-capitalize">Payroll Module</h5>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div class="card bg-transparent border-0 text-center third_section_card">
								<div class="card-body third_section_card_body">
									<div class="third_section_card_img">
										<img src="assets/images/selfservice.png" alt="" class="img-fluid">
										<h5 class="text-white mt-3 text-capitalize">Self Service</h5>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- third section end here  -->

			<!-- clients love section start -->
			<section class="py-5 my-5">
				<div class="container-fluid">
					<div class="row g-4 align-items-center">
						<div class="col-xl-3">
							<div class="ps-lg-4">
								<h1 class="text-pink hero_section_heading">
									Clients <br>
									Loves Cleon HR 
								</h1>
								<p class="text-pink">
									See what they <br>
									are saying about us
								</p>
								<div>
									<img src="assets/images/star.png" class="img-fluid w-30s">
									<img src="assets/images/star.png" class="img-fluid w-30s">
									<img src="assets/images/star.png" class="img-fluid w-30s">
									<img src="assets/images/star.png" class="img-fluid w-30s">
									<img src="assets/images/star.png" class="img-fluid w-30s">
								</div>
							</div>
						</div>
						<div class="col-xl-9">
							<div class="bg-swiper position-relative ps-lg-5">
								<div class="swiper mySwiper">
									<div class="swiper-wrapper">
										<div class="swiper-slide">
											<div class="card border-pink rounded-5 p-4 bg-white">
												<div class="card-body">
													<div>
														<h6 class="text-pink">Impressive!</h6>
														<div class="py-4">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
														</div>
														<p>
															“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary, we’ve compiled (and explained) 100 terms, phrases and resources all designers should know.”
														</p>
														<hr>
														<div class="d-flex align-items-center justify-content-between">
															<div class="d-flex align-items-center">
																<div class="flex-shrink-0">
																	<img src="assets/images/user.jpg" class="img-fluid w-80s rounded-pill border border-white border-4">
																</div>
																<div class="flex-grow-1 ms-3">
																	<h6 class="fw-bold">Remi A. Olunloyo</h6>
																	<span class="fs-12">Lagos</span>
																</div>
															</div>
															<div>
																<img src="assets/images/quotes.png" class="img-fluid w-80s">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="swiper-slide">
											<div class="card border-pink rounded-5 p-4 bg-white">
												<div class="card-body">
													<div>
														<h6 class="text-pink">Impressive!</h6>
														<div class="py-4">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
														</div>
														<p>
															“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary, we’ve compiled (and explained) 100 terms, phrases and resources all designers should know.”
														</p>
														<hr>
														<div class="d-flex align-items-center justify-content-between">
															<div class="d-flex align-items-center">
																<div class="flex-shrink-0">
																	<img src="assets/images/user.jpg" class="img-fluid w-80s rounded-pill border border-white border-4">
																</div>
																<div class="flex-grow-1 ms-3">
																	<h6 class="fw-bold">Remi A. Olunloyo</h6>
																	<span class="fs-12">Lagos</span>
																</div>
															</div>
															<div>
																<img src="assets/images/quotes.png" class="img-fluid w-80s">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="swiper-slide">
											<div class="card border-pink rounded-5 p-4 bg-white">
												<div class="card-body">
													<div>
														<h6 class="text-pink">Impressive!</h6>
														<div class="py-4">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
														</div>
														<p>
															“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary, we’ve compiled (and explained) 100 terms, phrases and resources all designers should know.”
														</p>
														<hr>
														<div class="d-flex align-items-center justify-content-between">
															<div class="d-flex align-items-center">
																<div class="flex-shrink-0">
																	<img src="assets/images/user.jpg" class="img-fluid w-80s rounded-pill border border-white border-4">
																</div>
																<div class="flex-grow-1 ms-3">
																	<h6 class="fw-bold">Remi A. Olunloyo</h6>
																	<span class="fs-12">Lagos</span>
																</div>
															</div>
															<div>
																<img src="assets/images/quotes.png" class="img-fluid w-80s">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="swiper-slide">
											<div class="card border-pink rounded-5 p-4 bg-white">
												<div class="card-body">
													<div>
														<h6 class="text-pink">Impressive!</h6>
														<div class="py-4">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
															<img src="assets/images/star.png" class="img-fluid w-30s">
														</div>
														<p>
															“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary, we’ve compiled (and explained) 100 terms, phrases and resources all designers should know.”
														</p>
														<hr>
														<div class="d-flex align-items-center justify-content-between">
															<div class="d-flex align-items-center">
																<div class="flex-shrink-0">
																	<img src="assets/images/user.jpg" class="img-fluid w-80s rounded-pill border border-white border-4">
																</div>
																<div class="flex-grow-1 ms-3">
																	<h6 class="fw-bold">Remi A. Olunloyo</h6>
																	<span class="fs-12">Lagos</span>
																</div>
															</div>
															<div>
																<img src="assets/images/quotes.png" class="img-fluid w-80s">
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- clients love section end -->

			<!-- case study section start from here -->
			<section class="py-5 casestudy_section">
				<div class="container-fluid">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12">
							<h1 class="text-pink hero_section_heading text-center">Case Study</h1>
						</div>
					</div>
					<div class="row mt-5">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12 col-xxl-12">
							<div class="owl-carousel owl-theme" id="casestudy">
								<div class="item">
									<div class="item-card caseone">
										<div class="card border-0 bg-transparent casestudy_card">
											<div class="card-header py-5 bg-transparent px-0 casestudy_card_header">
												<span class="text-white p-3 bg-pink">Jan 25th 2023</span>
											</div>
											<div class="card-body casestudy_card_body">
												<p class="text-white">
													“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary.........
												</p>
											</div>
											<div class="card-footer casestudy_card_footer pb-5">
												<a href="#" class="text-decoration-none text-white">Read More....</a>
											</div>
										</div>
									</div>
								</div>
								<div class="item mt-5">
									<div class="item-card casetwo">
										<div class="card border-0 bg-transparent casestudy_card">
											<div class="card-header py-5 bg-transparent px-0 casestudy_card_header">
												<span class="text-white p-3 bg-pink">Jan 25th 2023</span>
											</div>
											<div class="card-body casestudy_card_body">
												<p class="text-white">
													“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary.........
												</p>
											</div>
											<div class="card-footer casestudy_card_footer pb-5">
												<a href="#" class="text-decoration-none text-white">Read More....</a>
											</div>
										</div>
									</div>
								</div>
								<div class="item">
									<div class="item-card casethree">
										<div class="card border-0 bg-transparent casestudy_card">
											<div class="card-header py-5 bg-transparent px-0 casestudy_card_header">
												<span class="text-white p-3 bg-pink">Jan 25th 2023</span>
											</div>
											<div class="card-body casestudy_card_body">
												<p class="text-white">
													“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary.........
												</p>
											</div>
											<div class="card-footer casestudy_card_footer pb-5">
												<a href="#" class="text-decoration-none text-white">Read More....</a>
											</div>
										</div>
									</div>
								</div>
								<div class="item mt-5">
									<div class="item-card casefour">
										<div class="card border-0 bg-transparent casestudy_card">
											<div class="card-header py-5 bg-transparent px-0 casestudy_card_header">
												<span class="text-white p-3 bg-pink">Jan 25th 2023</span>
											</div>
											<div class="card-body casestudy_card_body">
												<p class="text-white">
													“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary.........
												</p>
											</div>
											<div class="card-footer casestudy_card_footer pb-5">
												<a href="#" class="text-decoration-none text-white">Read More....</a>
											</div>
										</div>
									</div>
								</div>
								<div class="item">
									<div class="item-card caseone">
										<div class="card border-0 bg-transparent casestudy_card">
											<div class="card-header py-5 bg-transparent px-0 casestudy_card_header">
												<span class="text-white p-3 bg-pink">Jan 25th 2023</span>
											</div>
											<div class="card-body casestudy_card_body">
												<p class="text-white">
													“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary.........
												</p>
											</div>
											<div class="card-footer casestudy_card_footer pb-5">
												<a href="#" class="text-decoration-none text-white">Read More....</a>
											</div>
										</div>
									</div>
								</div>
								<div class="item mt-5">
									<div class="item-card casetwo">
										<div class="card border-0 bg-transparent casestudy_card">
											<div class="card-header py-5 bg-transparent px-0 casestudy_card_header">
												<span class="text-white p-3 bg-pink">Jan 25th 2023</span>
											</div>
											<div class="card-body casestudy_card_body">
												<p class="text-white">
													“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary.........
												</p>
											</div>
											<div class="card-footer casestudy_card_footer pb-5">
												<a href="#" class="text-decoration-none text-white">Read More....</a>
											</div>
										</div>
									</div>
								</div>
								<div class="item">
									<div class="item-card casethree">
										<div class="card border-0 bg-transparent casestudy_card">
											<div class="card-header py-5 bg-transparent px-0 casestudy_card_header">
												<span class="text-white p-3 bg-pink">Jan 25th 2023</span>
											</div>
											<div class="card-body casestudy_card_body">
												<p class="text-white">
													“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary.........
												</p>
											</div>
											<div class="card-footer casestudy_card_footer pb-5">
												<a href="#" class="text-decoration-none text-white">Read More....</a>
											</div>
										</div>
									</div>
								</div>
								<div class="item mt-5">
									<div class="item-card casefour">
										<div class="card border-0 bg-transparent casestudy_card">
											<div class="card-header py-5 bg-transparent px-0 casestudy_card_header">
												<span class="text-white p-3 bg-pink">Jan 25th 2023</span>
											</div>
											<div class="card-body casestudy_card_body">
												<p class="text-white">
													“If you want to work in UI (or work with a UI designer), you need to speak the language. In our UI glossary.........
												</p>
											</div>
											<div class="card-footer casestudy_card_footer pb-5">
												<a href="#" class="text-decoration-none text-white">Read More....</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- case section study end here -->

			<!-- footer section start from here -->
			<footer class="footer_section py-5">
				<div class="container">
					<div class="bg-pink py-5 rounded-2">
						<div class="row align-items-center">
							<div class="col-12 col-sm-12 col-md-12 col-lg-6 text-center">
								<div>
									<h3 class="text-white">Subscribe To Our Newsletter</h3>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-12 col-lg-6">
								<div class="d-flex justify-content-center">
									<form action="">
										<div class="input-group rounded-0">
											<input type="text" class="form-control rounded-0 py-3" placeholder="Recipient's username">
											<button class="input-group-text bg-dark text-white rounded-0" id="basic-addon2">Subscribe</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div>
								<h5 class="footer_heading">
									Cleon<span>HR</span>
								</h5>
								<p class="text-white">
									Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc vulputate libero et ve.
								</p>
								<p class="text-white">
									<span class="me-2 text-pink"><i class="fa-solid fa-phone"></i></span>+234-456-321-7890
								</p>
								<p class="text-white">
									<span class="me-2 text-pink"><i class="fa-solid fa-envelope"></i></span>info@whytecleon.com
								</p>
								<div>
									<ul class="list-unstyled footer_social_list">
										<li>
											<a href="#" class="text-decoration-none">
												<span>
													<i class="fa-brands fa-linkedin"></i>
												</span>
											</a>
										</li>
										<li>
											<a href="#" class="text-decoration-none">
												<span>
													<i class="fa-brands fa-instagram"></i>
												</span>
											</a>
										</li>
										<li>
											<a href="#" class="text-decoration-none">
												<span>
													<i class="fa-brands fa-facebook-f"></i>
												</span>
											</a>
										</li>
										<li>
											<a href="#" class="text-decoration-none">
												<span>
													<i class="fa-brands fa-twitter"></i>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div>
								<h5 class="text-capitalize text-white">our modules</h5>
								<ul class="list-unstyled footer_section_list">
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">E -Learning</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Cleon time</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Performance Management</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Cooperative</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">HMO</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Hr Module</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3">
							<div>
								<h5 class="text-capitalize text-white">hot links</h5>
								<ul class="list-unstyled footer_section_list">
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">About Us</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Our Team</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Pricing</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Case Studies</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Testimonials</span>
										</a>
									</li>
									<li>
										<a href="#" class="text-decoration-none">
											<span><img src="assets/images/pinkfooterstar.png" alt="" class="img-fluid"></span>
											<span class="ms-2">Support</span>
										</a>
									</li>
								</ul>
							</div>
						</div>
						<div class="col-12 col-sm-6 col-md-6 col-lg-3"></div>
					</div>
				</div>
			</footer>
			<!-- footer section end here -->
			<!-- js area -->
			<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
			<script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
			<script type="text/javascript" src="assets/js/app.js"></script>
			<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script>
			<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
			<script>
				$('#getStartedBtn').on('click', function() {
					$('#dropdownContent').toggle();
				});
			</script>
			<script>
				var swiper = new Swiper(".mySwiper", {
					slidesPerView: 'auto',
					centeredSlides: true,
					spaceBetween: 30,
					autoplay: true,
					pagination: {
						el: ".swiper-pagination",
						clickable: true,
					},
					breakpoints: {
						550: {
							slidesPerView: 1,
							spaceBetween: 20,
						},
						640: {
							slidesPerView: 2,
							spaceBetween: 20,
						},
						768: {
							slidesPerView: 2,
							spaceBetween: 30,
						},
						1024: {
							slidesPerView: 2,
							spaceBetween: 50,
						},
					},
				});
			</script>

		</body>
		</html>