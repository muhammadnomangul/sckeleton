@section('title')
Password | Admin Panel
@endsection
@include('admin.layout.header')
<!-- Main Content -->
<style>
    .add_more{
        color: #E82583;
        float: right;
        cursor: pointer;
    }
</style>
<script src="{{ asset('Ckeditor/ckeditor.js') }}"></script>
<div class="container">
    <h1 class="mb-4 text-primary font-weight-bold">Update Password</h1>
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="post" action='{{route("admin.Updatepassword")}}'>
                @csrf
                <div class="row questions">
                <div class="col-lg-12 row appended">
                   <div class="col-lg-12 mt-2 row">
					<div class="col-lg-12 padding_bottom">
						<label>Old Password</label>
						<div class="input-group bg-input mb-2 p-1">
							<input type="password" placeholder="Old Password" name="old_password" class="form-control">
						</div>
					</div>
				</div>
				<div class="col-lg-12 row">
					<div class="col-lg-12 padding_bottom">
						<label>New Password</label>
						<div class="input-group bg-input mb-2 p-1">
							<input type="password" placeholder="New Password" name="password" class="form-control">
						</div>
					</div>
				</div>
				<div class="col-lg-12 row">
					<div class="col-lg-12 padding_bottom">
						<label>Confirm Password</label>
						<div class="input-group bg-input mb-2 p-1">
							<input type="password" placeholder="Confirm Password" name="password_confirmation" class="form-control">
						</div>
					</div>
				</div>
        </div>
            <div class="col-lg-12 text-center">
                <button class="btn btn-sm btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
<footer class="sticky-footer bg-white"></footer>
</div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@include('admin.layout.footer')
