@section('title')
Manage Company Survey | Admin Panel
@endsection
@include('admin.layout.header')
<!-- Main Content -->
<style>
    .add_more{
        color: #E82583;
        float: right;
        cursor: pointer;
    }
</style>
<script src="{{ asset('Ckeditor/ckeditor.js') }}"></script>
<div class="container">
    <h1 class="mb-4 text-primary font-weight-bold">Manage Company Survey</h1>
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="post" action='{{route("admin.survey.store")}}'>
                @csrf
                <input type="hidden" value="company" name="type">
                <div class="row questions">
                    @forelse($surveys as $index=>$survey)
                    <div class="col-lg-12 row appended">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label>Question {{$index + 1}}</label>
                                <input class="form-control" value="{{$survey->question}}" required name="questions[{{$index}}][question]" placeholder="Question" type="text" required>
                                <span class="add_more more_option" data-id="{{$index}}">Add more Option</span>
                            </div>
                        </div>
                        @foreach($survey->options as $optionIndex=>$option)
                        <div class="col-lg-6">
                         <div class="form-group">
                            <label class="text-capitalize fs-14">Option</label>
                            <input class="form-control" value="{{$option->option}}" required name="questions[{{$index}}][options][]" placeholder="option" type="text" required>
                            @if($optionIndex > 1)
                            <span class="add_more remove_option">Remove</span>
                            @endif
                        </div>
                    </div>
                    @endforeach
                    @if($index > 0)
                    <div class="col-lg-12"><span class="add_more remove_ques">Remove Question</span></div>
                    @endif
                </div>
                @empty
                <div class="col-lg-12 row appended">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Question 1</label>
                            <input class="form-control" required name="questions[0][question]" placeholder="Question" type="text" required>
                            <span class="add_more more_option" data-id="0">Add more Option</span>
                        </div>
                    </div>
                    <div class="col-lg-6">
                     <div class="form-group">
                        <label class="text-capitalize fs-14">Option</label>
                        <input class="form-control" required name="questions[0][options][]" placeholder="option" type="text" required>
                    </div>
                </div>
                <div class="col-lg-6">
                 <div class="form-group">
                    <label class="text-capitalize fs-14">Option</label>
                    <input class="form-control" required name="questions[0][options][]" placeholder="option" type="text" required>
                </div>
            </div>
        </div>
        @endforelse
        <div class="col-lg-12"><span class="add_more more_question">Add More Question</div>
            <div class="col-lg-12 text-center">
                <button class="btn btn-sm btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
<footer class="sticky-footer bg-white"></footer>
</div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<script>
    CKEDITOR.replace('desc');
</script>
@section('js')
<script>
    $(document).on('click','.more_question', function() {
        var length = $('.appended').length;
        var ques = length + 1;
        $(this).parent().before(`<div class="col-lg-12 row appended">
            <div class="col-lg-12">
            <div class="form-group">
            <label>Question ${ques}</label>
            <input class="form-control" required name="questions[${length}][question]" placeholder="Question" type="text" required>
            <span class="add_more more_option" data-id="${length}">Add more Option</span>
            </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <label class="text-capitalize fs-14">option</label>
            <input class="form-control" required name="questions[${length}][options][]" placeholder="option" type="text" required>
            </div>
            </div>
            <div class="col-lg-6">
            <div class="form-group">
            <label class="text-capitalize fs-14">option</label>
            <input class="form-control" required name="questions[${length}][options][]" placeholder="option" type="text" required>
            </div>
            </div>
            <div class="col-lg-12"><span class="add_more remove_ques">Remove Question</span></div>
            </div>`);
    });
    $(document).on('click','.more_option',function(){
        var length = $(this).attr('data-id');
        $(this).closest('.row').append(`<div class="col-lg-6">
         <div class="form-group">
         <label class="text-capitalize fs-14">option</label>
         <input class="form-control" required name="questions[${length}][options][]" placeholder="option" type="text" required>
         <span class="add_more remove_option">Remove</span>
         </div>
         </div>`);
    })
    $(document).on('click','.remove_option',function(){
        $(this).closest('.col-lg-6').remove();
    })
    $(document).on('click','.remove_ques',function(){
        $(this).closest('.appended').remove();
    })
</script>
@endsection
@include('admin.layout.footer')
