 @foreach($claims as $claim)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$claim->employee_claim->user->c_name}}</td>
                                            <td>@foreach($countries->where('id', $claim->employee_claim->user->country) as $country)
                                                    {{ $country->name }}
                                                @endforeach</td>
                                            <td>{{$claim->user->name}} {{$claim->user->l_name}}</td>
                                            <td>{{$claim->user->email}}</td>
                                            <td>{{$claim->user->phone}}</td>
                                            <td>{{$claim->employee_claim->title}}</td>
                                            <td>2023-05-15</td>
                                            <td>${{$claim->t_hour * ($claim->employee_claim->value)}}</td>
                                            <td><span class="badge badge-warning">Pending</span></td>
                                            <td>{{$claim->created_at}}</td>
                                        </tr>
                                        @endforeach