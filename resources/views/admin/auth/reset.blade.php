<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Reset Password</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('admin_css/css/sb-admin-2.css')}}" rel="stylesheet">
    <style>
        .banner{
            background-image: url(admin_css/img/signinbanner.jpg);
            height: 300px;
            background-size: cover;
        }
        .login-form{
            margin-top: -194px;
        }
        .invalid-feedback{
            display: block !important;
        }
    </style>

</head>

<!-- <body class="bg-gradient-primary"> -->

    <div class="container-fluid">

        <section class="banner">
            <div class="row">
                <div class="col-lg-12">
                    <div >
                        here
                    </div>
                </div>
            </div>
        </section>
        <div class="row bg-primary py-5">
            <div class="col-lg-3 mx-auto">
                <div class="p-5 bg-white login-form">
                    <div class="text-center mb-4">
                        <img src="img/logo/logo.png" alt="">
                    </div>
                    <h3 class="text-center mb-4">Password Reset</h3>
                    <form class="user" method="POST" action="{{ route('password.update') }}">
                        @csrf
                        <input type="hidden" name="token" value="{{ $token }}">
                        <div class="form-group">
                          <input type="email" name="email" value="{{$email}}" class="form-control rounded-0 form-control-user" id="exampleInputEmail"
                          aria-describedby="emailHelp" placeholder="Enter Email Address..." required>
                          @error('email')
                          <span class="invalid-feedback"  role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control rounded-0 form-control-user" id="exampleInputPassword"
                        placeholder="New Password" required>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control rounded-0 form-control-user" id="exampleInputConfirmPassword"
                      placeholder="Confirm Password" name="password_confirmation" required>
                  </div>
                  <button type="submit" class="btn btn-primary btn-user btn-block">
                      Reset Password
                  </button>
              </form>


            <!-- <div class="text-center">
                <a class="small" href="forgot-password.html">Forgot Password?</a>
            </div>
            <div class="text-center">
                <a class="small" href="register.html">Create an Account!</a>
            </div> -->
        </div>
    </div>
</div>


<!-- </div> -->

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin-2.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> 


<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>



<script>
    @if (Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    @endif


    @if (Session::has('info'))
    toastr.info("{{ Session::get('info') }}");
    @endif


    @if (Session::has('warning'))
    toastr.warning("{{ Session::get('warning') }}");
    @endif
    @if (Session::has('message'))
    toastr.success("{{ Session::get('success') }}");
    @endif

    @if (Session::has('error'))
    toastr.error("{{ Session::get('error') }}");
    @endif
</script>

@if (session('toast_error'))
<script>
    var errors = {!! json_encode(session('toast_error')) !!};
    for (var i = 0; i < errors.length; i++) {
        toastr.error(errors[i], 'Validation Error');
    }
</script>
@endif
<script>

</script>

</body>

</html>


