<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Admin Login</title>
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">
    <link href="{{asset('admin_css/css/sb-admin-2.css')}}" rel="stylesheet">


</head>
<style>
   .invalid-feedback{
    display: block !important;
}
</style>
<body class="bg-gradient-primary">

    <div class="container">

        <!-- Outer Row -->
        <div class="row justify-content-center">

            <div class="col-xl-10 col-lg-12 col-md-9">

                <div class="card o-hidden border-0 shadow-lg my-5">
                    <div class="card-body p-0">
                        <!-- Nested Row within Card Body -->
                        <div class="row">
                            <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
                            <div class="col-lg-6">
                                <div class="p-5">
                                    <div class="text-center">
                                        <h1 class="h4 text-gray-900 mb-4">Welcome Back!</h1>
                                    </div>
                                    <form method="POST" action="{{route('admin.login')}}">
                                        @csrf
                                        <input type="hidden" name="admin" id="admin"
                                        value="admin">
                                        <div class="form-group">
                                            <input type="email" name="email" id="email" class="form-control form-control-user"
                                            id="exampleInputEmail" aria-describedby="emailHelp"
                                            placeholder="Enter Email Address...">
                                            @error('email')
                                            <span class="invalid-feedback"  role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <input type="password" name="password" class="form-control form-control-user"
                                            id="exampleInputPassword" placeholder="Password" required>
                                            @error('password')
                                            <span class="invalid-feedback"  role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-checkbox small">
                                                <input type="checkbox" class="custom-control-input" id="customCheck">
                                                <label class="custom-control-label" for="customCheck">Remember
                                                Me</label>
                                            </div>
                                        </div>
                                        <button class=" submit_form btn btn-primary btn-user btn-block">
                                            Login
                                        </button>
                                    </form>
                                    <hr>
                                    <a href="{{ url('auth/google') }}" class="btn btn-google btn-user btn-block">
                                        <i class="fab fa-google fa-fw"></i> Login with Google
                                    </a>
                                    <a href="{{ url('auth/facebook') }}" class="btn btn-facebook btn-user btn-block">
                                        <i class="fab fa-facebook-f fa-fw"></i> Login with Facebook
                                    </a>
                                    <hr>
                                    <div class="text-center">
                                        <a class="small" href="{{route('admin.forget')}}">Forgot Password?</a>
                                    </div>
                              <!--       <div class="text-center">
                                        <a class="small" href="register.html">Create an Account!</a>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>

    </div>

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('admin_css/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('admin_css/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('admin_css/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <!-- <script src="{{asset('admin_css/js/sb-admin-2.min.js')}}"></script> -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> 


    <link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>



    <script>
        @if (Session::has('success'))
        toastr.success("{{ Session::get('success') }}");
        @endif


        @if (Session::has('info'))
        toastr.info("{{ Session::get('info') }}");
        @endif


        @if (Session::has('warning'))
        toastr.warning("{{ Session::get('warning') }}");
        @endif
        @if (Session::has('message'))
        toastr.success("{{ Session::get('success') }}");
        @endif

        @if (Session::has('error'))
        toastr.error("{{ Session::get('error') }}");
        @endif
    </script>

    @if (session('toast_error'))
    <script>
        var errors = {!! json_encode(session('toast_error')) !!};
        for (var i = 0; i < errors.length; i++) {
            toastr.error(errors[i], 'Validation Error');
        }
    </script>
    @endif
    <script>

        $('#reset_password').submit(function(e) {
            e.preventDefault(); 
            var email = $('#email').val();
            $.ajax({
                type: 'GET',
                url: '{{route("check.user")}}',
                data: {
                    email: email,
                },
                success: function(response) {
                    if ( response === 'admin') {
                        $('#reset_password').off('submit').submit();
                    } else {
                        toastr.error('Email not found or user is not a admin');
                    }
                },
                error: function() {
                    alert('An error occurred during the check');
                }
            });
        });

    </script>

</body>

</html>