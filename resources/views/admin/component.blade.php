<h4 class="claim_heading">Claims Type</h4>
@forelse($claims as $claim)
    <li class="search_item"><a href="{{ url('admin/claim/type?id='.$claim->id) }}">{{ $claim->title }}</a></li>
@empty
<h6 class="no_claim_heading">No Record Found</h6>
@endforelse
<h4 class="claim_heading">Claims Request</h4>
@forelse($declinedclaims as $declinedclaim)
<li class="search_item"><a href="{{url('admin/manage/claim?id='.$declinedclaim->id)}}">{{$declinedclaim->employee_claim->title}}</a></li>
@empty
<h6 class="no_claim_heading">No Record Found</h6>
@endforelse
<h4 class="claim_heading">Approved Claims</h4>
@forelse($approvedclaims as $claimed)
<li class="search_item"><a href="{{url('admin/approved/claim?id='.$claimed->id)}}">{{$claimed->employee_claim->title}}</a></li>
@empty
<h6 class="no_claim_heading">No Record Found</h6>
@endforelse