@section('title')
CLAIM TYPE MANAGEMENT | Admin Panel
@endsection
@include('admin.layout.header')
<div class="container-fluid">
  <h1 class="mb-4 text-primary font-weight-bold">CLAIM TYPE MANAGEMENT</h1>
  <div class="card shadow mb-4">
    <div class="card-body">
      <div class="row mb-5">
        <div class="col-lg-5">
          <div class="row">
            <div class="col-lg-6">
              <button class="btn btn-primary mb-2 w-100 mr-3" data-toggle="modal"
              data-target="#AddClaim">
              Add Claim Type
            </button>
          </div>
          <div class="col-lg-6">
            <button class="btn w-100 btn-primary mr-3" id="downloadButton" >
              Export Raw Data
            </button>
          </div>
        </div>
      </div>
      <div class="col-lg-2">
        <div class="text-center my-lg-0 my-3">
          <p class="mb-0">Total Candidates: 350</p>
        </div>
      </div>
      <div class="col-lg-5">
        <div class="row">
          <div class="col-lg-6">
            <div class="form-group mb-0 d-flex">
              <select class="form-control mb-3" id="dateFilter">
                <option disabled selected value="all">Filter by</option>
                <option value="last24">Last 24 Hours</option>
                <option value="last7">Last 7 Days</option>
                <option value="last30">Last 30 Days</option>
              </select>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="">
             <input placeholder="Custom Calendar" class="form-control" type="date" id="date">
           </div>
         </div>
       </div>
     </div>
   </div>
   <div class="table-responsive">

    <table class="table table-bordered data-table" id="Table" width="100%" cellspacing="0">
      <thead>
        <tr>
          <th>SN</th>
          <th>Company Name</th>
          <th>Country</th>
          <th>Claim Title</th>
          <th>Schedule</th>
          <th>Payment Value</th>
          <th>Window Start</th>
          <th>Window End</th>
          <th>Created By</th>
          <th>Last Updated</th>
        </tr>
      </thead>
      <tbody class="appending">
        @if(isset($claims))
        @foreach($claims as $claim)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{$claim->user->c_name}}</td>
          <td>
         @foreach($countries->where('id', $claim->user->country) as $country)
    {{ $country->name }}
@endforeach
              </td>
          <td>{{$claim->title}}</td>
          <td>{{$claim->schedule}}</td>
          <td>{{$claim->value}}</td>
          <td>{{$claim->time_start}}</td>
          <td>{{$claim->time_end}}</td>
          <td>{{$claim->user->name}}</td>
          <td>{{$claim->updated_at->format('Y-m-d')}}</td>
        </tr>
        @endforeach
        @else
        <tr>
          <td>1</td>
          <td>{{$claim->user->c_name}}</td>
          <td>
         @foreach($countries->where('id', $claim->user->country) as $country)
    {{ $country->name }}
@endforeach
              </td>
          <td>{{$claim->title}}</td>
          <td>{{$claim->schedule}}</td>
          <td>{{$claim->value}}</td>
          <td>{{$claim->time_start}}</td>
          <td>{{$claim->time_end}}</td>
          <td>{{$claim->user->name}}</td>
          <td>{{$claim->updated_at->format('Y-m-d')}}</td>
        </tr>
        @endif
      </tbody>
    </table>


  </div>
</div>
</div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white"></footer>
<!-- End of Footer -->
</div>
<!-- End of Content Wrapper -->
</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
  <i class="fas fa-angle-up"></i>
</a>

<!-- create new user modal -->
<div class="modal fade" id="AddClaim" tabindex="-1" role="dialog" aria-labelledby="AddClaimLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="AddClaimLabel">Add Claim Type</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form method="POST" action="{{url('admin/create/claim')}}">
        @csrf
        <div class="form-group">
          <label for="company">Select Company</label>
          <select class="form-control" id="company" name="company_id">
           @foreach($comapnies as $company)
           <option value="{{$company->id}}">{{$company->name}}</option>
           @endforeach
         </select>
       </div>
       <div class="form-group">
        <label for="claimTitle">Enter Claim Title</label>
        <input type="text" name="title" class="form-control" id="claimTitle" required/>
      </div>
      <div class="form-group">
        <label for="claimSchedule">Select Claim Schedule</label>
        <select class="form-control" id="claimSchedule" name="schedule" required>
          <option value="hourly">Hourly</option>
          <option value="daily">Daily</option>
          <option value="weekly">Weekly</option>
          <option value="monthly">Monthly</option>
        </select>
      </div>
      <div class="form-group">
        <label for="paymentPerSchedule">Enter Payment Per Schedule</label>
        <input type="text" class="form-control" name="value" id="paymentPerSchedule" required/>
      </div>
      <div class="form-group">
        <label for="paymentPerSchedule">Description</label>
        <textarea type="text" class="form-control" name="desc" id="paymentPerSchedule" required/></textarea>
      </div>
      <div class="form-group">
        <label for="windowStart">Select Window Start (Calendar)</label>
        <input type="date" class="form-control from" min="{{$today}}" name="start_date" id="windowStart" required/>
      </div>
      <div class="form-group">
        <label for="windowEnd">Select Window End (Calendar)</label>
        <input type="date" class="form-control to" id="windowEnd" value="end_date" required/>
      </div>

  </div>
  <div class="modal-footer">

    <button type="submit" class="btn btn-primary">Add Claim Type</button>

    <button type="button" class="btn btn-secondary" data-dismiss="modal">
      Close
    </button>
  </div>
      </form>

</div>
</div>
</div>



<!-- create new user modal -->
<div class="modal fade" id="viewprofile" tabindex="-1" role="dialog" aria-labelledby="viewprofileLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="viewprofileLabel">User detials</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <table class="table">
        <tr>
          <th>SN</th>
          <td id="modal-sn">1</td>
        </tr>
        <tr>
          <th>First Name</th>
          <td id="modal-firstname">Abdullahi</td>
        </tr>
        <tr>
          <th>Last Name</th>
          <td id="modal-lastname">Abubakar</td>
        </tr>
        <tr>
          <th>Email</th>
          <td id="modal-email">abdullahi@example.com</td>
        </tr>
        <tr>
          <th>Phone</th>
          <td id="modal-phone">+234 809-123-4567</td>
        </tr>
        <tr>
          <th>Country</th>
          <td id="modal-country">Nigeria</td>
        </tr>
        <tr>
          <th>Authentication</th>
          <td id="modal-authentication">Email</td>
        </tr>
        <tr>
          <th>2FA Status</th>
          <td id="modal-2fa-status">On</td>
        </tr>
        <tr>
          <th>Created On</th>
          <td id="modal-created-on">2022-01-01</td>
        </tr>
        <tr>
          <th>Last Login</th>
          <td id="modal-last-login">2022-01-15 10:00:00
          </td>
        </tr>
      </table>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">
        Close
      </button>

    </div>
  </div>
</div>
</div>

<!-- Confirmation modal -->
<div class="modal fade" id="suspendModal" tabindex="-1" role="dialog" aria-labelledby="suspendModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="suspendModalLabel">Suspend User Confirmation</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <p>Are you sure you want to suspend this user?</p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
      <button type="button" class="btn btn-danger">Suspend User</button>
    </div>
  </div>
</div>
</div>

@section('js')
<script>

  $('#date').on('input', function() {
    var date = $(this).val();
    filter(date);
  });

  $('#dateFilter').on('change',function(){
   var date = $(this).val();
   filter(date);
 })
  function filter(date){
    $.ajax({
      url : '{{route("admin.typefilter")}}',
      method : 'GET',
      data : {
        date : date,
      },
      success:function(response){
        console.log(response.component);
        $('.appending').empty().append(response.component);
      }
    })
  }
    $(document).on('input','.from',function(){
    var val = $(this).val();
    $('.to').attr('min',val);
  })
  $(document).on('input','.to',function(){
    var val = $('.from').val();
    if(!val){
      toastr.error('Please Select Start Date First');
      $(this).val('');
    }
  })
</script>
@endsection

@include('admin.layout.footer')