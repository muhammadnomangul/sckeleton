                  @foreach($claims as $claim)
                      <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$claim->user->c_name}}</td>
                        <td>@foreach($countries->where('id', $claim->user->country) as $country)
                                                    {{ $country->name }}
                                                @endforeach</td>
                        <td>{{$claim->title}}</td>
                        <td>{{$claim->schedule}}</td>
                        <td>{{$claim->value}}</td>
                        <td>{{$claim->time_start}}</td>
                        <td>{{$claim->time_end}}</td>
                        <td>{{$claim->user->name}}</td>
                        <td>{{$claim->updated_at->format('Y-m-d')}}</td>
                      </tr>
                  @endforeach