@include('admin.layout.header')
<div class="container-fluid">
  <h1 class="mb-4 text-primary font-weight-bold">CLAIM TYPE MANAGEMENT</h1>
  <div class="card shadow mb-4">
    <div class="card-body">
     <div class="table-responsive">

      <table class="table table-bordered data-table" id="Table" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>SN</th>
            <th>Company Name</th>
            <th>Country</th>
            <th>Claim Title</th>
            <th>Schedule</th>
            <th>Payment Value</th>
            <th>Window Start</th>
            <th>Window End</th>
            <th>Created By</th>
            <th>Last Updated</th>
          </tr>
        </thead>
        <tbody class="appending">
          @foreach($typeclaims as $type)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$type->user->c_name}}</td>
            <td>
             @foreach($countries->where('id', $type->user->country) as $country)
             {{ $country->name }}
             @endforeach
           </td>
           <td>{{$type->title}}</td>
           <td>{{$type->schedule}}</td>
           <td>{{$type->value}}</td>
           <td>{{$type->time_start}}</td>
           <td>{{$type->time_end}}</td>
           <td>{{$type->user->name}}</td>
           <td>{{$type->updated_at->format('Y-m-d')}}</td>
         </tr>
         @endforeach
       </tbody>
     </table>


   </div>
 </div>
</div>
</div>
<div class="container-fluid">
  <h1 class="mb-4 text-primary font-weight-bold">MANAGE CLAIM REQUESTS
  </h1>
  <div class="card shadow mb-4">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered data-table" id="Table1" width="100%"
        cellspacing="0">
        <thead>
          <tr>
            <th>SN</th>
            <th>Company Name</th>
            <th>Country</th>
            <th>Employee Full Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Claim Title</th>
            <th>Payment Value</th>
            <th>Approval Status</th>
            <th>Timestamp</th>
          </tr>
        </thead>
        <tbody class="appending">
          @foreach($requests as $request)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$request->employee_claim->user->c_name}}</td>
            <td>@foreach($countries->where('id', $request->employee_claim->user->country) as $country)
              {{ $country->name }}
            @endforeach</td>
            <td>{{$request->user->name}} {{$request->user->l_name}}</td>
            <td>{{$request->user->email}}</td>
            <td>{{$request->user->phone}}</td>
            <td>{{$request->employee_claim->title}}</td>
            <td>${{$request->t_hour * ($request->employee_claim->value)}}</td>
            <td><span>{{$request->status}}</span></td>
            <td>{{$request->created_at}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<div class="container-fluid">
  <h1 class="mb-4 text-primary font-weight-bold">CLAIM APPROVAL LOG </h1>
  <div class="card shadow mb-4">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered " id="Table2" width="100%"
        cellspacing="0">
        <thead>
          <tr>
            <th>SN</th>
            <th>Company Name</th>
            <th>Country</th>
            <th>Employee Full Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Claim Title</th>
            <th>Approved By</th>
            <th>Timestamp</th>
          </tr>
        </thead>
        <tbody class="appending"> 
          @foreach($claims as $claim)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$claim->employee_claim->user->c_name}}</td>
            <td>@foreach($countries->where('id', $claim->employee_claim->user->country) as $country)
              {{ $country->name }}
            @endforeach</td>
            <td>{{$claim->user->name}} {{$claim->user->l_name}}</td>
            <td>{{$claim->user->email}}</td>
            <td>{{$claim->user->phone}}</td>
            <td>{{$claim->employee_claim->title}}</td>
            <td>{{$claim->action_by}}</td>
            <td>{{$claim->created_at}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
</div>
<footer class="sticky-footer bg-white"></footer>
</div>
</div>

@include('admin.layout.footer')