@section('title')
Dashboard | Admin Panel
@endsection
@include('admin.layout.header')
<!-- Begin Page Content -->
<div class="container-fluid">

    <h1 class="mb-3 text-primary mb-3">Cleontime Dashboard</h1>
    <div class="row mb-lg-0 mb-3">
        <div class="col-lg-3 col-md-6">
            <div class="form-group mb-0 d-flex">
                <select class="form-control mb-3" id="dateFilter">
                    <option value="today">Last 24 Hours</option>
                    <option value="week">Last 7 Days</option>
                    <option value="month">Last 30 Days</option>
                </select>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="">
                <input  placeholder="Custom Calendar" class="form-control" type="text" onfocus="(this.type='date')" id="date" />

            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-download fa-sm text-white-50 "></i> download Raw Data </a>
            </div>
            <div class="col-lg-3 col-md-6">
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-download fa-sm text-white-50"></i> Print as PDF</a>
                </div>

            </div>
        </div>


        <!-- Content Row -->
        <div class="row text-center">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-3 col-md-6 mb-4">
                        <div class="card border-left-primary shadow h-100 py-2">
                            <div class="card-body">
                                <div class="row no-gutters align-items-center">
                                    <div class="col mr-2">
                                        <div
                                        class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                        Total Subscribers
                                    </div>
                                    <div class="h5 mb-0 font-weight-bold text-gray-800">50</div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-3 col-md-6 mb-4">
                    <div class="card border-left-primary shadow h-100 py-2">
                        <div class="card-body">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                    <div
                                    class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                    Total Partners
                                </div>
                                <div class="h5 mb-0 font-weight-bold text-gray-800 partners"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card border-left-primary shadow h-100 py-2">
                    <div class="card-body">
                        <div class="row no-gutters align-items-center">
                            <div class="col mr-2">
                                <div
                                class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                Total Claim Types

                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800 claim_types"></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>



        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div
                            class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                            Total Claim Requests
                        </div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800 requested_claims"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div
                        class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                        Total Approved Claims

                    </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800 approved_claims">
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div
                    class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    Total Declined Claims
                </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800 declined_claims">
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div
                    class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    Total Claim Requested Amount
                </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800 total_claim_amount">
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div
                    class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    Total Approved Claims Amount
                </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800 approved_claim_amount">
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div
                    class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    Total Declined Claims Amount

                </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800 declined_claim_amount">
                </div>
            </div>

        </div>
    </div>
</div>
</div>

<!-- FIRST EMPTY BOX -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div
                    class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    ***Total***
                </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">000</div>
            </div>

        </div>
    </div>
</div>
</div>
<!-- SECOND EMPTY BOX -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div
                    class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    ***Total***
                </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">000</div>
            </div>

        </div>
    </div>
</div>
</div>
<!-- THIRD EMPTY BOX -->
<div class="col-xl-3 col-md-6 mb-4">
    <div class="card border-left-primary shadow h-100 py-2">
        <div class="card-body">
            <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                    <div
                    class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                    ***Total***
                </div>
                <div class="h5 mb-0 font-weight-bold text-gray-800">000</div>
            </div>

        </div>
    </div>
</div>
</div>





</div>

<div class="row g-3 mt-2">


    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Total Approved Claims Vs Total Declined Claims

                </h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <canvas id="claims-chart"
                    style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>

            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Total Approved Claims Amount Vs Total Declined Claims Amount


                </h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <canvas id="claims-amount"
                    style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>

            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">  Total Claim Requests

                </h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class="chartjs-size-monitor-expand">
                            <div class=""></div>
                        </div>
                        <div class="chartjs-size-monitor-shrink">
                            <div class=""></div>
                        </div>
                    </div>
                    <canvas id="myAreaChart"
                    style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>

            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">  Total Approved Claims
                </h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class=""></div>
                    </div>
                    <canvas id="failedSignupChart"
                    style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>
            </div>
        </div>

    </div>



    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">  Total Declined Claims</h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class=""></div>
                        <div class=""></div>
                    </div>
                    <canvas id="DeclinedClaims"
                    style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>
            </div>
        </div>
    </div>




    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">  Total Claim Requested Amount</h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class=""></div>
                        <div class=""></div>
                    </div>
                    <canvas id="ClaimRequestedAmount"
                    style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>
            </div>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">  Total Approved Claims Amount</h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class=""></div>
                        <div class=""></div>
                    </div>
                    <canvas id="Approvedclaim"
                    style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>
            </div>
        </div>
    </div>


    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">  Total Declined Claims Amount
                </h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class=""></div>
                        <div class=""></div>
                    </div>
                    <canvas id="ClaimsAmount" style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">  Total Subscribers
                </h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class=""></div>
                        <div class=""></div>
                    </div>
                    <canvas id="SubscribersTrend" style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>
            </div>
        </div>
    </div>

    <div class="col-lg-6">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">  Total Partners

                </h6>
            </div>
            <div class="card-body">
                <div class="chart-area">
                    <div class="chartjs-size-monitor">
                        <div class=""></div>
                        <div class=""></div>
                    </div>
                    <canvas id="PartnersTrend" style="display: block; width: 653px; height: 320px;" width="653"
                    height="320" class="chartjs-render-monitor"></canvas>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
</div>

<!-- Content Row -->



</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
        <div class="copyright text-center my-auto">

        </div>
    </div>
</footer>
<!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

@section('js')
<script>
    $(document).ready(function(){
        var data = "today";
        custom_chart(data);
        update_chart();
    })
    $('#dateFilter').on('change' , function(){
        var val = $(this).val();
        custom_chart(val);
    })
    var delayTimer;

    $('#date').on('input', function() {
        var val = $(this).val();
        clearTimeout(delayTimer);
        delayTimer = setTimeout(function() {
            var date = new Date(val);
            if (!isNaN(date.getTime())) {
                custom_chart(val);
            }
    }, 500); // Half a second (500 milliseconds)
    });

    function custom_chart(data){
        $.ajax({
            url: '{{route("admin.custom_data")}}',
            method: 'GET',
            data : {
                data : data,
            },
            success: function(response) {
                $('.partners').text(response.partners);
                $('.approved_claims').text(response.approved_claims);
                $('.declined_claims').text(response.declined_claims);
                $('.requested_claims').text(response.requested_claims);
                $('.claim_types').text(response.claim_types);
                $('.approved_claim_amount').text(response.approved_claim_amount);
                $('.declined_claim_amount').text(response.declined_claim_amount);
                $('.total_claim_amount').text(response.total_claim_amount);
            }
        })
    }
    function update_chart(){
        $.ajax({
            url: '{{route("admin.chart_data")}}',
            method: 'GET',
            success: function(response) {
                countChart([response.approved_claims, response.declined_claims]);
                claims_amount([response.approved_claim_amount, response.declined_claim_amount]);
                claim_request(response.yearly_request);
                claim_approved(response.yearly_approved);
                declined_claim(response.yearly_declined);
                yearly_claim_value(response.yearly_claim_vlaue);
                yearly_approve_value(response.yearly_approve_vlaue);
                yearly_decline_value(response.yearly_decline_vlaue);
                partner(response.partners);
            }
        })
    }


    function countChart(data) {
        var ctx = document.getElementById('claims-chart').getContext('2d');

    var total = data[0] + data[1]; // Calculate the total

    // Calculate percentages
    var approvedPercentage = (data[0] / total) * 100;
    var declinedPercentage = (data[1] / total) * 100;

    var updatedData = {
        labels: ['Total Approved Claims', 'Total Declined Claims'],
        datasets: [{
            data: [approvedPercentage, declinedPercentage], // Use the calculated percentages
            backgroundColor: ['#28a745', '#dc3545']
        }]
    };

    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: updatedData,
        options: {
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function (value) {
                            return value + '%';
                        }
                    }
                }]
            }
        }
    });
}
function claims_amount(data) {
    var ctx = document.getElementById('claims-amount').getContext('2d');

    var total = data[0] + data[1]; // Calculate the total

    // Calculate percentages
    var approvedPercentage = (data[0] / total) * 100;
    var declinedPercentage = (data[1] / total) * 100;

    var updatedData = {
        labels: ['Total Approved Claims', 'Total Declined Claims'],
        datasets: [{
            data: [approvedPercentage, declinedPercentage], // Use the calculated percentages
            backgroundColor: ['#28a745', '#dc3545']
        }]
    };

    var myChart = new Chart(ctx, {
        type: 'horizontalBar',
        data: updatedData,
        options: {
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    ticks: {
                        beginAtZero: true,
                        callback: function (value) {
                            return value + '%';
                        }
                    }
                }]
            }
        }
    });
}
function claim_request(data) {
   var ctx = document.getElementById("myAreaChart");
   var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Total Request Claims : ",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: data,
      }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
      },
      gridLines: {
          display: false,
          drawBorder: false
      },
      ticks: {
          maxTicksLimit: 7
      }
  }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
        }
    },
    gridLines: {
      color: "rgb(234, 236, 244)",
      zeroLineColor: "rgb(234, 236, 244)",
      drawBorder: false,
      borderDash: [2],
      zeroLineBorderDash: [2]
  }
}],
  },
  legend: {
      display: false
  },
  tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel  + number_format(tooltipItem.yLabel);
      }
  }
}
}
});

}

function claim_approved(data){
    var ctx = document.getElementById("failedSignupChart");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Approve Claims : ",
          lineTension: 0.3,
          backgroundColor: "rgba(255, 99, 132, 0.05)",
          borderColor: "rgba(255, 99, 132, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(255, 99, 132, 1)",
          pointBorderColor: "rgba(255, 99, 132, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(255, 99, 132, 1)",
          pointHoverBorderColor: "rgba(255, 99, 132, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: data,
      }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
      },
      gridLines: {
          display: false,
          drawBorder: false
      },
      ticks: {
          maxTicksLimit: 7
      }
  }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          callback: function(value, index, values) {
            return  number_format(value);
        }
    },
    gridLines: {
      color: "rgb(234, 236, 244)",
      zeroLineColor: "rgb(234, 236, 244)",
      drawBorder: false,
      borderDash: [2],
      zeroLineBorderDash: [2]
  }
}],
  },
  legend: {
      display: false
  },
  tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel  + number_format(tooltipItem.yLabel);
      }
  }
}
}
});

}
function declined_claim(data){
    var ctx = document.getElementById("DeclinedClaims");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Declined Claims : ",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: data,
      }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
      },
      gridLines: {
          display: false,
          drawBorder: false
      },
      ticks: {
          maxTicksLimit: 7
      }
  }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
        }
    },
    gridLines: {
      color: "rgb(234, 236, 244)",
      zeroLineColor: "rgb(234, 236, 244)",
      drawBorder: false,
      borderDash: [2],
      zeroLineBorderDash: [2]
  }
}],
  },
  legend: {
      display: false
  },
  tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel  + number_format(tooltipItem.yLabel);
      }
  }
}
}
});

}
function yearly_claim_value(data){
    var ctx = document.getElementById("ClaimRequestedAmount");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Total Claim Amount : ",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: data,
      }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
      },
      gridLines: {
          display: false,
          drawBorder: false
      },
      ticks: {
          maxTicksLimit: 7
      }
  }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
        }
    },
    gridLines: {
      color: "rgb(234, 236, 244)",
      zeroLineColor: "rgb(234, 236, 244)",
      drawBorder: false,
      borderDash: [2],
      zeroLineBorderDash: [2]
  }
}],
  },
  legend: {
      display: false
  },
  tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel  + number_format(tooltipItem.yLabel);
      }
  }
}
}
});
}

function yearly_approve_value(data){
    var ctx = document.getElementById("Approvedclaim");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Approved Claim Amount : ",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: data,
      }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
      },
      gridLines: {
          display: false,
          drawBorder: false
      },
      ticks: {
          maxTicksLimit: 7
      }
  }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
        }
    },
    gridLines: {
      color: "rgb(234, 236, 244)",
      zeroLineColor: "rgb(234, 236, 244)",
      drawBorder: false,
      borderDash: [2],
      zeroLineBorderDash: [2]
  }
}],
  },
  legend: {
      display: false
  },
  tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel  + number_format(tooltipItem.yLabel);
      }
  }
}
}
});

}

function yearly_decline_value(data){
    var ctx = document.getElementById("ClaimsAmount");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Declined Claim Amount : ",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data: data,
      }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
      },
      gridLines: {
          display: false,
          drawBorder: false
      },
      ticks: {
          maxTicksLimit: 7
      }
  }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
        }
    },
    gridLines: {
      color: "rgb(234, 236, 244)",
      zeroLineColor: "rgb(234, 236, 244)",
      drawBorder: false,
      borderDash: [2],
      zeroLineBorderDash: [2]
  }
}],
  },
  legend: {
      display: false
  },
  tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel  + number_format(tooltipItem.yLabel);
      }
  }
}
}
});
}
function partner(data){
    var ctx = document.getElementById("PartnersTrend");
    var myLineChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        datasets: [{
          label: "Partners",
          lineTension: 0.3,
          backgroundColor: "rgba(78, 115, 223, 0.05)",
          borderColor: "rgba(78, 115, 223, 1)",
          pointRadius: 3,
          pointBackgroundColor: "rgba(78, 115, 223, 1)",
          pointBorderColor: "rgba(78, 115, 223, 1)",
          pointHoverRadius: 3,
          pointHoverBackgroundColor: "rgba(78, 115, 223, 1)",
          pointHoverBorderColor: "rgba(78, 115, 223, 1)",
          pointHitRadius: 10,
          pointBorderWidth: 2,
          data:data,
      }],
    },
    options: {
        maintainAspectRatio: false,
        layout: {
          padding: {
            left: 10,
            right: 25,
            top: 25,
            bottom: 0
        }
    },
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
      },
      gridLines: {
          display: false,
          drawBorder: false
      },
      ticks: {
          maxTicksLimit: 7
      }
  }],
      yAxes: [{
        ticks: {
          maxTicksLimit: 5,
          padding: 10,
          // Include a dollar sign in the ticks
          callback: function(value, index, values) {
            return  number_format(value);
        }
    },
    gridLines: {
      color: "rgb(234, 236, 244)",
      zeroLineColor: "rgb(234, 236, 244)",
      drawBorder: false,
      borderDash: [2],
      zeroLineBorderDash: [2]
  }
}],
  },
  legend: {
      display: false
  },
  tooltips: {
      backgroundColor: "rgb(255,255,255)",
      bodyFontColor: "#858796",
      titleMarginBottom: 10,
      titleFontColor: '#6e707e',
      titleFontSize: 14,
      borderColor: '#dddfeb',
      borderWidth: 1,
      xPadding: 15,
      yPadding: 15,
      displayColors: false,
      intersect: false,
      mode: 'index',
      caretPadding: 10,
      callbacks: {
        label: function(tooltipItem, chart) {
          var datasetLabel = chart.datasets[tooltipItem.datasetIndex].label || '';
          return datasetLabel  + number_format(tooltipItem.yLabel);
      }
  }
}
}
});

}
</script>
@endsection

@include('admin.layout.footer')