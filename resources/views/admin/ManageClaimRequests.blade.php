@section('title')
MANAGE CLAIM REQUESTS | Admin Panel
@endsection
@include('admin.layout.header')
<div class="container-fluid">
    <h1 class="mb-4 text-primary font-weight-bold">MANAGE CLAIM REQUESTS
    </h1>
    <div class="card shadow mb-4">
        <div class="card-body">
            <div class="row mb-5">
                <div class="col-lg-5">
                    <div class="row">
                        <div class="col-lg-6">
                            <button class="btn w-100 btn-primary mr-3" id="downloadButton" >
                                Export Raw Data
                            </button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group mb-0 d-flex">
                                <select class="form-control mb-3" id="dateFilter">
                                    <option disabled selected value="all">Filter by</option>
                                    <option value="last24">Last 24 Hours</option>
                                    <option value="last7">Last 7 Days</option>
                                    <option value="last30">Last 30 Days</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="">
                                <input placeholder="Custom Calendar" class="form-control" type="date" id="date">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered data-table" id="Table" width="100%"
                cellspacing="0">
                <thead>
                    <tr>
                        <th>SN</th>
                        <th>Company Name</th>
                        <th>Country</th>
                        <th>Employee Full Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Claim Title</th>
                        <th>Payment Value</th>
                        <th>Approval Status</th>
                        <th>Timestamp</th>
                    </tr>
                </thead>
                <tbody class="appending">
                    @if(isset($claims))
                    @foreach($claims as $claim)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$claim->employee_claim->user->c_name}}</td>
                        <td>@foreach($countries->where('id', $claim->employee_claim->user->country) as $country)
                            {{ $country->name }}
                        @endforeach</td>
                        <td>{{$claim->user->name}} {{$claim->user->l_name}}</td>
                        <td>{{$claim->user->email}}</td>
                        <td>{{$claim->user->phone}}</td>
                        <td>{{$claim->employee_claim->title}}</td>
                        <td>${{$claim->t_hour * ($claim->employee_claim->value)}}</td>
                        <td><span>{{$claim->status}}</span></td>
                        <td>{{$claim->created_at}}</td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td>1</td>
                        <td>{{$claim->employee_claim->user->c_name}}</td>
                        <td>@foreach($countries->where('id', $claim->employee_claim->user->country) as $country)
                            {{ $country->name }}
                        @endforeach</td>
                        <td>{{$claim->user->name}} {{$claim->user->l_name}}</td>
                        <td>{{$claim->user->email}}</td>
                        <td>{{$claim->user->phone}}</td>
                        <td>{{$claim->employee_claim->title}}</td>
                        <td>${{$claim->t_hour * ($claim->employee_claim->value)}}</td>
                        <td><span>{{$claim->status}}</span></td>
                        <td>{{$claim->created_at}}</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
</div>
<footer class="sticky-footer bg-white"></footer>
</div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>

<!-- create new user modal -->
<div class="modal fade" id="AddClaim" tabindex="-1" role="dialog" aria-labelledby="AddClaimLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="AddClaimLabel">Add Claim Type/h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="company">Select Company</label>
                        <select class="form-control" id="company">
                            <option>Company 1</option>
                            <option>Company 2</option>
                            <option>Company 3</option>
                            <option>Company 4</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="claimTitle">Enter Claim Title</label>
                        <input type="text" class="form-control" id="claimTitle" />
                    </div>
                    <div class="form-group">
                        <label for="claimSchedule">Select Claim Schedule</label>
                        <select class="form-control" id="claimSchedule">
                            <option>Hourly</option>
                            <option>Daily</option>
                            <option>Weekly</option>
                            <option>Monthly</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="paymentPerSchedule">Enter Payment Per Schedule</label>
                        <input type="text" class="form-control" id="paymentPerSchedule" />
                    </div>
                    <div class="form-group">
                        <label for="windowStart">Select Window Start (Calendar)</label>
                        <input type="date" class="form-control" id="windowStart" />
                    </div>
                    <div class="form-group">
                        <label for="windowEnd">Select Window End (Calendar)</label>
                        <input type="date" class="form-control" id="windowEnd" />
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">Add Claim Type</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="viewprofile" tabindex="-1" role="dialog" aria-labelledby="viewprofileLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="viewprofileLabel">User detials</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <table class="table">
                <tr>
                    <th>SN</th>
                    <td id="modal-sn">1</td>
                </tr>
                <tr>
                    <th>First Name</th>
                    <td id="modal-firstname">Abdullahi</td>
                </tr>
                <tr>
                    <th>Last Name</th>
                    <td id="modal-lastname">Abubakar</td>
                </tr>
                <tr>
                    <th>Email</th>
                    <td id="modal-email">abdullahi@example.com</td>
                </tr>
                <tr>
                    <th>Phone</th>
                    <td id="modal-phone">+234 809-123-4567</td>
                </tr>
                <tr>
                    <th>Country</th>
                    <td id="modal-country">Nigeria</td>
                </tr>
                <tr>
                    <th>Authentication</th>
                    <td id="modal-authentication">Email</td>
                </tr>
                <tr>
                    <th>2FA Status</th>
                    <td id="modal-2fa-status">On</td>
                </tr>
                <tr>
                    <th>Created On</th>
                    <td id="modal-created-on">2022-01-01</td>
                </tr>
                <tr>
                    <th>Last Login</th>
                    <td id="modal-last-login">2022-01-15 10:00:00
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                Close
            </button>

        </div>
    </div>
</div>
</div>
<div class="modal fade" id="suspendModal" tabindex="-1" role="dialog" aria-labelledby="suspendModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="suspendModalLabel">Suspend User Confirmation</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>Are you sure you want to suspend this user?</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="button" class="btn btn-danger">Suspend User</button>
        </div>
    </div>
</div>
</div>

@section('js')
<script>

    $('#date').on('input', function() {
        var date = $(this).val();
        filter(date);
    });

    $('#dateFilter').on('change',function(){
       var date = $(this).val();
       filter(date);
   })
    function filter(date){
        var value = "pending";
        $.ajax({
            url : '{{route("admin.pendingfilter")}}',
            method : 'GET',
            data : {
                date : date,
                value : value,
            },
            success:function(response){
                console.log(response.component);
                $('.appending').empty().append(response.component);
            }
        })
    }
</script>
@endsection
@include('admin.layout.footer')