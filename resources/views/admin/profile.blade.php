@section('title')
Edit Profile | Admin Panel
@endsection
@include('admin.layout.header')
<style>
    .add_more{
        color: #E82583;
        float: right;
        cursor: pointer;
    }
</style>
<div class="container">
    <h1 class="mb-4 text-primary font-weight-bold">Edit Your Profile</h1>
    <div class="card shadow mb-4">
        <div class="card-body">
            <form method="post" action='{{route("admin.updateProfile")}}' enctype="multipart/form-data">
                @csrf
                <div class="row questions">
                <div class="col-lg-12 row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" required name="name" value="{{Auth::user()->name}}" placeholder="name" type="text">
                        </div>
                    </div>
                    <div class="col-lg-12">
                       <div class="form-group">
                        <label class="text-capitalize fs-14">Last Name</label>
                        <input class="form-control" value="{{Auth::user()->l_name}}" name="l_name" placeholder="Last Name" type="text" required>
                    </div>
                </div>
                <div class="col-lg-12">
                   <div class="form-group">
                    <label class="text-capitalize fs-14">E-mail</label>
                    <input class="form-control" name="email" value="{{Auth::user()->email}}" placeholder="email" type="email" required>
                </div>
            </div>
              <div class="col-lg-12">
                         <div class="form-group">
                            <label>Profile Image</label>
                            <input type="file" class="dropify form-control" name="image">
                        </div>
                        </div>
        </div>
            <div class="col-lg-12 text-center">
                <button class="btn btn-sm btn-primary">Submit</button>
            </div>
        </div>
    </form>
</div>
</div>
</div>
<footer class="sticky-footer bg-white"></footer>
</div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
@include('admin.layout.footer')
