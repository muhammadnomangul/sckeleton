@section('title')
Company Survey | Admin Panel
@endsection
@include('admin.layout.header')
<!-- Main Content -->
<style>
    .dropdown-item{
        cursor: pointer;
    }
    .question-answer {
        margin-bottom: 20px;
        border: 1px solid #e0e0e0;
        border-radius: 5px;
        padding: 15px;
    }

/* Style for question text */
.question-answer p:first-child {
    margin-bottom: 10px;
    font-weight: bold;
}

/* Style for answer text */
.question-answer p:last-child {
    margin-top: 0;
}
</style>
<div class="container my-4">
    <h1 class="mb-4 text-primary font-weight-bold">Company Survey</h1>
    <div class="row">
        <div class="col-lg-4">
        </div>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered data-table" width="100%" cellspacing="0">
            <thead>
             <tr>
                <th>SN</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Company Name</th>
                <th>Phone</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody class="appending">
            @forelse($surveys->unique('user_id') as $survey)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $survey->userRef->name }}</td>
                <td>{{ $survey->userRef->l_name }}</td>
                <td>{{ $survey->userRef->email }}</td>
                <td>{{ $survey->userRef->c_name }}</td>
                <td>{{ $survey->userRef->phone }}</td>
                <td>
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button"
                        id="actionDropdown{{ $loop->iteration }}" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        Action
                    </button>
                    <div class="dropdown-menu" aria-labelledby="actionDropdown{{ $loop->iteration }}">
                        <a class="dropdown-item view_survey" data-toggle="modal" data-target="#addFieldModal" data-id="{{ $survey->user_id }}">View Survey</a>
                    </div>
                </div>
            </td>
        </tr>
        @empty
        <tr>
            <td colspan="7">No surveys found.</td>
        </tr>
        @endforelse
    </tbody>

</table>
</div>
</div>
</div>
</div>
<a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
</a>
<div class="modal fade" id="addFieldModal" tabindex="-1" role="dialog" aria-labelledby="addFieldModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="addFieldModalLabel">Survey Questions</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="questionAnswerBody">
        </div>
    </div>
</div>
</div>

@section('js')
<script>

</script>

@endsection
@include('admin.layout.footer')
