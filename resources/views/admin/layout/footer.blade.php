<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="addFieldModal" tabindex="-1" role="dialog" aria-labelledby="addFieldModalLabel"
aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="addFieldModalLabel">Survey Questions</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="questionAnswerBody">
            <div class="question-answer">
                <p><strong>Question:</strong> Lorem ipsum dolor sit amet?</p>
                <p><strong>Answer:</strong> Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
        </div>
    </div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function() {
        $('.nav-link.dropdown-toggle').click(function() {
            $(this).next('.dropdown-menu').toggleClass('show');
        });
    });
</script>
<script>
  $('.view_survey').on('click', function() {
    $('#questionAnswerBody').empty();
    var id = $(this).attr('data-id');
    $.ajax({
        url: '{{ route("admin.survey") }}',
        method: 'GET',
        data: {
            id: id,
        },
        success: function(response) {
            $.each(response.survey, function(index, item) {
                var questionAnswerHtml = '<div class="question-answer">';
                questionAnswerHtml += '<p><strong>Question:</strong> ' + item.question + '</p>';
                questionAnswerHtml += '<p><strong>Answer:</strong> ' + item.answer + '</p>';
                questionAnswerHtml += '</div>';
                $('#questionAnswerBody').append(questionAnswerHtml);
            });
        }
    });
});
  $(document).ready(function() {
    $('#downloadButton').click(function() {
        // Create an array to store the data
        var data = [];

        // Add the table headings to the data array
        var headings = [];
        $('#Table thead th').each(function() {
            headings.push($(this).text());
        });
        data.push(headings.join(','));

        // Iterate through the table rows and extract the data
        $('#Table tbody tr').each(function() {
            var row = [];
            $(this).find('td').each(function() {
                row.push($(this).text());
            });
            data.push(row.join(','));
        });

        // Create a CSV string from the data
        var csv = data.join('\n');

        // Create a Blob object with the CSV data
        var blob = new Blob([csv], { type: 'text/csv' });

        // Create a download link
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.download = 'table_data.csv';

        // Trigger a click event on the link to start the download
        link.click();
    });
});


        // select
  document.getElementById('selectAll').addEventListener('change', function () {
    let checkboxes = document.getElementsByTagName('input');
    for (let i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].type == 'checkbox') {
            checkboxes[i].checked = this.checked;
        }
    }
});
  function exportData() {
            // Get the table element
    var table = document.getElementById("Table");

            // Create a CSV string from the table data
    var csv = [];
    for (var i = 0; i < table.rows.length; i++) {
        var row = table.rows[i];
        var rowData = [];
        for (var j = 0; j < row.cells.length; j++) {
            rowData.push(row.cells[j].innerText);
        }
        csv.push(rowData.join(","));
    }
    csv = csv.join("\n");

            // Create a temporary download link and trigger a download
    var link = document.createElement("a");
    link.setAttribute(
        "href",
        "data:text/csv;charset=utf-8," + encodeURIComponent(csv)
        );
    link.setAttribute("download", "data.csv");
    link.style.display = "none";
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
</script>
<!-- Bootstrap core JavaScript-->
<script src="{{asset('admin_css/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('admin_css/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

<!-- Core plugin JavaScript-->
<script src="{{asset('admin_css/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

<!-- Custom scripts for all pages-->
<script src="{{asset('admin_css/js/sb-admin-2.min.js')}}"></script>

<!-- Page level plugins -->
<script src="{{asset('admin_css/vendor/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin_css/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

<!-- Page level custom scripts -->
<script src="{{asset('admin_css/js/demo/datatables-demo.js')}}"></script>


<script src="{{asset('admin_css/vendor/jquery/jquery.min.js')}}"></script>
<script src="{{asset('admin_css/vendor/chart.js/Chart.min.js')}}"></script>
<script src="{{asset('admin_css/js/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('admin_css/js/demo/claimschart.js')}}"></script>
<script src="{{asset('admin_css/js/demo/mainchart.js')}}"></script>
@yield('js')
<script>
	$('.search_bar_field').on('input',function(){
		var val = $(this).val();
		$.ajax({
			method : 'GET',
			url : '{{route("admin.suggestion")}}',
			data : {
				val : val,
			},
			success:function(response){
				$('.search_list').empty().append(response.component);
				$('.search_bar').removeClass('d-none');
			}
		})
	})
	document.addEventListener("DOMContentLoaded", function() {
		var searchBar = document.querySelector('.search_bar');

		document.addEventListener('click', function(event) {
			var isClickInsideSearchBar = searchBar.contains(event.target);
			if (!isClickInsideSearchBar) {
				searchBar.classList.add('d-none');
			}
		});
	});
</script>
<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>
<script>
   @if (Session::has('success'))
   toastr.success("{{ Session::get('success') }}");
   @endif


   @if (Session::has('info'))
   toastr.info("{{ Session::get('info') }}");
   @endif


   @if (Session::has('warning'))
   toastr.warning("{{ Session::get('warning') }}");
   @endif
   @if (Session::has('message'))
   toastr.success("{{ Session::get('success') }}");
   @endif

   @if (Session::has('error'))
   toastr.error("{{ Session::get('error') }}");
   @endif
</script>

@if (session('toast_error'))
<script>
   var errors = {!! json_encode(session('toast_error')) !!};
   for (var i = 0; i < errors.length; i++) {
       toastr.error(errors[i], 'Validation Error');
   }
</script>

@endif
</body>

</html>
