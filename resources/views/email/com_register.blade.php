
<!DOCTYPE html>
<html>
<head>
  <style>
    .container {
      background-color: white;
      padding: 20px;
      border-radius: 25px;
      margin: 20px; /* Add margin to the container */
    }
    .header {
      text-align: center;
      color: white;
      background-color: #E82583;
      padding: 20px;
      border-top-left-radius: 25px;
      border-top-right-radius: 25px;
    }
    .content {
      padding: 30px;
    }
    p {
      font-size: 18px;
    }
  </style>
</head>
<body style="background-color: #E82583; padding: 30px;">
  <table border="0" cellspacing="0" cellpadding="0" align="center" width="100%">
    <tr>
      <td class="header">
        <h1 style="font-size: 35px !important;">Congratulations! Your Company Account is Live on CleonHR</h1>
      </td>
    </tr>
    <tr>
      <td class="container">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td class="content">
              <img src="{{$message->embed('candidate_styling/image/candi-logo.png')}}" alt="Logo" style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;width:100px;margin-bottom:10px" title="Logo">

              <p>Dear {{ $mail['name'] }},</p>
              <p style="padding-top: 10px;">We are excited to inform you that your company account has been successfully created on CleonHR. Your account is now live and ready to use.</p>
              <p style="padding-top: 10px;">You can now log in to your account using your registered email address and the password you created during the account creation process.</p>
              <a href="{{route('company.login')}}" style="background-color: #e82583;padding: 13px 30px;font-size: 18px;color: white;text-decoration: none;border-radius: 12px;margin-left: 39%;">Click to Login</a>
              <p style="padding-top: 10px;">Thank you for choosing CleonHR.</p>
              <p style="margin: 0px;">Best Regards,</p>
              <p style="margin: 0px;">CleonHR Team</p>
              <h6 style="font-size:18px !important;margin: 0px;">You are recieving this mail because you registered with CleonHr.</h6>
              <p style="color: #E82583;margin: 0px;">Copyright {{ \Carbon\Carbon::now()->year }}</p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>




<!DOCTYPE html>
<html>
<head>
  <style>
    @media only screen and (max-width: 600px) {
      .container {
        margin: 10px !important;
        padding: 10px !important;
      }
    }
  </style>
</head>
<body style="margin: 0; padding: 0; background-color: #E82583;">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td class="header" style="text-align: center; color: white; background-color: #E82583; padding: 20px; border-top-left-radius: 25px; border-top-right-radius: 25px;">
        <h1 style="font-size: 35px !important; margin: 0;">Congratulations! Your Company Account is Live on CleonHR</h1>
      </td>
    </tr>
    <tr>
      <td class="container" style="padding: 20px; border-radius: 25px; margin: 20px;">
        <table border="0" cellpadding="0" style="background-color: white; border-radius: 25px" cellspacing="0" width="100%">
          <tr>
            <td class="content" style="padding: 20px;">
              <img src="{{$message->embed('cleon_styling/image/candi-logo.png')}}" alt="Logo" style="display: block; border: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: 100px; margin-bottom: 20px;">
              <p style="font-size: 18px; margin-top: 0;">Dear {{ $mail['name'] }},</p>
              <p style="font-size: 18px;">We are excited to inform you that your company account has been successfully created on CleonHR. Your account is now live and ready to use.</p>
              <p style="font-size: 18px;">You can now log in to your account using your registered email address and the password you created during the account creation process.</p>
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td class="content" align="center" style="padding-top: 20px;">
                    <a href="{{ route('user.login') }}" style="background-color: #e82583; padding: 13px 30px; font-size: 18px; color: white; text-decoration: none; border-radius: 12px; display: inline-block;">Click to Login</a>
                  </td>
                </tr>
              </table>
              <p style="font-size: 18px;">Thank you for choosing CleonHR.</p>
              <p style="font-size: 18px; margin: 0;">Best Regards,<br>CleonHR Team</p>
              <p style="font-size: 14px; color: #E82583; margin: 0;">&copy; {{ \Carbon\Carbon::now()->year }} CleonHR. All rights reserved.</p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
