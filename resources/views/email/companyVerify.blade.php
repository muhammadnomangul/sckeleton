<!DOCTYPE html>
<html>
<head>
  <style>
    @media only screen and (max-width: 600px) {
      .container {
        margin: 10px !important;
        padding: 10px !important;
      }
    }
  </style>
</head>
<body style="margin: 0; padding: 0; background-color: #E82583;">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td class="header" style="text-align: center; color: white; background-color: #E82583; padding: 20px; border-top-left-radius: 25px; border-top-right-radius: 25px;">
        <h1 style="font-size: 35px !important; margin: 0;">OTP Verification for Your Company Account Creation on CleonHR</h1>
      </td>
    </tr>
    <tr>
      <td class="container" style="padding: 20px; border-radius: 25px; margin: 20px;">
        <table border="0" cellpadding="0" style="background-color: white; border-radius: 25px" cellspacing="0" width="100%">
          <tr>
            <td class="content" style="padding: 20px;">
              <img src="{{$message->embed('cleon_styling/image/candi-logo.png')}}" alt="Logo" style="display: block; border: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: 100px; margin-bottom: 20px;">
              <p style="font-size: 18px; margin-top: 0;">Dear {{ $mail['name'] }},</p>
              <p style="font-size: 18px;">Thank you for choosing CleonHR to create your company account. To verify your account and complete the registration process, please enter the One-Time Password (OTP) below:</p>
              <p style="font-size: 18px;">OTP : {{ $mail['code'] }}</p>
              <p style="font-size: 18px;">Please note that the OTP is valid for a limited time only.</p>
              <p style="font-size: 18px;">After entering the OTP, you will be able to complete the registration process and start using our platform to manage your company.</p>
              <p style="font-size: 18px;">For your security, we recommend that you do not share your OTP with anyone.</p>
              <p style="font-size: 18px;">If you have not initiated the account creation process, please ignore this email</p>
              <p style="font-size: 18px;">Thank you for choosing CleonHR. We look forward to working with you.</p>
              <p style="font-size: 18px; margin: 0;">Best Regards,<br>CleonHR Team</p>
              <p style="font-size: 14px; color: #E82583; margin: 0;">&copy; {{ \Carbon\Carbon::now()->year }} CleonHR. All rights reserved.</p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
