<!DOCTYPE html>
<html>
<head>
    <title>Email Verification</title>
</head>
<body style="font-family: Arial, sans-serif; background-color: #f7f7f7; padding: 20px; text-align: center;">
    <table style="width: 100%; max-width: 600px; background-color: #fff; margin: 0 auto; border-collapse: collapse;">
        <tr>
            <td style="padding: 20px;">
                <h1 style="color: #007BFF;">Email Verification</h1>
                <p>Hello {{ $mail['name'] }},</p>
                <p>Thank you for signing up with our service. Your verification code is:</p>
                <p style="font-size: 24px; font-weight: bold; color: #007BFF;">{{ $mail['code'] }}</p>
                <p>Please enter this code in the verification field to complete the process.</p>
                <p>If you did not create an account with us, please ignore this email.</p>
                <p>Thank you for using our service!</p>
            </td>
        </tr>
    </table>
</body>
</html>
