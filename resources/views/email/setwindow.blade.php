<!DOCTYPE html>
<html>
<head>
  <style>
    @media only screen and (max-width: 600px) {
      .container {
        margin: 10px !important;
        padding: 10px !important;
      }
    }
  </style>
</head>
<body style="margin: 0; padding: 0; background-color: #E82583;">
  <table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
      <td class="header" style="text-align: center; color: white; background-color: #E82583; padding: 20px; border-top-left-radius: 25px; border-top-right-radius: 25px;">
        <h1 style="font-size: 35px !important; margin: 0;">Your claim window date has been successfully setup.</h1>
      </td>
    </tr>
    <tr>
      <td class="container" style="padding: 20px; border-radius: 25px; margin: 20px;">
        <table border="0" cellpadding="0" style="background-color: white; border-radius: 25px" cellspacing="0" width="100%">
          <tr>
            <td class="content" style="padding: 20px;">
              <img src="{{$message->embed('cleon_styling/image/candi-logo.png')}}" alt="Logo" style="display: block; border: 0; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic; width: 100px; margin-bottom: 20px;">
              <p style="font-size: 18px; margin-top: 0;">Dear {{ $mail['name'] }},</p>
              <p style="font-size: 18px;">This is to inform you that your claims window date has been successfully setup.
                It is now live and will be open to your employees on the set date.
              </p>
              <p style="font-size: 18px;">To view this date, simply log in to your company account for Cleontime and click on the "Configure Window" tab. From there, you can see the set date that the claims window will be open.</p>
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td class="content" align="center" style="padding-top: 20px;">
                    <a href="{{ route('company.login') }}" style="background-color: #e82583; padding: 13px 30px; font-size: 18px; color: white; text-decoration: none; border-radius: 12px; display: inline-block;">Click to Login</a>
                  </td>
                </tr>
              </table>
              <p style="font-size: 18px; padding-top: 20px;">Thank you for choosing CleonHR.</p>
              <p style="font-size: 18px; margin: 0;">Best Regards,<br>CleonHR Team</p>
              <p style="font-size: 14px; color: #E82583; margin: 0;">&copy; {{ \Carbon\Carbon::now()->year }} CleonHR. All rights reserved.</p>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>
