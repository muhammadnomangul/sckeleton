<!DOCTYPE html>
<html>
<head>
    <title>Claim Approved</title>
</head>
<body style="font-family: Arial, sans-serif; background-color: #f7f7f7; padding: 20px; text-align: center;">
    <table style="width: 100%; max-width: 600px; background-color: #fff; margin: 0 auto; border-collapse: collapse;">
        <tr>
            <td style="padding: 20px;">
                <h1 style="color: #007BFF;">Hello {{ $content['name'] }}</h1>
                <p>Claim approval:Employee {{ $content['employeName'] }} claim has been approved With Value {{$content['value']}} by {{$content['approved']}}.Your Approval is needed.Click on the buttons to accept or declined the claim.</p>
                <a href="{{ $content['accept'] }}"><button class="btn btn-primary">Approve Claim</button></a>
                <a href="{{ $content['decline'] }}"><button class="btn btn-primary">Decline Claim</button></a>            </td>
            </tr>
        </table>
    </body>
    </html>
