<!DOCTYPE html>
<html>
<head>
    <title>Claim Approved</title>
</head>
<body style="font-family: Arial, sans-serif; background-color: #f7f7f7; padding: 20px; text-align: center;">
    <table style="width: 100%; max-width: 600px; background-color: #fff; margin: 0 auto; border-collapse: collapse;">
        <tr>
            <td style="padding: 20px;">
                <h1 style="color: #007BFF;">Hello {{ $name }}</h1>
                <p>Claim approval:Your {{ $claimName }} claim has been approved With Value {{$value}}.</p>
                <a href="{{url('company/login')}}"><button class="btn btn-primary">Login on CleonTime</button></a>
            </td>
        </tr>
    </table>
</body>
</html>
