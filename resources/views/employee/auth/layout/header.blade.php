<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="{{asset('employe_styling/auth/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('employe_styling/auth/css/style.css')}}">
	<script src="https://kit.fontawesome.com/00b231478f.js" crossorigin="anonymous"></script>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
</head>
<body>

	<section class="candidate-singup">
		<header>
			<nav class="navbar navbar-expand-lg navbar-light bg-transparent p-0 m-0">
				<div class="container-fluid">
					<div class="d-flex align-items-center">
						<a class="navbar-brand sizebrand" href="{{url('/')}}">
							<img src="{{asset('employe_styling/auth/image/candi-logo.png')}}" class="img-fluid">
						</a>
					</div>
					<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse space-link" id="navbarTogglerDemo02">
						
					</div>
				</div>
			</nav>
		</header>