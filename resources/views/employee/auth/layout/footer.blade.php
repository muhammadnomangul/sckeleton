
</section>


<!-- js area -->
<script src="{{asset('employe_styling/auth/js/jquery-3.6.0.js')}}"></script>
<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

<script type="text/javascript">
	function showpass(){
		const passinput = document.getElementById('passinput');

		if (passinput.type === 'password') {
			passinput.type = 'text'
		} else {
			passinput.type = 'password'
		}
	}
</script>

<script>
    var swiper = new Swiper(".mySwiper", {
      slidesPerView: 1,
      autoplay: true,
      spaceBetween: 10,
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      breakpoints: {
        640: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 1,
          spaceBetween: 10,
        },
      },
    });
  </script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> 


<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>



<script>
    @if (Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    @endif


    @if (Session::has('info'))
    toastr.info("{{ Session::get('info') }}");
    @endif


    @if (Session::has('warning'))
    toastr.warning("{{ Session::get('warning') }}");
    @endif
    @if (Session::has('message'))
    toastr.success("{{ Session::get('success') }}");
    @endif

    @if (Session::has('error'))
    toastr.error("{{ Session::get('error') }}");
    @endif
</script>

@if (session('toast_error'))
<script>
    var errors = {!! json_encode(session('toast_error')) !!};
    for (var i = 0; i < errors.length; i++) {
        toastr.error(errors[i], 'Validation Error');
    }
</script>
@endif

<script>

    $('#reset_password').submit(function(e) {
        e.preventDefault(); 
        var email = $('#email').val();
        $.ajax({
            type: 'GET',
            url: '{{route("check.user")}}',
            data: {
                email: email,
            },
            success: function(response) {
                if ( response === 'company_employee') {
                $('#reset_password').off('submit').submit();
                } else {
                    toastr.error('Email not found or user is not a Employee');
                }
            },
            error: function() {
                alert('An error occurred during the check');
            }
        });
    });

</script>
@yield('js')
</body>
</html>