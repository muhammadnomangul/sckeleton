<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Confirm E-mail</title>
    <link rel="stylesheet" type="text/css" href="{{asset('employe_styling/auth/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('employe_styling/auth/css/style.css')}}">
    <script src="https://kit.fontawesome.com/00b231478f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
</head>
<body>

    <section class="candidate-singup">
        <header>
            <nav class="navbar navbar-expand-lg navbar-light bg-transparent p-0 m-0">
              <div class="container-fluid">
                <div class="d-flex align-items-center">
                    <a class="navbar-brand sizebrand" href="#">
                        <img src="{{asset('employe_styling/auth/image/candi-logo.png')}}" class="img-fluid">
                    </a>
                    <a href="#" class="text-decoration-none rounded-5 shadow light-pink p-2 px-4 d-flex align-items-center ms-3">
                        <div>
                            <img src="{{asset('employe_styling/auth/image/candi-icon.png')}}" class="img-fluid" style="width: 30px;">
                        </div>
                        <h6 class="text-white mb-0 ms-3 fs-14">Free Advisory</h6>
                    </a>
                </div>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse space-link justify-content-end" id="navbarTogglerDemo02">
                <a href="login-page-candi.html" class="text-decoration-none p-2 px-4 bg-pink rounded-5 text-capitalize text-white fs-14 " onclick="event.preventDefault(); document.getElementById('logout-form').submit();">  Logout

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>                    </a>
                </div>
            </div>
        </nav>
    </header>
    <div class="row justify-content-lg-end justify-content-center">
        <div class="col-lg-6 col-xl-8">
            <div class="my-5 text-center">
                <img src="{{asset('employe_styling/auth/image/candi-img.png')}}" class="img-fluid img-fooa">
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-6 col-lg-5 col-xl-4">
            <div class="p-3 px-xl-5 p-2 my-5 pb-5 bg-white rounded-4 ms-lg-auto me-xl-5 me-lg-2">
                <div class="mt-5">
                    <h4 class="fw-bold text-uppercase">CONFIRM OTP</h4>
                    <form class="row g-3 mt-3" method="post" action="{{route('verify_email')}}" id="verify_email">
                        @csrf
                        <div class="col-lg-12">
                            <div class="mx-3">
                              <div class="otp-input-fields">
                                <input type="number" class="otp__digit otp__field__1" maxlength="1" size="1" oninput="restrictToSingleDigit(this)" required>
                                <input type="number" class="otp__digit otp__field__2" maxlength="1" size="1" oninput="restrictToSingleDigit(this)" required>
                                <input type="number" class="otp__digit otp__field__3" maxlength="1" size="1" oninput="restrictToSingleDigit(this)" required>
                                <input type="number" class="otp__digit otp__field__4" maxlength="1" size="1" oninput="restrictToSingleDigit(this)" required>
                                <input type="number" class="otp__digit otp__field__5" maxlength="1" size="1" oninput="restrictToSingleDigit(this)" required>
                            </div>  
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-check my-3">
                          <input class="form-check-input" type="checkbox" value="" id="flexCheckDefault" required >
                          <label class="form-check-label fs-14" for="flexCheckDefault">
                            By clicking get started, you agree to 
                            <a href="#" class="text-decoration-none">
                                our terms and conditions
                            </a>
                        </label>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="d-flex align-items-center justify-content-between my-3">
                        <div>
                            <button class="border-0 bg-pink rounded-3 btncust text-white d-flex align-items-center justify-content-between fs-14 w-100 flex-nowrap">
                                <p class="mb-0 ps-2 fs-14s text-nowrap">Get Started</p>
                                <div class="text-end ms-3">
                                    <img src="{{asset('employe_styling/auth/image/submit-arrow.png')}}" class="img-fluid width-20">
                                </div>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@section('js')
<script>
    function restrictToSingleDigit(input) {
        if (input.value.length > 1) {
        input.value = input.value.slice(0, 1); // Only keep the first character
    }
}
document.addEventListener("DOMContentLoaded", function () {
    const otpFields = document.querySelectorAll(".otp__digit");

    otpFields.forEach(function (field, index) {
        field.addEventListener("input", function () {
            // Remove any non-numeric characters
            this.value = this.value.replace(/\D/g, "");

            if (this.value !== "") {
                if (index < otpFields.length - 1) {
                    otpFields[index + 1].focus();
                }
            }
        });

        field.addEventListener("keydown", function (e) {
            if (e.key === "Backspace" && this.value === "") {
                if (index > 0) {
                    otpFields[index - 1].focus();
                }
            }
        });
    });
});

$('#verify_email').submit(function(e){
    e.preventDefault();
    var field1 = $(".otp__field__1").val();
    var field2 = $(".otp__field__2").val();
    var field3 = $(".otp__field__3").val();
    var field4 = $(".otp__field__4").val();
    var field5 = $(".otp__field__5").val();
    var otpValue = field1 + field2 + field3 + field4 + field5;
    $('#verify_email').append('<input type="hidden" value="'+otpValue+'" name="otp">');
    $('#verify_email').off('submit').submit();

})

</script>
@endsection
@include('employee.auth.layout.footer')
