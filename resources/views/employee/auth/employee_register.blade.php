@section('title')
Register
@endsection
@include('employee.auth.layout.header')
<div class="row justify-content-lg-end justify-content-center">
	<div class="col-lg-6 col-xl-8">
		<div class="my-5 text-center">
			<img src="{{asset('assets/side/EmpLogin.jpg')}}" style="border-radius: 20px;" class="img-fluid img-fooa">
		</div>
	</div>
	<div class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-4">
		<div class="p-4 my-5 bg-white rounded-4 ms-lg-auto me-lg-5">
			<div class="mt-2">
				<h4 class="fw-bold text-uppercase">CREATE CLEON ACCOUNT</h4>
				<form class="row g-3" method="POST" action="{{ route('register') }}">
					@csrf
					<input type="hidden" name="role" value="employee">
					<div class="col-lg-12">
						<div class="mt-3">
							<input type="text" class="form-control pad-12 fs-14" placeholder="First Name" value="{{old('first_name')}}" name="first_name" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div>
							<input type="text" class="form-control pad-12 fs-14" placeholder="Last Name" value="{{old('last_name')}}" name="last_name" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div>
							<input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" class="form-control pad-12 fs-14" placeholder="Phone" name="phone_number" value="{{old('phone_number')}}" required>
						</div>
					</div>
					<div class="col-lg-12">
						<div>
							<input type="email" class="form-control pad-12 fs-14" placeholder="Email" value="{{old('email')}}" name="email" required>
							@if ($errors->has('email'))
							<span class="invalid-feedback d-block" style="margin-top: 10px !important;" role="alert">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="col-lg-12">
						<div>
							<input type="password" class="form-control pad-12 fs-14" placeholder="Password" name="password">
							@if ($errors->has('password'))
							<span class="invalid-feedback d-block" style="margin-top: 10px !important;" role="alert">
								<strong>{{ $errors->first('password') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="col-lg-12">
						<div>
							<input type="password" class="form-control pad-12 fs-14" placeholder="Confirm Password" name="password_confirmation">
						</div>
					</div>
					<div class="col-lg-12">
						<div class="mb-3">
							<p class="fs-14">
								By login, you agree to our <a href="#" class="text-decoration-none text-pink fw-bold">Terms and Conditions</a>
							</p>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="d-flex align-items-center justify-content-between">
							<div>
								<button class="border-0 bg-pink rounded-3 btncust text-white d-flex align-items-center justify-content-between fs-14 w-100" style="text-decoration: none;" href="login-page-candi.html">
									<p class="mb-0 ps-2">Create</p>
									<div class="text-end ms-3">
										<img src="{{asset('employe_styling/auth/image/submit-arrow.png')}}" class="img-fluid width-20">
									</div>
								</button>
							</div>
							<div>
								<p class="fs-14 mb-0">
									Already have an account ? 
									<a href="{{route('user.login')}}" class="text-decoration-none fs-14 text-pink">Login Here</a>
								</p>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
@include('employee.auth.layout.footer')