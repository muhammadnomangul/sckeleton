@section('title')
Login
@endsection
@include('employee.auth.layout.header')
<style>
	@media(max-width : 1160px ) and (min-width : 991px){
		.margin_bottom{
			margin-bottom: 15px !important;
		}
	}
	@media(max-width : 366px ){
		.margin_bottom{
			margin-bottom: 15px !important;
		}
	}
</style>
<div class="row justify-content-lg-end justify-content-center">
	<div class="col-lg-6 col-xl-8">
		<div class="my-5 text-center">
			<img src="{{asset('assets/side/EmpLogin.jpg')}}" style="border-radius: 20px;" class="img-fluid img-fooa">
		</div>
	</div>
	<div class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-4">
		<div class="p-4 my-5 bg-white rounded-4 ms-lg-auto me-lg-5">
			<div class="mt-4 ">
				<h4 class="fw-bold text-uppercase">login</h4>
				<form class="row g-3" method="POST" action="{{route('user.login')}}">
					@csrf
					<input type="hidden" name="employee" id="employee"
					value="employee">
					<div class="col-lg-12">
						<div class="mt-3">
							<div class="input-group border rounded-2 p-2 margin_bottom" style="position:relative;">
								<input type="email" id="email" class="form-control border-0 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required
								placeholder="user.doe@officialemail.com"
								aria-label="user.doe@officialemail.com" aria-describedby="basic-addon2">
								<span class="input-group-text bg-transparent border-0" id="basic-addon2">
									<img src="{{asset('cleon_styling/image/at.png')}}" class="img-fluid">
								</span>
								@error('email')
								<span class="invalid-feedback" style="position: absolute;top: 50px;" role="alert">
									<strong>{{ $message }}</strong>
								</span>
								@enderror
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="mt-3">
							<div class="input-group border rounded-2 p-2">
								<input type="password" name="password" class="form-control fs-14 border-0" placeholder="Password" aria-label="Password" aria-describedby="basic-addon3" id="passinput" required>
								<span class="input-group-text bg-transparent border-0" id="basic-addon3" onclick="showpass();">
									<img src="{{asset('employe_styling/auth/image/pass-eye.png')}}" class="img-fluid">
								</span>
							</div>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="mb-3">
							<p class="fs-14">
								By login, you agree to our <a href="#" class="text-decoration-none text-pink fw-bold">Terms and Conditions</a>
							</p>
						</div>
					</div>
					<div class="col-lg-12">
						<div class="d-flex align-items-center justify-content-between">
							<div>
								<button class="border-0 bg-pink rounded-3 btncust text-white d-flex align-items-center justify-content-between fs-14 w-100" style="text-decoration: none;" >
									<p class="mb-0 ps-2">Login</p>
									<div class="text-end ms-3">
										<img src="{{asset('employe_styling/auth/image/submit-arrow.png')}}" class="img-fluid width-20">
									</div>
								</button>
							</div>
							<div>
								<a href="{{route('user.forget')}}" class="text-decoration-none fs-14 text-dark">Forgot Password ?</a>
							</div>
						</div>
					</div>
					<!-- <div class="col-lg-12">
						<div class="mt-4">
							<p class="fs-14">
								Don’t have an account ?
								<a href="{{route('user.register')}}" class="text-decoration-none text-pink fw-bold">
									Create an account
								</a>
							</p>
						</div>
					</div> -->
					<!-- <div class="col-lg-12">
						<div class="d-flex align-items-center justify-content-between">
							<a href="{{ url('auth/google') }}" class="text-decoration-none d-block">
								<img src="{{asset('employe_styling/auth/image/login-google.png')}}" class="img-fluid">
							</a>
							<a href="{{ url('auth/facebook') }}" class="text-decoration-none d-block ms-3">
								<img src="{{asset('employe_styling/auth/image/login-fb.png')}}" class="img-fluid">
							</a>
						</div>
					</div> -->
				</form>
			</div>
		</div>
	</div>
</div>
@include('employee.auth.layout.footer')