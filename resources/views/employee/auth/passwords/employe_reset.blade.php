@section('title')
Reset Password
@endsection
@include('employee.auth.layout.header')
<style>
    .invalid-feedback{
        display: block !important;
    }
</style>
<div class="row justify-content-lg-end justify-content-center">
    <div class="col-lg-6 col-xl-8">
        <div class="my-5 text-center">
            <img src="{{asset('assets/side/EmpLogin.jpg')}}" style="border-radius: 20px;" class="img-fluid img-fooa">
        </div>
    </div>
    <div class="col-10 col-sm-10 col-md-8 col-lg-6 col-xl-4">
        <div class="p-4 my-5 pb-5 bg-white rounded-4 ms-lg-auto me-lg-5">
            <div class="mt-5">
                <h4 class="fw-bold text-uppercase">RESET PASSWORD</h4>
                <form class="row g-3 mt-3" method="POST" action="{{route('employee.pass_update')}}">
                    @csrf
                    <div class="col-lg-12">
                     <div style="margin-bottom: 15px;position: relative;">
                        <input type="text" class="form-control pad-12 fs-14" value="{{$email}}" required name="email" readonly>
                    </div>
                </div>
                <div class="col-lg-12">
                 <div style="margin-bottom: 15px;position: relative;">
                     <input type="text"  oninput="this.value = this.value.replace(/[^0-9]/g, '')" class="form-control pad-12 fs-14" placeholder="OTP" required name="otp">
                     @error('otp')
                     <span class="invalid-feedback" style="font-size:0.7rem;position: absolute;top: 50px;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div style="margin-bottom: 15px;position: relative;">
                    <input type="password" class="form-control pad-12 fs-14" placeholder="New Password" required name="password">
                    @error('password')
                    <span class="invalid-feedback" style="font-size:0.7rem;position: absolute;top: 50px;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-lg-12">
                <div>
                    <input type="password" class="form-control pad-12 fs-14" placeholder="Confirm New Password" required name="password_confirmation">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="d-flex align-items-center justify-content-between">
                    <div>
                        <button class="border-0 bg-pink rounded-3 btncust text-white d-flex align-items-center justify-content-between fs-14 w-100" style="text-decoration: none;" >
                            <p class="mb-0 ps-2">Reset</p>
                            <div class="text-end ms-3">
                                <img src="{{asset('employe_styling/auth/image/submit-arrow.png')}}" class="img-fluid width-20">
                            </div>
                        </button>
                    </div>
                    <div>
                        <p class="fs-12 mb-0">
                            Already have an account ?
                            <a href="{{route('user.login')}}" class="text-decoration-none fs-14 text-pink">Login Here</a>
                        </p>
                    </div>
                </div>
            </div>
        </form>
       <!--  <div class="col-lg-12">
            <div class="d-flex align-items-center justify-content-between pb-4 pt-3">
                <a href="#" class="text-decoration-none d-block">
                    <img src="{{asset('employe_styling/auth/image/login-google.png')}}" class="img-fluid">
                </a>
                <a href="#" class="text-decoration-none d-block ms-3">
                    <img src="{{asset('employe_styling/auth/image/login-fb.png')}}" class="img-fluid">
                </a>
            </div>
        </div> -->
    </div>
</div>
</div>
</div>
@include('employee.auth.layout.footer')