@section('pageTitle')
Your Claim History
@endsection
@include('employee.layout.header')
<style>
	.bg-lightest-pink{
		background-color: #E825830D !important;
	}
</style>
<!-- left side section start -->
<div class="mt-5 mb-3">
	<div class="row g-3 mt-5">
		<div class="col-lg-12 pt-5">
			<div class="appending_claim">
				<table class="" id="claim_table">
					<thead class="bg-table-head">
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								S/N
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Claim Type
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Claim Value
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Duration
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Amount Due
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Timestamp
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								status
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								actions
							</span>
						</th>
					</thead>
					<tbody>
						@foreach($claims as $claim)
						<tr class="{{$claim->status === 'declined' ? '' : 'bg-lightest-pink'}}">
							<td>
								<span class="text-capitalize fs-12">
									{{$loop->iteration}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$claim->employee_claim->title}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$claim->value}} {{$claim->per_hour}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$claim->t_hour}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									N {{$claim->t_hour * ($claim->value)}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{ $claim->created_at->format('Y-m-d') }}
								</span>
							</td>
							<td style="width: 180px;">
								<span class="text-capitalize">
									@if($claim->status == 'cancel')
									<a href="#"
									class="rounded text-decoration-none btncust1 bg-secondary px-4 fs-12 text-white text-capitalize p-2 rounded-5">
									Cancelled
									@else
									<a href="#"
									class="rounded text-decoration-none btncust1 {{$claim->status == 'pending' ? 'btn-skyish-color' :($claim->status == 'approved' ? 'bg-success' : 'bg-danger')}} px-4 fs-12 text-white text-capitalize p-2 rounded-5">
									{{$claim->status}}
									@endif
								</a>
							</span>
						</td>
						<td style="width: 120px;">
							<div class="d-flex align-items-center ">
								<button
								class="rounded border-0 btncust less-light-pink text-capitalize d-flex align-items-center justify-content-center ms-2 dropdown-toggle"
								type="button" data-bs-toggle="dropdown" aria-expanded="false">
								<img src="{{asset('employe_styling/image/icons/drops.png')}}" class="img-fluid">
							</button>
							@if($claim->status == "pending" || $claim->status == 'declined')
							<ul class="dropdown-menu bg-pink">
								@if($claim->status == 'declined')
								<li>
									<a class="dropdown-item fs-14 d-flex align-items-center reason_modal text-white text-capitalize" data-id="{{$claim->reason}}">
										<img src="{{asset('assets/side/decline.png')}}" class="img-fluid">
										<span class="ps-2">Decline Reason</span>
									</a>
								</li>
								@endif
								@if($claim->status == "pending")
								<li>
									<a class="dropdown-item fs-14 d-flex align-items-center text-white text-capitalize delete_claim" data-id="{{$claim->id}}">
										<span class="ps-2">Cancel Claim</span>
									</a>
									<form method="POST" action="{{route('employee.cancel_claim',['id'=>$claim->id])}}" class="delete_claim_form">@csrf</form>
								</li>
								@endif
							</ul>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@if ($claims->lastPage() > 1)
		<div class="d-flex justify-content-end mt-1" style="padding-right:5%">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					@if ($claims->onFirstPage())
					<li class="page-item disabled">
						<span class="page-link" aria-disabled="true">&laquo;</span>
					</li>
					@else
					<li class="page-item">
						<a class="page-link" href="{{ $claims->previousPageUrl() }}" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					@endif
					@foreach ($claims->getUrlRange(1, $claims->lastPage()) as $page => $url)
					<li class="page-item {{ $page == $claims->currentPage() ? 'active' : '' }}">
						<a class="page-link" href="{{ $url }}">{{ $page }}</a>
					</li>
					@endforeach
					@if ($claims->hasMorePages())
					<li class="page-item">
						<a class="page-link" href="{{ $claims->nextPageUrl() }}" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
					@else
					<li class="page-item disabled">
						<span class="page-link" aria-disabled="true">&raquo;</span>
					</li>
					@endif
				</ul>
			</nav>
		</div>
		@endif
	</div>
</div>
</div>
</div>
<div class="modal fade " id="reason_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Decline Reson</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<p class="reason_message"></p>
			</div>
		</div>
	</div>
</div>

@section('js')
<script>
	$('.reason_modal').on('click',function(){
		var reason = $(this).attr('data-id');
		$('.reason_message').text(reason);
		$('#reason_modal').modal('show');
	})
	$(document).on('click', '.delete_claim', function(event) {
		var current = $(this);
		Swal.fire({
			title: "Cancel Claim",
			text: "Are you sure, You Want to cancel this Claim!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes",
		}).then((result) => {
			if (result.isConfirmed) {
				current.closest('li').find('.delete_claim_form').submit();
			}
		});
	});
</script>
@endsection
@include('employee.layout.footer')