@section('pageTitle')
Notifications
@endsection
@include('employee.layout.header')
<style>

</style>
<div class="row" style="margin-bottom: 80px;margin-top:40px">
	<div class="col-md-12">
		@forelse($notifications as $noti)
		<div class="container border border-secondary rounded-3 mr-3 ml-3 mt-3 mb-3 pt-3 pb-1">
			<div class="row">
				<div class="col-11" style="font-weight: 600;font-size: 15px;">
					<a style="text-decoration : none;color:black">{{$noti->content}}</a>
				</div>
				<div class="col "><a href="#" class="link-danger delete-box">Delete</a>
					<form class="delete-form" method="post" action="{{route('employee.del.notification',['id'=>$noti->id])}}">@csrf</form></div>
				</div>
				<div class="text-primary" style="font-size: 15px;">{{$noti->created_at->diffForHumans()}}</div>
			</div>
			@empty
			<h2>No Notification</h2>
			@endforelse
		</div>
		@if ($notifications->lastPage() > 1)
		<div class="d-flex justify-content-end mt-1" style="padding-right:5%">
			<nav aria-label="Page navigation example">
				<ul class="pagination">
					@if ($notifications->onFirstPage())
					<li class="page-item disabled">
						<span class="page-link" aria-disabled="true">&laquo;</span>
					</li>
					@else
					<li class="page-item">
						<a class="page-link" href="{{ $notifications->previousPageUrl() }}" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
						</a>
					</li>
					@endif
					@foreach ($notifications->getUrlRange(1, $notifications->lastPage()) as $page => $url)
					<li class="page-item {{ $page == $notifications->currentPage() ? 'active' : '' }}">
						<a class="page-link" href="{{ $url }}">{{ $page }}</a>
					</li>
					@endforeach
					@if ($notifications->hasMorePages())
					<li class="page-item">
						<a class="page-link" href="{{ $notifications->nextPageUrl() }}" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
						</a>
					</li>
					@else
					<li class="page-item disabled">
						<span class="page-link" aria-disabled="true">&raquo;</span>
					</li>
					@endif
				</ul>
			</nav>
		</div>
		@endif
	</div>
	@section('js')
	<script>
		$('.delete-box').click(function(event) {
			event.preventDefault();
			Swal.fire({
				title: "Are you sure you want to delete this record?",
				text: "If you delete this, it will be gone forever.",
				icon: "warning",
				showCancelButton: true,
				confirmButtonColor: "#3085d6",
				cancelButtonColor: "#d33",
				confirmButtonText: "Yes",
			}).then((result) => {
				if (result.isConfirmed) {
					$(this).parent().find('.delete-form').submit();
				}
			});
		})
	</script>
	@endsection
	@include('employee.layout.footer')
