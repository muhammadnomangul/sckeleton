@section('pageTitle')
Make A Claim
@endsection
@include('employee.layout.header')
<style>
	.claim_edit_button:hover{
		background-color:#E82583;
		color: white;
	}
	.not-active{
		pointer-events: none;
		opacity: 0.5;
	}
</style>

<!-- left side section start -->
<div class="mt-5 mb-3">

	<!-- cards start -->
	<div class="row g-2">
		<div class="col-12 col-sm-12 com-md-12">
			<h5>Choose your claim type</h5>
		</div>
		@forelse($claims as $claim)
		<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 col-xxl-4 {{$active ? '' : 'not-active'}}">
			<div class="card setup_cards">
				<div class="row no-gutters">
					<div class="col-4 col-sm-4 col-md-4 col-lg-4">
						<div class="setup_cards_left">
							<span class="bg-pink text-white description_modal" style="cursor: pointer;" data-id="{{$claim->desc}}">?</span>
						</div>
					</div>
					<div class="col-8 col-sm-8 col-md-8 col-lg-8">
						<div class="card-body">
							<h5 class="fw-semibold text-capitalize">{{$claim->title}}</h5>
							<p class="fs-14">{{$claim->value}} / {{$claim->schedule}}</p>
						</div>
					</div>
				</div>

				<div class="d-flex justify-content-end" style="position: absolute;top: 7px;right: 10px;">
					<button class="btn btn-link" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-ellipsis-h" style="color:#E82583" aria-hidden="true"></i>
					</button>
					<ul class="dropdown-menu" style="background-color:white" aria-labelledby="dropdownMenuButton">
						<li class="claim_edit_button modal_claim" data-id="{{$claim->id}}" style="padding: 4px 17px;cursor: pointer;" data-place="{{$claim->schedule}}">
							<span>Claim</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
		@empty
		<h3>No Claim Found</h3>
		@endforelse
	</div>
	@if ($claims->lastPage() > 1)
	<div class="d-flex justify-content-end mt-1" style="padding-right:5%">
		<nav aria-label="Page navigation example">
			<ul class="pagination">
				@if ($claims->onFirstPage())
				<li class="page-item disabled">
					<span class="page-link" aria-disabled="true">&laquo;</span>
				</li>
				@else
				<li class="page-item">
					<a class="page-link" href="{{ $claims->previousPageUrl() }}" aria-label="Previous">
						<span aria-hidden="true">&laquo;</span>
					</a>
				</li>
				@endif
				@foreach ($claims->getUrlRange(1, $claims->lastPage()) as $page => $url)
				<li class="page-item {{ $page == $claims->currentPage() ? 'active' : '' }}">
					<a class="page-link" href="{{ $url }}">{{ $page }}</a>
				</li>
				@endforeach
				@if ($claims->hasMorePages())
				<li class="page-item">
					<a class="page-link" href="{{ $claims->nextPageUrl() }}" aria-label="Next">
						<span aria-hidden="true">&raquo;</span>
					</a>
				</li>
				@else
				<li class="page-item disabled">
					<span class="page-link" aria-disabled="true">&raquo;</span>
				</li>
				@endif
			</ul>
		</nav>
	</div>
	@endif
	<!-- cards end -->
</div>
<!-- left side section end -->


<!-- create moda start -->
<!-- Modal -->
<div class="modal fade claiming_modal" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-sm">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Make Overtime</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form method="POST" action="{{url('employee/make/claim')}}" enctype="multipart/form-data" class="employee_claim_response">
				@csrf
				<div class="modal-body">
					<input type="hidden" class="claim_id" name="claim_id" value="">
					<div class="row g-3">
						<div class="col-12">
							<div class="input-group bg-input p-1">
								<input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" name="t_hour" class="form-control claiming_time fs-12 bg-transparent border-0"
								placeholder="Enter Total Hours">
							</div>
						</div>
					</div>
					<div class="row g-3 mt-1 append_support">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12" style="text-align:right">
							<div class="bg-input p-2 text-center">
								<label class="text-capitalize d-block text-center " for="uploadid">
									<div>
										<img src="{{asset('employe_styling/image/icons/cloud.png')}}" class="img-fluid">
									</div>
									<p class="mb-0 text-capitalize fs-12 Browse_text"> Upload Supporting Document</p>
								</label>
								<input type="file" class="d-none upload_supporting" required id="uploadid" name="document[]">
							</div>
							<span class="add_more_supporting_document" style="cursor:pointer;color: #E82583;font-size:12px">Add More</span>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-center">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12">
							<button type="button"
							class="btn btn_create text-white d-block px-5 submit_button fw-semibold submit_employe_claim">Claim</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- create modal end -->
@section('js')
<script>
	$('.claim_edit_button').on('click',function(){
		var placholder = $(this).attr('data-place');
		if(placholder == 'hourly'){
			placholder = "Hour";
		}else if(placholder == 'weekly'){
			placholder = "Week";
		}else if(placholder == 'daily'){
			placholder = "Day";
		}else if(placholder == 'monthly'){
			placholder = "Month";
		}
		$('.claiming_time').attr('placeholder',"Enter Total " + placholder);
	})
	$('.submit_button').on('click', function() {
		Swal.fire({
			title: "Submit Claim",
			text: "Are you sure, You Want to Claim this claim!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes",
		}).then((result) => {
			if (result.isConfirmed) {
				$('.employee_claim_response').submit();
				$('.submit_employe_claim').prop('disabled', true);
			}
		});
	});

</script>
@endsection
@include('employee.layout.footer')