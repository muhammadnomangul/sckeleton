@section('pageTitle')
Manage Claims Data
@endsection
@include('employee.layout.header')
<style>
	.bg-lightest-pink{
		background-color: #E825830D !important;
	}
</style>
@php
$finance = can_access('financial_controller')
@endphp
<div class=" pt-4 mt-5 mb-3">
	<div class="row g-3">
		<div class="col-lg-12">
			<div class="appending_claim table-responsive">
				<table class="table align-middle table-borderless" style="width:100%;" id="claim_table">
					<thead class="bg-table-head">
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Select
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								S/N
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Full Name
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Email
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Claim Type
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Claim Value
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Duration
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Amount Due
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Status
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Action by
							</span>
						</th>
						<th>
							<span class="text-capitalize fs-w-500 fs-13">
								Actions
							</span>
						</th>
					</thead>
					<tbody>
						@foreach($claims as $claim)
						<tr class="{{$claim->status === 'declined' ? '' : 'bg-lightest-pink'}} parent_div_body">
							<td>
								@if($claim->newStatus == 'Action' || ($claim->approval == 1 && Auth::user()->role == 'company_admin'))
								<span class="text-capitalize fs-12">
									<input type="checkbox" name="claim_id" value="{{$claim->id}}" class="id_claims"> 
								</span>
								@endif
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$loop->iteration}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$claim->user->name}} {{$claim->user->l_name}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$claim->user->email}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$claim->employee_claim->title}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$claim->value}} {{$claim->per_hour}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									{{$claim->t_hour}}
								</span>
							</td>
							<td>
								<span class="text-capitalize fs-12">
									N {{$claim->t_hour * ($claim->value)}}
								</span>
							</td>
							<td style="width: 180px;">

								<span class="text-capitalize">
									@if($claim->status == "declined")
									<a class="rounded text-decoration-none btncust1 bg-red px-4 fs-12 text-white text-capitalize p-2 rounded-5">
										@if($claim->newStatus == 'Action')
										<i class="fa fa-circle text-warning" style="margin-right: 3px;"></i>
										@endif 
										Dispute
										@elseif($claim->status == 'approved')
										<a 
										class="rounded text-decoration-none btncust1 bg-success px-4 fs-12 text-white text-capitalize p-2 rounded-5">
										@if($claim->newStatus == 'Action')
										<i class="fa fa-circle text-warning" style="margin-right: 3px;"></i>
										@endif
										Approved
										@elseif($claim->status == 'pending')
										@if((Auth::user()->main_id == $claim->branch_head && $claim->branch) || ( Auth::user()->main_id == $claim->depart_head && $claim->department) || (Auth::user()->main_id == $claim->unit_head && $claim->unit))
										<a 
										class="rounded text-decoration-none btncust1 bg-success px-4 fs-12 text-white text-capitalize p-2 rounded-5">
										@if($claim->newStatus == 'Action')
										<i class="fa fa-circle text-warning" style="margin-right: 3px;"></i>
										@endif
										Approved
										@else
										<a class="rounded text-decoration-none btncust1 btn-skyish-color px-4 fs-12 text-white text-capitalize p-2 rounded-5">
											@if($claim->newStatus == 'Action')
											<i class="fa fa-circle text-warning" style="margin-right: 3px;"></i>
											@endif
											Pending
											@endif
											@endif
										</a>
									</span>
								</td>
								<td>
									<span class="text-capitalize fs-12">
										{{$claim->processed}}
									</span>
								</td>
								<td style="width: 120px;">
									<div class="d-flex align-items-center">
										<button
										class="rounded border-0 btncust less-light-pink text-capitalize d-flex align-items-center justify-content-center ms-2 dropdown-toggle"
										type="button" data-bs-toggle="dropdown" aria-expanded="false" style="margin-right: 5px;">
										<img src="{{asset('styling/image/icons/drops.png')}}" class="img-fluid">
									</button>
									<input type="hidden" class="source{{$claim->id}}" value="{{$claim->source}}">
									<ul class="dropdown-menu bg-pink">
										@if($user == 'finance' && $claim->notification == null)
										<li>
											<a class="dropdown-item fs-14 d-flex align-items-center text-white text-capitalize" href="{{ route('company.notifications', ['id' => $claim->id]) }}">
												<i class="fa fa-bell"></i>
												<span class="ps-2">Notify Employee</span>
											</a>
										</li>
										@endif
										@if($claim->confirm)
										<li>
											<a class="dropdown-item fs-14 d-flex align-items-center text-white text-capitalize editing_claim" data-id="{{$claim->id}}">
												<img src="{{asset('assets/side/reset.png')}}" class="img-fluid">
												<span class="ps-2">Respond To Claim</span>
											</a>
										</li>
										@endif
										@if(!empty($claim->document))
										@foreach($claim->document as $index=>$document)
										<li>
											<a class="dropdown-item fs-14 d-flex align-items-center text-white text-capitalize" href="{{url('document/'.$document)}}" download="">
												<i class="fa fa-download"></i>
												<span class="ps-2">Supporting Document {{$index > 0 ? $index + 1 : ''}}</span>
											</a>
										</li>
										@endforeach
										@endif
										@if($claim->status == 'declined')
										<li>
											<a class="dropdown-item fs-14 d-flex align-items-center reason_modal text-white text-capitalize" data-id="{{$claim->reason}}">
												<img src="{{asset('assets/side/decline.png')}}" class="img-fluid">
												<span class="ps-2">Decline Reason</span>
											</a>
										</li>
										@endif
									</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
				<div class="d-flex justify-content-end mt-1" style="padding-right:5%">
					<nav aria-label="Page navigation example">
						<ul class="pagination">
							@if ($claims->onFirstPage())
							<li class="page-item disabled">
								<span class="page-link" aria-disabled="true">&laquo;</span>
							</li>
							@else
							<li class="page-item">
								<a class="page-link" href="{{ $claims->previousPageUrl() }}" aria-label="Previous">
									<span aria-hidden="true">&laquo;</span>
								</a>
							</li>
							@endif
							@foreach ($claims->getUrlRange(1, $claims->lastPage()) as $page => $url)
							<li class="page-item {{ $page == $claims->currentPage() ? 'active' : '' }}">
								<a class="page-link" href="{{ $url }}">{{ $page }}</a>
							</li>
							@endforeach
							@if ($claims->hasMorePages())
							<li class="page-item">
								<a class="page-link" href="{{ $claims->nextPageUrl() }}" aria-label="Next">
									<span aria-hidden="true">&raquo;</span>
								</a>
							</li>
							@else
							<li class="page-item disabled">
								<span class="page-link" aria-disabled="true">&raquo;</span>
							</li>
							@endif
						</ul>
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade " id="claiming_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content edit_claims">

		</div>
	</div>
</div>
<div class="modal fade " id="reason_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Decline Reson</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<p class="reason_message"></p>
			</div>
		</div>
	</div>
</div>


@section('js')
<script>
	$('.reason_modal').on('click',function(){
		var reason = $(this).attr('data-id');
		$('.reason_message').text(reason);
		$('#reason_modal').modal('show');
	})
	$(document).on('change', '.status_select', function(event) {
		var val = $(this).val();
		if(val == 'declined'){
			$('.reason').prop('disabled',false).closest('.col-12').removeClass('d-none');
		}else{
			$('.reason').prop('disabled',true).closest('.col-12').addClass('d-none');
		}
	})

	$(document).on('click','.submit_employe_claim',function(){
		Swal.fire({
			title: "Respond to Claim",
			text: "Are you sure, You Want to Update this claim!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes",
		}).then((result) => {
			if (result.isConfirmed) {
				$('.employee_claim_response').submit();
				$('.submit_employe_claim').prop('disabled',true);
			}
		});
	})
	$(document).on('click','.editing_claim',function(){
		var id = $(this).attr('data-id');
		$(this).closest('.parent_div_body').find('.id_claims').prop('checked',true);
		var source = $('.source'+id).val();
		$.ajax({
			url: "{{url('company/employee/claim/edit')}}" + "/" + id,
			type: 'get',
			data: {
				source : source,
			},
			success: function(response) {
				$('.edit_claims').empty().append(response.component);
				$('.id_claims:checked').each(function(){
					var current_val = $(this).val();
					$('.employee_claim_response').append(`<input type="hidden" name="claim_id[]" value="${current_val}">`);
				});
				$('#claiming_modal').modal('show');
			}
		});
	})

</script>
@endsection
@include('employee.layout.footer')
