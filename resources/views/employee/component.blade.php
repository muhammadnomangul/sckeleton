<h4 class="claim_heading">Claims</h4>
@forelse($claims as $claim)
    <li class="search_item"><a href="{{ route('employee.claim') }}?id={{ $claim->id }}">{{ $claim->title }}</a></li>
@empty
<h6 class="no_claim_heading">No Record Found</h6>
@endforelse
<h4 class="claim_heading">Claimed Claims</h4>
@forelse($claimeds as $claimed)
<li class="search_item"><a href="{{route('employee.claim_history')}}?id={{ $claimed->id }}">{{$claimed->employee_claim->title}}</a></li>
@empty
<h6 class="no_claim_heading">No Record Found</h6>
@endforelse