</div>
</div>
<div class="modal fade " id="description_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Claim Description</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<p class="claim_description"></p>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="decline_notification" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Decline Claim</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form method="POST" action="" class="decline_notification_form" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
					<div class="row g-3">
						<div class="col-12">
							<label>Reason</label>
							<div class="input-group bg-input p-1">
								<input type="text" name="reason" class="form-control fs-12 bg-transparent border-0"
								placeholder="Reason For Claim Declining">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-center">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12">
							<button type="submit"
							class="btn btn_create text-white d-block px-5 fw-semibold">Submit</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- js area -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.14.0/dist/sweetalert2.min.js"></script>

<script type="text/javascript" src="{{asset('employe_styling/js/jquery-3.6.0.min.js')}}"></script>
<script type="text/javascript" src="{{asset('employe_styling/js/bootstrap.bundle.min.js')}}"></script>
<script type="text/javascript" src="{{asset('employe_styling/js/my_script.js')}}"></script>
<script type="text/javascript" src="{{asset('employe_styling/js/jquery.dataTables.js')}}"></script>
<script src='{{asset("employe_styling/js/ckeditor.js")}}'></script>
<script src='{{asset("employe_styling/js/dataTables.buttons.min.js")}}'></script>
<script src='{{asset("employe_styling/js/pdfmake.min.js")}}'></script>
<script src='{{asset("employe_styling/js/buttons.html5.min.js")}}'></script>
<script src='{{asset("employe_styling/js/buttons.print.min.js")}}'></script>
<script type="text/javascript" src="{{asset('employe_styling/js/dropdown.js')}}"></script>
<!-- Bottom slider script -->
<script src="{{asset('employe_styling/Slide/slider.js')}}"></script>
<!-- <script src="{{asset('employe_styling/Notification/noty.js')}}"></script>  -->
<script src="{{asset('employe_styling/Notification/profile1.js')}}"></script>
<script src="{{asset('employe_styling/Notification/profile2.js')}}"></script>
<script src="{{asset('employe_styling/Notification/profile3.js')}}"></script>


<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/dropify/dist/js/dropify.min.js"></script>


<script>
	$('.dropbtn1').on('click',function(){
		$(this).parent().find('.dropdown-content').toggle('show');
	})
</script>
<script>
	@if (Session::has('success'))
	toastr.success("{{ Session::get('success') }}");
	@endif


	@if (Session::has('info'))
	toastr.info("{{ Session::get('info') }}");
	@endif


	@if (Session::has('warning'))
	toastr.warning("{{ Session::get('warning') }}");
	@endif
	@if (Session::has('message'))
	toastr.success("{{ Session::get('success') }}");
	@endif

	@if (Session::has('error'))
	toastr.error("{{ Session::get('error') }}");
	@endif
</script>

@if (session('toast_error'))
<script>
	var errors = {!! json_encode(session('toast_error')) !!};
	for (var i = 0; i < errors.length; i++) {
		toastr.error(errors[i], 'Validation Error');
	}
</script>
@endif


<script type="text/javascript">

		// menu toggle
	let toggle = document.querySelector('.toggle');
	let navigation = document.querySelector('.navigation');
	let main = document.querySelector('.main');
	let img = document.querySelector('.img');
	let img1 = document.querySelector('.img1');


		// add hovered class in selected list item

	let list = document.querySelectorAll('.navigation li');
	function activeLink() {
		list.forEach((item) =>
			item.classList.remove('hovered'));
		this.classList.add('hovered');
	}
	list.forEach((item) =>
		item.addEventListener('mouseover', activeLink)
		);

	$(document).ready(function () {
		$('.modal_claim').on('click', function() {
			var id = $(this).attr('data-id');
			$('.claim_id').val(id);
			$('.claiming_modal').modal('show');
		});
		$('.btn-close').on('click',function(){
			$('.claiming_modal').modal('hide');
		})
		$('.dropify').dropify();
	});

</script>
<script>
	$(document).ready(function(){
		$('#logout-form').attr('action' ,'{{route("logout")}}');
		$('#logout-form').append('@csrf');
		$('#claim_table').DataTable({
			"paging": false, 
			"searching": false,
			"ordering": true, 
			"info": false, 
			"order": [],
		});
	})
	$('.add_more_supporting_document').on('click',function(){
		var length = $('.upload_supporting').length;
		$('.append_support').append(`
			<div class="col-12 col-sm-12 col-md-12 col-lg-12 appended_supports" style="text-align:right;">
			<div class="bg-input p-2 text-center">
			<label class="text-capitalize d-block text-center " for="uploadid${length}">
			<div>
			<img src="{{asset('employe_styling/image/icons/cloud.png')}}" class="img-fluid">
			</div>
			<p class="mb-0 text-capitalize fs-12 Browse_text"> Upload Supporting Document</p>
			</label>
			<input type="file" class="d-none upload_supporting" required id="uploadid${length}" name="document[]">
			</div>
			<span class="remove_support" style="cursor:pointer;color: #E82583;font-size:12px">Remove</span>
			</div>`);
	})
	$(document).on('click','.remove_support',function(){
		$(this).closest('.appended_supports').remove();
	})
	$(document).on({
		mouseenter: function() {
			var text = $(this).attr('data-id');
			$('.claim_description').text(text);
			$('#description_modal').modal('show');
		},
	}, '.description_modal');
	$('.search_bar_field').on('input',function(){
		var val = $(this).val();
		$.ajax({
			method : 'GET',
			url : '{{route("employee.suggestion")}}',
			data : {
				val : val,
			},
			success:function(response){
				$('.search_list').empty().append(response.component);
				$('.search_bar').removeClass('d-none');
			}
		})
	})
	$(document).on('change','.upload_supporting', function() {
		const file = $(this)[0].files[0];
		if (file) {
			const fileName = file.name;
			$(this).parent().find('.Browse_text').text(fileName);
		} else {
			$(this).parent().find('.Browse_text').text('Upload Supporting Document');
		}
	});
	$('.notifiction_drop').on('click', function() {
		$('.notification-buttons').css('display', 'none');
		$(this).find('.notification-buttons').css('display', 'flex');
	});
	$('.fa-bell').on('click',function(){
		if ($('#dropdownAbout').css('display') === 'block') {
			$('#dropdownAbout').css('display', 'none');
		} else {
			$('#dropdownAbout').css('display', 'block');
		}
		var count = parseInt($('.notification_count').text());
		var ids =[];
		$('.notification_id').each(function(){
			ids.push($(this).val());
		})
		if(count > 0){
			$.ajax({
				method : "get",
				url : "{{route('update_count')}}",
				data : {
					id : ids,
				},
				success:function(response){
					$('.notification_count').text(0);
				}
			})
		}
	})
	$('.decline_notification').on('click',function(){
		var url = $(this).attr('data-id');
		$('.decline_notification_form').attr('action',url);
		$('#decline_notification').modal('show');
	})
	$('.toogle_navigation').on('click',function(){
		$('.navigation').toggleClass('active');
		$('.main').toggleClass('active');
		$('.img, .imgsize').toggleClass('active');
		if($('.navigation').hasClass('active')){
			$('.open_navigation').removeClass('d-none');
			$('.close_navigation').addClass('d-none');
		}else{
			$('.open_navigation').addClass('d-none');
			$('.close_navigation').removeClass('d-none');
		}	
		$.ajax({
			method : 'GET',
			url : '{{route("employee.sidebar")}}',
			success:function(response){

			}
		})
	})
</script>
@if(session('reason'))
<script>
	$(document).ready(function(){
		var id = "{{ session('id') }}";
		var link = "{{ session('link') }}";
		var user = "{{ session('user') }}";
		var baseUrl = '{{ url("/") }}';
		$('.decline_notification_form').attr('action', baseUrl + '/' +link + "?id=" + id + "&user=" + user);
		$('#decline_notification').modal('show');
	});
</script>
@endif

@yield('js')
</body>

</html>