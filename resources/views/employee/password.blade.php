@section('pageTitle')
Profile
@endsection
@include('employee.layout.header')
<style>
	.dropify-wrapper {
		width: 100% !important;
		height: 100% !important;
	}
	.dropify-clear{
		display: none !important;
	}
	.select2-container--default{
		width: 100% !important;
	}
	.dropify-render img{
		height: 100% !important;
		width: 100% !important;
	}
</style>

<!-- left side section start -->
<div class="mt-5 mb-3">
	<div class="row g-3">
		<div class="col-lg-12" style="margin-top:10px">
			<h1 style="margin-bottom:20px">Edit Your Password</h1>
			<form class=" row g-3" method="POST" action="{{route('employee.password.update')}}" enctype="multipart/form-data">
				@csrf
				<div class="col-lg-12 mt-2 row" style="margin-top:20px">
					<div class="col-lg-12 padding_bottom">
						<label>Old Password</label>
						<div class="input-group bg-input mt-2 p-1">
							<input type="password" placeholder="Old Password" name="old_password" class="form-control fs-14 bg-transparent border-0">
						</div>
					</div>
				</div>
				<div class="col-lg-12 row" style="margin-top:20px">
					<div class="col-lg-12 padding_bottom">
						<label>New Password</label>
						<div class="input-group bg-input mt-2 p-1">
							<input type="password" placeholder="New Password" name="password" class="form-control fs-14 bg-transparent border-0">
						</div>
					</div>
				</div>
				<div class="col-lg-12 row" style="margin-top:20px">
					<div class="col-lg-12 padding_bottom">
						<label>Confirm Password</label>
						<div class="input-group bg-input mt-2 p-1">
							<input type="password" placeholder="Confirm Password" name="password_confirmation" class="form-control fs-14 bg-transparent border-0">
						</div>
					</div>
				</div>
				<div class="col-md-12 d-flex justify-content-center">
					<div class="mt-3" style="width:30%">
						<button class="text-decoration-none bg-pink fs-13 fw-bold text-center rounded-3 d-block text-white p-3 w-100">
							Submit
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- left side section end -->
</div>
</div>
@include('employee.layout.footer')
