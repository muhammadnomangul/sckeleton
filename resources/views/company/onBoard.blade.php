@section('pageTitle')
On Board Employee
@endsection
@include('company.layout.header')
<style>
	.claim_edit_button:hover{
		background-color:#E82583;
		color: white;
	}
</style>
<div class="mt-5 mb-3">
	<div class="row g-3">
		<div class="col-lg-9">
			<div class="alert alert-success" id="success-msg" style="display: none;">
			</div>
			<div class="alert alert-danger" id="error-msg" style="display: none;">
			</div>
			<form class="row g-3" id="onboard-employee-form" method="POST">
				@csrf
				<div class="col-lg-12">
					<div>
						<label class="text-capitalize mb-2 fs-14 fw-bold">
							Enter New Staff Details
						</label>
					</div>

					<input type="hidden" name="id" id="id" value="0">
					<div class="input-group bg-input mt-2 p-1">
						<input type="text" class="form-control fs-14 bg-transparent border-0"
						placeholder="Staff ID" name="staff_id" id="staff_id" required>
					</div>
				</div>

				<div class="col-lg-12">
					<div class="input-group bg-input mt-2 p-1">
						<select name="title"
						class="form-control fs-14 bg-transparent border-0" required>
						<option value>Title</option>
						<option value="mr">Mr</option>
						<option value="mrs">Mrs</option>
						<option value="miss">Miss</option>
						<option value="dr">Dr</option>
						<option value="prof">Prof</option>
						<option value="rev">Rev</option>
						<option value="hon">Hon</option>
						<option value="capt">Capt</option>
					</select>
				</div>
			</div>

			<div class="col-lg-12">
				<div class="input-group bg-input mt-2 p-1">
					<input type="text" class="form-control fs-14 bg-transparent border-0"
					placeholder="First Name" name="first_name" id="first_name" required>
				</div>
			</div>

			<div class="col-lg-12">
				<div class="input-group bg-input mt-2 p-1">
					<input type="text" class="form-control fs-14 bg-transparent border-0"
					placeholder="Last Name" name="last_name" id="last_name" required>
				</div>
			</div>

			<div class="col-lg-12">
				<div class="input-group bg-input mt-2 p-1">
					<input placeholder="Date of Birth" id="dob" name="dob"
					type="text"
					onfocus="(this.type='date')"
					onblur="(this.type='text')"
					class="form-control fs-14 bg-transparent border-0">
				</div>
			</div>
			<div class="col-lg-12">
				<div class="input-group bg-input mt-2 p-1">
					<select name="gender"
					class="form-control fs-14 bg-transparent border-0" required>
					<option>Select Gender</option>
					<option value="male">Male</option>
					<option value="female">Female</option>
					<option value="other">Other</option>
				</select>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="input-group bg-input mt-2 p-1">
				<input type="text" class="form-control fs-14 bg-transparent border-0"
				placeholder="Phone" name="phone_number" id="phone_number" required>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="input-group bg-input mt-2 p-1">
				<input type="text" class="form-control fs-14 bg-transparent border-0"
				placeholder="Email" name="email" id="email" required>
			</div>
		</div>
		@if(false)
		<div class="col-lg-12">
			<div class="input-group bg-input mt-2 p-1">
				<input type="password" class="form-control fs-14 bg-transparent border-0"
				placeholder="Password" name="password" id="password" required>
			</div>
		</div>

		<div class="col-lg-12">
			<div class="input-group bg-input mt-2 p-1">
				<select name="state_of_origin"
				class="form-control fs-14 bg-transparent border-0" required>
				<option>State of origin</option>
				<option value="america">America</option>
				<option value="nigeria">Nigeria</option>
			</select>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="input-group bg-input mt-2 p-1">
			<select name="nationality"
			class="form-control fs-14 bg-transparent border-0" required>
			<option>Nationality</option>
			<option value="america">America</option>
			<option value="nigeria">Nigeria</option>
		</select>
	</div>
</div>

@endif

<div class="row" id="all-fields">

</div>
</form>
</div>
<div class="col-lg-3">
	<div>
		<label class=" text-capitalize fw-bold fs-14">Upload Staff in Bulk</label>
		<form action="{{route('company.bulk-employee-onboard')}}" method="POST"
		enctype="multipart/form-data" onsubmit="return validatefrom()">
		@csrf
		<div class="bg-input p-5 text-center mt-3">
			<label class="text-capitalize d-block text-center " for="uploadid">
				<div>
					<img src="{{asset('assets/admin/image/icons/cloud.png')}}" class="img-fluid">
				</div>
				<p class="mb-0 text-capitalize">Upload Document</p>
			</label>
			<input type="file" class="d-none" id="uploadid" name="file" accept=".xls,.xlsx" required>
		</div>
		<div class="mt-4">
			<span class="fs-14 d-block my-3">Merge Columns</span>
			<table class="table align-middle border-top ">
				<tbody>
					<tr>
						<td>
							<span class="fs-12">1.</span>
							<span class="ps-3 fs-12">First Name</span>
						</td>
						<td class="text-end">
							<div>
								<select name="first_name" required id="first_name_b"
								class="select select-c fs-12 bg-transparent border-0">
								<option value="0">Match Column</option>
								<option selected value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
								<option value="5">5</option>
							</select>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<span class="fs-12">2.</span>
						<span class="ps-3 fs-12">Last Name</span>
					</td>
					<td class="text-end">
						<div>
							<select name="last_name" id="last_name_b" required
							class="select select-c fs-12 bg-transparent border-0">
							<option value="0">Match Column</option>
							<option value="1">1</option>
							<option selected value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<span class="fs-12">3.</span>
					<span class="ps-3 fs-12">Email</span>
				</td>
				<td class="text-end">
					<div>
						<select name="email" id="email_b" required
						class="select select-c fs-12 bg-transparent border-0">
						<option value="0">Match Column</option>
						<option value="1">1</option>
						<option value="2">2</option>
						<option selected value="3">3</option>
						<option value="4">4</option>
						<option value="5">5</option>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<span class="fs-12">4.</span>
				<span class="ps-3 fs-12">Phone</span>
			</td>
			<td class="text-end">
				<div>
					<select name="phone_number" id="phone_number_b" required
					class="select select-c fs-12 bg-transparent border-0">
					<option value="0">Match Column</option>
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option selected value="4">4</option>
					<option value="5">5</option>
				</select>
			</div>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<button type="submit"
			class="text-decoration-none bg-pink fs-13 fw-bold text-center rounded-3 d-block text-white p-3 w-100">
			Upload
		</button>
	</td>
</tr>
</tbody>
</table>
</div>
</form>
</div>
</div>

<div class="col-lg-4 complete-all-fields">
	<div class="mt-3">
		<button id="complete-all-fields"
		class="text-decoration-none bg-pink fs-13 fw-bold text-center rounded-3 d-block text-white p-3 w-100">
		Complete all Fields
	</button>
</div>
</div>

<div class="col-lg-4 remove-all-fields" style="display: none;">
	<div class="mt-3">
		<button id="remove-all-fields"
		class="text-decoration-none bg-danger fs-13 fw-bold text-center rounded-3 d-block text-white p-3 w-100">
		Remove all Fields
	</button>
</div>
</div>


<div class="col-lg-4">
	<div class="mt-3">
		<button id="register-employee" type="button"
		class="text-decoration-none bg-pink fs-13 fw-bold text-center rounded-3 d-block text-white p-3 w-100">
		Request Completion
	</button>
</div>
</div>
<div class="col-lg-4">
	<div class="mt-3">
		<button id="reset-employee-data"
		class="text-decoration-none bg-pink fs-13 fw-bold text-center rounded-3 d-block text-white p-3 w-100">
		Reset Page
	</button>
</div>
</div>
</div>
</div>
@section('js')


@endsection
@include('company.layout.footer')
