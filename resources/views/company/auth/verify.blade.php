
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Signup Step 1</title>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <script src="https://kit.fontawesome.com/00b231478f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
</head>
<style>
    .loader {
      border: 4px solid #f3f3f3;
      border-radius: 50%;
      border-top: 4px solid pink;
      width: 25px;
      height: 25px;
      -webkit-animation: spin 2s linear infinite; /* Safari */
      animation: spin 2s linear infinite;
  }
  @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
  }
</style>
<body>

    <section class="signup_section">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="signup_section_leftside">
                    <div>
                        <h1 class="hero_section_heading">Start your Cleon journey in 5 simple steps</h1>
                        <span class="pink-hr mt-3"></span>
                        <p class="text-white mt-4">Let’s solve all your HR problems</p>
                        <div>
                            <a href="#" class="text-decoration-none btn btn_explore px-5">Explore</a>
                            <a href="#" class="text-decoration-none btn btn_seemore px-5">See More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <!-- header start from here -->
                <header class="header">
                    <nav class="navbar navbar-expand-lg">
                        <div class="container-fluid">
                            <a class="navbar-brand" href="#">Cleon<span>HR</span></a>
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto ms-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Pricing</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Case Studies</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">More</a>
                                </li>
                            </ul>
                            <div>
                                <a href="{{url('/login')}}"
                                class="text-decoration-none btn btn_started">LOGIN</a>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
            <!-- header end here -->
            <div class="position-relative mt-5 me-lg-5">
                <ul class=" ps-0 navbar-nav lh-lg">

                    <li class="position-relative">
                        <div class="ps-5 collaspE2 mb-4" style="display:block;">
                            <form class="d-flex align-items-center">

                                <div>
                                    <input type="text" maxlength="1" class="form-control inputheightwidt" name="input1">
                                </div>
                                <div class="ms-3">
                                    <input type="text" class="form-control inputheightwidt" maxlength="1" name="input2">
                                </div>
                                <div class="ms-3">
                                    <input type="text" class="form-control inputheightwidt" maxlength="1" name="input3">
                                </div>
                                <div class="ms-3">
                                    <input type="text" class="form-control inputheightwidt" maxlength="1" name="input4">
                                </div>
                                <div class="ms-3">
                                    <input type="text" class="form-control inputheightwidt" maxlength="1" name="input5">
                                </div>
                                <div class="loader d-none" style="margin-top:5px;margin-left: 18px;"></div>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<!-- js area -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.bundle.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.clickbtn').on('click', function () {
            $(".collaspE").show();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.one').show();
            $('.two').show();
            $('.three').show();
            $('.four').show();
            $('.five').show();
            $('.imgdisplay').hide();
            $('.imgdisplay1').hide();
            $('.imgdisplay2').hide();
            $('.imgdisplay3').hide();
            $('.imgdisplay4').hide();

        })
        $('.clickbtn1').on('click', function () {
            $(".collaspE").hide();
            $(".collaspE1").show();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.imgdisplay').show();
            $('.one').hide();
            $('.imgdisplay2').hide();
            $('.three').show();

        })
        $('.clickbtn2').on('click', function () {
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").show();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.imgdisplay').show();
            $('.imgdisplay1').show();
            $('.two').hide();
            $('.one').hide();
        })
        $('.clickbtn3').on('click', function () {
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").show();
            $(".collaspE4").hide();
            $('.imgdisplay1').show();
            $('.imgdisplay2').show();
            $('.two').hide();
            $('.three').hide();
            $('.four').show();
            $('.imgdisplay3').hide();
        })
        $('.clickbtn4').on('click', function () {
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").show();
            $('.imgdisplay').show();
            $('.imgdisplay1').show();
            $('.imgdisplay2').show();
            $('.imgdisplay3').show();
            $('.one').hide();
            $('.two').hide();
            $('.three').hide();
            $('.four').hide();
        })

        $('.cbn').on('click', function () {
            $(".collaspE").show();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.one').show();
            $('.two').show();
            $('.three').show();
            $('.four').show();
            $('.five').show();
            $('.imgdisplay').hide();
            $('.imgdisplay1').hide();
            $('.imgdisplay2').hide();
            $('.imgdisplay3').hide();
            $('.imgdisplay4').hide();

        })
        $('.cbn1').on('click', function () {
            $(".collaspE").hide();
            $(".collaspE1").show();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.imgdisplay').show();
            $('.one').hide();
            $('.imgdisplay2').hide();
            $('.three').show();

        })
        $('.cbn2').on('click', function () {
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").show();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.imgdisplay').show();
            $('.imgdisplay1').show();
            $('.two').hide();
            $('.one').hide();
        })
        $('.cbn3').on('click', function () {
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").show();
            $(".collaspE4").hide();
            $('.imgdisplay1').show();
            $('.imgdisplay2').show();
            $('.two').hide();
            $('.three').hide();
            $('.four').show();
            $('.imgdisplay3').hide();
        })
        $('.cbn4').on('click', function () {
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").show();
            $('.imgdisplay').show();
            $('.imgdisplay1').show();
            $('.imgdisplay2').show();
            $('.imgdisplay3').show();
            $('.one').hide();
            $('.two').hide();
            $('.three').hide();
            $('.four').hide();
        })

    })
</script>

<script>
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 10,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
        },
    });
</script>
<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>


@if (session('toast_error'))
<script>
    var errors = {!! json_encode(session('toast_error')) !!};
    for (var i = 0; i < errors.length; i++) {
        toastr.error(errors[i], 'Validation Error');
    }
</script>
@endif
<script>
    @if (Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    @endif


    @if (Session::has('info'))
    toastr.info("{{ Session::get('info') }}");
    @endif


    @if (Session::has('warning'))
    toastr.warning("{{ Session::get('warning') }}");
    @endif
    @if (Session::has('message'))
    toastr.success("{{ Session::get('success') }}");
    @endif

    @if (Session::has('error'))
    toastr.error("{{ Session::get('error') }}");
    @endif
</script>

</body>

</html>