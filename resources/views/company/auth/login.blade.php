<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">

    <title>Login Page</title>
    <link rel="stylesheet" type="text/css" href="{{asset('cleon_styling/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('cleon_styling/css/style.css')}}">
    <script src="https://kit.fontawesome.com/00b231478f.js" crossorigin="anonymous"></script>
</head>
<style>
    @media(max-width : 1160px ) and (min-width : 991px){
        .margin_bottom{
            margin-bottom: 15px !important;
        }
    }
    @media(max-width : 366px ){
        .margin_bottom{
            margin-bottom: 15px !important;
        }
    }
</style>
<body>

    <section class="signup_section">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="signup_section_leftside">
                    <div>
                        <h1 class="hero_section_heading mb-3">Welcome back, Let’s get you into your account</h1>
                        <span class="pink-hr"></span>
                        <p class="text-white mt-4">Let’s solve all your HR problems</p>
                        <div class="mt-4">
                            <a href="#" class="text-decoration-none me-2 btn bg-pink py-2 text-white px-5">Explore</a>
                            <a href="#" class="text-decoration-none btn btn_seemore py-2 px-5">See More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">

                <div class="position-relative p-4 mt-5 me-lg-5">
                    <div class="my-3">
                        <img src="{{asset('cleon_styling/image/logo.png')}}" class="img-fluid logo-size-log">
                    </div>
                    <div class="ps-4 mt-4">
                        <h4 class="fw-bold text-uppercase">login</h4>
                        <form class="row g-3" method="POST" action="{{route('company.login')}}">
                            @csrf
                            <input type="hidden" name="company" id="company"
                            value="company">
                            <div class="col-lg-8">
                                <div class="mt-3">
                                    <div class="input-group border rounded-2 p-2 margin_bottom" style="position:relative;">
                                        <input type="email" id="email" class="form-control border-0 @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required
                                        placeholder="user.doe@officialemail.com"
                                        aria-label="user.doe@officialemail.com" aria-describedby="basic-addon2">
                                        <span class="input-group-text bg-transparent border-0" id="basic-addon2">
                                            <img src="{{asset('cleon_styling/image/at.png')}}" class="img-fluid">
                                        </span>
                                        @error('email')
                                        <span class="invalid-feedback" style="position: absolute;top: 50px;" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8">
                                <div class="mt-3">
                                    <div class="input-group border rounded-2 p-2">
                                        <input type="password" class="form-control border-0 @error('password') is-invalid @enderror" name="password" required placeholder="Password"
                                        aria-label="Password" aria-describedby="basic-addon3" id="passinput">
                                        <span class="input-group-text bg-transparent border-0" id="basic-addon3"
                                        onclick="showpass();">
                                        <img src="{{asset('cleon_styling/image/pass-eye.png')}}" class="img-fluid">
                                    </span>
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div>
                                <p style="font-size:15px">
                                    By login, you agree to our <a href="#"
                                    class="text-decoration-none text-pink fw-bold">Terms and Conditions</a>
                                </p>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="d-flex align-items-center justify-content-between">
                                <div>
                                    <button
                                    class="submit_form border-0 bg-pink rounded-3 btncust text-white d-flex align-items-center justify-content-between w-100" style="text-decoration: none;" type="submit">
                                    <p class="mb-0 ps-2">
                                        Login
                                    </p>
                                    <div class="text-end ms-3">
                                        <img src="{{asset('cleon_styling/image/submit-arrow.png')}}" class="img-fluid width-20">
                                    </div>
                                </button>
                            </div>
                            <div>
                                <a href="{{ route('company.forget') }}" class="text-decoration-none text-dark">Forgot Password ?</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="mt-4">
                            <p>
                                Don’t have an account ?
                                <a href="{{route('company.register')}}"
                                class="text-decoration-none text-pink fw-bold">
                                Create an account
                            </a>
                        </p>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="d-flex align-items-center justify-content-between">
                        <a href="{{ url('auth/google') }}" class="text-decoration-none d-block">
                            <img src="{{asset('cleon_styling/image/login-google.png')}}" class="img-fluid">
                        </a>
                        <a href="{{ url('auth/facebook') }}" class="text-decoration-none d-block ms-3">
                            <img src="{{asset('cleon_styling/image/login-fb.png')}}" class="img-fluid">
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</section>


<!-- js area -->
<script src="{{asset('cleon_styling/js/jquery-3.6.0.js')}}"></script>
<script type="text/javascript" src="{{asset('cleon_styling/js/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

<script type="text/javascript">
    function showpass() {
        const passinput = document.getElementById('passinput');

        if (passinput.type === 'password') {
            passinput.type = 'text'
        } else {
            passinput.type = 'password'
        }
    }
</script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script> 


<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>



<script>
    @if (Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    @endif


    @if (Session::has('info'))
    toastr.info("{{ Session::get('info') }}");
    @endif


    @if (Session::has('warning'))
    toastr.warning("{{ Session::get('warning') }}");
    @endif
    @if (Session::has('message'))
    toastr.success("{{ Session::get('success') }}");
    @endif

    @if (Session::has('error'))
    toastr.error("{{ Session::get('error') }}");
    @endif
</script>

@if (session('toast_error'))
<script>
    var errors = {!! json_encode(session('toast_error')) !!};
    for (var i = 0; i < errors.length; i++) {
        toastr.error(errors[i], 'Validation Error');
    }
</script>
@endif
<script>

    $('#reset_password').submit(function(e) {
        e.preventDefault(); 
        var email = $('#email').val();
        $.ajax({
            type: 'GET',
            url: '{{route("check.user")}}',
            data: {
                email: email,
            },
            success: function(response) {
                if ( response === 'company') {
                    $('#reset_password').off('submit').submit();
                } else {
                    toastr.error('Email not found or user is not a company');
                }
            },
            error: function() {
                alert('An error occurred during the check');
            }
        });
    });

</script>

</body>

</html>

