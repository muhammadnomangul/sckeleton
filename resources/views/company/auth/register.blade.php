
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Signup</title>
    <link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    <script src="https://kit.fontawesome.com/00b231478f.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />
    <script src="https://checkout.flutterwave.com/v3.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/css/intlTelInput.css" rel="stylesheet">
</head>
<style>
    .loader {
      border: 4px solid #f3f3f3;
      border-radius: 50%;
      border-top: 4px solid pink;
      width: 25px;
      height: 25px;
      -webkit-animation: spin 2s linear infinite; /* Safari */
      animation: spin 2s linear infinite;
  }
  @-webkit-keyframes spin {
      0% { -webkit-transform: rotate(0deg); }
      100% { -webkit-transform: rotate(360deg); }
  }

  @keyframes spin {
      0% { transform: rotate(0deg); }
      100% { transform: rotate(360deg); }
  }
  .border-pink {
    border: 4px solid var(--pink-color) !important;
}
.select2-selection--single{
    height: 40px !important;
}
.select2-container--default .select2-selection--single .select2-selection__arrow{
    height: 39px !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered{
    line-height: 37px !important;
}
.intl-tel-input{
    width: 100%;
}
.country-list{
    width: 315px;
    overflow-x: hidden;
}

.selected {
    border: 2px solid #fd0d5c; /* Change to your selected border style */
    /* Add any other styles for the selected state */
    position: relative;
}
select::placeholder {
    color: #999; /* Adjust the color as needed */
    opacity: 0.7; /* Adjust the opacity to control the fading effect */
}
.faded-placeholder::placeholder {
    color: #999; /* Adjust the color as needed */
    opacity: 0.7; /* Adjust the opacity to control the fading effect */
}
.selected::before {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgb(255 0 0 / 50%); /* Adjust the color and opacity as needed */
    pointer-events: none; /* Make sure the overlay doesn't interfere with clicks */
}
@media (min-width: 992px) {
  .code_width {
     width: 75% !important;
 }
}
@media (min-width: 701px) and (max-width: 992px) {
  .code_width {
     width: 55% !important;
 }
}
@media (min-width: 500px) and (max-width: 700px) {
  .code_width {
     width: 65% !important;
 }
}
@media (min-width: 300px) and (max-width: 500px) {
  .code_width {
     width: 80% !important;
 }
}
@media (max-width: 995px) {
    .signup_section_leftside {
        width : 100% !important;
        position : relative !important;
    }
}

body{
    /*background: #FBB231;*/
}

.flip-container {
    -webkit-perspective: 1000;
    -moz-perspective: 1000;
    -ms-perspective: 1000;
    perspective: 1000;

    -ms-transform: perspective(1000px);
    -moz-transform: perspective(1000px);
    -moz-transform-style: preserve-3d;
    -ms-transform-style: preserve-3d;
}


/* START: Accommodating for IE */

.flip-container.hover .back {
    -webkit-transform: rotateY(0deg);
    -moz-transform: rotateY(0deg);
    -o-transform: rotateY(0deg);
    -ms-transform: rotateY(0deg);
    transform: rotateY(0deg);
}

.flip-container.hover .front {
    -webkit-transform: rotateY(180deg);
    -moz-transform: rotateY(180deg);
    -o-transform: rotateY(180deg);
    transform: rotateY(180deg);
}

/* END: Accommodating for IE */

.flip-container, .front, .back {
    width: 435px;
    height: 258px;
    margin: 25px auto;
}

/* flip speed goes here */
.flipper {
    -webkit-transition: 0.6s;
    -webkit-transform-style: preserve-3d;
    -ms-transition: 0.6s;

    -moz-transition: 0.6s;
    -moz-transform: perspective(1000px);
    -moz-transform-style: preserve-3d;
    -ms-transform-style: preserve-3d;

    transition: 0.6s;
    transform-style: preserve-3d;

    position: relative;
}
.inputheightwidt {
    width : 100%;
    height : 60px;
}


/* hide back of pane during swap */
.front, .back {
    border: 1px solid #E3E3E3;
    -webkit-backface-visibility: hidden;
    -moz-backface-visibility: hidden;
    -ms-backface-visibility: hidden;
    backface-visibility: hidden;

    -webkit-transition: 0.6s;
    -webkit-transform-style: preserve-3d;
    -webkit-transform: rotateY(0deg);

    -moz-transition: 0.6s;
    -moz-transform-style: preserve-3d;
    -moz-transform: rotateY(0deg);

    -o-transition: 0.6s;
    -o-transform-style: preserve-3d;
    -o-transform: rotateY(0deg);

    -ms-transition: 0.6s;
    -ms-transform-style: preserve-3d;
    -ms-transform: rotateY(0deg);

    transition: 0.6s;
    transform-style: preserve-3d;
    transform: rotateY(0deg);

    position: absolute;
    top: 0;
    left: 0;
}
.namecard{
    width: 115px !important;
}

.card-expiry-wrap {
    margin-left: 188px;
    margin-top: -83px;
}

.cardholder-name-wrap {
    display: inline-block;
    vertical-align: middle;
    margin-right: 186px;
}
/* fix for firefox */
@-moz-document url-prefix() {
    .cardholder-name-wrap {
        margin-right: 10px;
    }
}
/* front pane, placed above back */
.front {
    -webkit-transform: rotateY(0deg);
    -ms-transform: rotateY(0deg);

    z-index: 2;
}

/* back, initially hidden pane */
.back {
    -webkit-transform: rotateY(-180deg);
    -moz-transform: rotateY(-180deg);
    -o-transform: rotateY(-180deg);
    -ms-transform: rotateY(-180deg);
    transform: rotateY(-180deg);
}

/* card styles goes here */

.card{
    background: linear-gradient( rgba(255, 255, 255, 1) 25%, rgba(255,255,255,0.2) ), #ddd;
    border-radius: 4px;
    -webkit-box-shadow: 0 2px 10px rgba(51,51,51, 0.2);
    -moz-box-shadow: 0 2px 10px rgba(51,51,51, 0.2);
    box-shadow: 0 2px 10px rgba(51,51,51, 0.2);
    box-sizing: border-box;
    font-family: 'Source Sans Pro', sans-serif;
    padding: 10px;
}

.card.back{
    text-align: right;
}

.card-provider{
    text-align: right;
    min-height: 40px; /* to remove image loading div resize */
}

.card-provider .c-image{
    display: inline-block;
    vertical-align: middle;
    margin-left: 18px;
}

.input-title,
.disclaimer{
    color: #737373;
    font-weight: 400;
    font-size: 15px;
    display: block;
}

.input-field{
    display: block;
    padding: 5px 10px;
    border: 1px solid #D9D9D9;
    border-radius: 4px;
    margin: 10px 0 15px;
    font-size: 16px;
    box-shadow: inset 0 1px 5px rgba(0,0,0,0.25);
}

.input-field.options{
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
}

.input-field.cvv-code{
    width: 100px;
}

.input-field:focus{
    outline: 0;
}

.input-field::-webkit-input-placeholder {
    color: #BBBBBB;
}

.input-field:-moz-placeholder { /* Firefox 18- */
    color: #BBBBBB;
}

.input-field::-moz-placeholder {  /* Firefox 19+ */
    color: #BBBBBB;
}

.input-field:-ms-input-placeholder {
    color: #BBBBBB;
}

.card-number{
    width: 160px;
}
.cardholder-name{
    width: 115px;
}

.cardholder-name-wrap{
    display: inline-block;
    vertical-align: middle;
    margin-right: 20px;
}

/* fix for firefox */

@-moz-document url-prefix() {
    .cardholder-name-wrap{
        margin-right: 10px;
    }
}

.custom-file-button {
    input[type="file"] {
        margin-left: -2px !important;

        &::-webkit-file-upload-button {
            display: none;
        }
        &::file-selector-button {
            display: none;
        }
    }

    &:hover {
        label {
            background-color: #dde0e3;
            cursor: pointer;
        }
    }
}
.card-expiry-wrap{
    display: inline-block;
    vertical-align: middle;
}

.custom-dropdown{
    position: relative;
    display: inline-block;
    margin-right: 5px;
}

.custom-dropdown .options{
    padding-right: 33px;
}

.custom-dropdown .select-icon{
    position: absolute;
    width: 12px;
    top: 24px;
    right: 13px;
}

.black-stripe{
    height: 50px;
    background: #333;
    margin: 30px -11px 20px;
}

.cvv-wrap{
    display: inline-block;
    text-align: left;
    margin-bottom: 6px;
}

.disclaimer{
    text-align: left;
}



/* navigation buttons styles */


.buttons{
    text-align: center;
    padding-top: 20px;
}

.buttons button{
    cursor: pointer;
}

.buttons button:focus{
    outline: 0;
}

.buttons button:disabled{
    cursor: not-allowed;
}

.next,
.prev{
    padding: 10px 20px;
    border: 0;
    background: #fff;
    border-radius: 3px;
    margin: 0 5px;
    font-size: 15px;
}


/*== start of code for tooltips ==*/
.tool {
    cursor: help;
    position: relative;
}
.input-group-text {
    font-size : 12px !important;
}

/*== common styles for both parts of tool tip ==*/
.tool::before,
.tool::after {
    left: 50%;
    opacity: 0;
    position: absolute;
    z-index: -100;
}

.tool:hover::before,
.tool:focus::before,
.tool:hover::after,
.tool:focus::after {
    opacity: 1;
    transform: scale(1) translateY(0);
    z-index: 100;
}


/*== pointer tip ==*/
.tool::before {
    border-style: solid;
    border-width: 1em 0.75em 0 0.75em;
    border-color: #3E474F transparent transparent transparent;
    bottom: 100%;
    content: "";
    margin-left: -0.5em;
    transition: all .65s cubic-bezier(.84,-0.18,.31,1.26), opacity .65s .5s;
    transform:  scale(.6) translateY(-90%);
}

.tool:hover::before,
.tool:focus::before {
    transition: all .65s cubic-bezier(.84,-0.18,.31,1.26) .2s;
}


/*== speech bubble ==*/
.tool::after {
    background: #3E474F;
    border-radius: .25em;
    bottom: 180%;
    color: #EDEFF0;
    content: attr(data-tip);
    margin-left: -8.75em;
    padding: 1em;
    transition: all .65s cubic-bezier(.84,-0.18,.31,1.26) .2s;
    transform:  scale(.6) translateY(50%);
    width: 17.5em;
}

.tool:hover::after,
.tool:focus::after  {
    transition: all .65s cubic-bezier(.84,-0.18,.31,1.26);
}

@media (max-width: 760px) {
    .tool::after {
        font-size: .75em;
        margin-left: -5em;
        width: 10em;
    }
}
/* Style for the overlay */
.overlay {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 0, 0, 0.5); /* Default red overlay, you can change this color */
    opacity: 0;
    transition: opacity 0.5s;
}

:root {
    --swiper-theme-color: #ff69b4; /* Pink color code */
}
::selection {
    /*background-color: black;*/
    /*color: red;*/
}
#getStartedBtn {
    cursor: pointer;
    background-color: #3498db;
    color: #fff;
    padding: 8px 20px;
    border-radius: 5px;
}

#dropdownContent {
    display: none;
    position: absolute;
    z-index: 1;
    background-color: #fff;
    box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
    padding: 10px;
    right: 0;
    border-radius: 5px; /* Add border-radius for a curved look */
}

#dropdownContent ul {
    list-style: none;
    margin: 0;
    padding: 0;
}

#dropdownContent ul li {
    margin-bottom: 10px;
}

#dropdownContent ul li a {
    display: block;
    padding: 8px;
    color: #333;
    text-decoration: none;
    border-radius: 3px; 
    transition: background-color 0.3s;
}

#dropdownContent ul li a:hover {
    background-color: #E82583;
    color: #fff;
}
.select2-container--default .select2-selection--single .select2-selection__rendered{
    color: black !important;
    font-size: 13px !important;
}
.select2-container--default .select2-selection--single{
    border : 1px solid #ced4da !important;
}
.selected-dial-code{
    font-size: 12px !important;
}
.col-lg-6{
    margin-top:10px !important;
}
</style>
<body>

    <section class="signup_section">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="signup_section_leftside" style="position:fixed;width: 49%;">
                    <div>
                        <h1 class="hero_section_heading">Start your Cleon journey in 5 simple steps</h1>
                        <span class="pink-hr mt-3"></span>
                        <p class="text-white mt-4">Let’s solve all your HR problems</p>
                        <div>
                            <a href="#" class="text-decoration-none btn btn_explore px-5">Explore</a>
                            <a href="#" class="text-decoration-none btn btn_seemore px-5">See More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <!-- header start from here -->
                <header class="header">
                    <nav class="navbar navbar-expand-lg">
                        <div class="container-fluid">
                            <a class="navbar-brand" href="#">Cleon<span>HR</span></a>
                            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul class="navbar-nav me-auto ms-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link active" aria-current="page" href="#">Home</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Pricing</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Case Studies</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">More</a>
                                </li>
                            </ul>
                            <div>
                             @auth
                             @if(Auth::user()->role == 'user')
                             <a class="btn btn_started" href="{{route('candidate.recruitment')}}">
                              @else
                              <a class="btn btn_started" href="{{route('company.dashboard')}}">
                                  @endif
                              Get Me Back In</a>
                              @else
                              <a href="{{route('company.login')}}" class="btn btn_started" id="getStartedBtn">Login
                              </a>
                              @endauth
                          </div>
                      </div>
                  </div>
              </nav>
          </header>
          <!-- header end here -->
          <div class="position-relative mt-5 me-lg-5">
            <ul class="column-line ps-0 navbar-nav lh-lg">
                <li class="position-relative">
                    <span class="stepslist hide_password text-white cbn">
                        <span class="one">1</span>
                        <img src="{{asset('assets/images/white-chck.png')}}" class="img-fluid imgdisplay">
                    </span>
                    <div class="ps-5 clickbtn hide_password">
                        <h6 class="mb-1">Select one or more module(s)<img style="float:right" class="collapse_icon" src="{{asset('assets/images/collapse.png')}}"></h6>
                        <p class="fs-12">
                            Select your preferred product to access amazing features
                        </p>
                    </div>
                    <div class="ps-5 collaspE  mb-4">
                        <div class="row"><div class="col-md-12"><input type="checkbox" class="select_module" style="height: 14px;"><label style="font-size: 17px;margin-left: 5px;">Select All</label></div></div>
                        <div class="swiper mySwiper">
                            <div class="swiper-wrapper">
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Recruitment">
                                        <img src="{{asset('assets/images/step-2.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Recruitment</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Internal">
                                        <img src="{{asset('assets/images/step-2.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Internal Control</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Hr">
                                        <img src="{{asset('assets/images/step-2.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">HR Administration</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Cleon_Time">
                                        <img src="{{asset('assets/images/step-2.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Cleon Time</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Payroll">
                                        <img src="{{asset('assets/images/step-2.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Payroll</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Advisory">
                                        <img src="{{asset('assets/images/step-2.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">HR Advisory</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="leave">
                                        <img src="{{asset('assets/images/step-3.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Leave Management</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="learning">
                                        <img src="{{asset('assets/images/step-3.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">E-Learning</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Health">
                                        <img src="{{asset('assets/images/step-3.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Health Managment</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="assessment">
                                        <img src="{{asset('assets/images/step-3.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Assessment</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="asset">
                                        <img src="{{asset('assets/images/step-3.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Cleon Asset</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Performance">
                                        <img src="{{asset('assets/images/step-3.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Performance</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                                <div class="swiper-slide">
                                    <div class="position-relative sizeimg rounded-4" data-id="Verification">
                                        <img src="{{asset('assets/images/step-2.png')}}" class="img-fluid w-100 rounded-4">
                                        <div class="titleofjob">
                                            <h6 class="mb-0 me-2 fs-14 text-white">Verification</h6>
                                            <img src="{{asset('assets/images/quest-cir-black.png')}}" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-button-next swiper-arrow-cs">
                            <img src="{{asset('assets/images/next.png')}}" class="img-fluid">
                        </div>
                        <div class="swiper-button-prev swiper-arrow-cs-1">
                            <img src="{{asset('assets/images/prev.png')}}" class="img-fluid">
                        </div>
                    </div>
                </li>
                <li class="position-relative">
                    <span class="stepslist hide_password text-white cbn1">
                        <span class="two">2</span>
                        <img src="{{asset('assets/images/white-chck.png')}}" class="img-fluid imgdisplay1">
                    </span>
                    <div class="ps-5 clickbtn1 hide_password">
                        <h6 class="mb-1">Enter company information<img style="float:right" class="collapse_icon" src="{{asset('assets/images/collapse.png')}}"></h6>
                        <p class="fs-12">
                            All company information must be valid
                        </p>
                    </div>
                    @php
                    $data = data();
                    $data = $data['data']
                    @endphp
                    <div class="ps-5 collaspE1 mb-4">
                        <form class="row g-3" id="register_form">
                            @csrf
                            <div class="col-lg-12 fields">
                                <div>
                                    <input type="text" class="form-control fs-12 p-12"
                                    placeholder="Company Name" required name="c_name">
                                </div>
                            </div>

                            <div class="col-lg-6 fields">
                                <div>
                                    <select class="form-select fs-12 p-12 select_fields" required name="industry">
                                        <option selected>Select Industry</option>
                                        @foreach($data['industries'] as $industry)
                                        <option value="{{$industry['id']}}">{{$industry['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div>
                                    <select class="form-select fs-12 p-12 select_fields" name="e_size" required>
                                        <option selected>Employee Size</option>
                                        @foreach($data['employee_sizes'] as $employee_sizes)
                                        <option value="{{$employee_sizes['id']}}">{{$employee_sizes['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div>
                                    <select class="form-select fs-12 p-12 select_fields" required name="country">
                                        @foreach($data['countries'] as $county)
                                        <option value="{{$county['id']}}" @if($county['id'] == 158) selected @endif>{{$county['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div>
                                    <input id="phone"  oninput="this.value = this.value.replace(/[^0-9]/g, '')"  name="number" type="tel" id="inlineFormInputGroup"
                                    class="form-control fs-12 p-12" style="padding-left: 100px !important;" required>
                                    <input id="phonecode" name="phone"
                                    id="inlineFormInputGroup" class="form-control" type="hidden" required>
                                    <!-- <input  type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')"  class="form-control fs-12 p-12" placeholder="Phone number" name="number" required> -->
                                </div>
                            </div>
                            <div class="col-lg-12 fields">
                                <div>
                                    <input type="email" class="form-control fs-12 p-12"
                                    placeholder="Official E-mail Address (This email will have unrestricted access)"
                                    name="email" required>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div>
                                    <input type="text" class="form-control fs-12 p-12"
                                    placeholder="Contact Person Full Name" name="name" required>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div class="input-group custom-file-button">
                                    <label class="input-group-text" for="inputGroupFile">Upload Logo</label>
                                    <input type="file" class="form-control fs-12 p-12"
                                    placeholder="Company Image" name="company_image" value="logo" accept="image/*" required id="inputGroupFile">
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div>
                                    <input id="phone2" oninput="this.value = this.value.replace(/[^0-9]/g, '')" name="number2" type="tel" id="inlineFormInputGroup2"
                                    class="form-control fs-12 p-12" style="padding-left: 100px !important;" required>
                                    <input id="phonecode2" name="phone2"
                                    id="inlineFormInputGroup2" class="form-control" type="hidden" required>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div>
                                    <input type="email" class="form-control fs-12 p-12"
                                    placeholder="Email Adress" name="company_email" required>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div>
                                    <input type="text" class="form-control fs-12 p-12"
                                    placeholder="Company Website" name="website" required>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div>
                                    <select class="form-select fs-12 p-12 select_fields" name="revenue">
                                        <option selected>Projected Revenue</option>
                                        @foreach($data['revenue'] as $revenue)
                                        <option value="{{$revenue['id']}}">{{$revenue['name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12 fields">
                                <div>
                                    <input type="text" class="form-control fs-12 p-12"
                                    placeholder="Company Address" name="address" required>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div style="position:relative">
                                    <input type="password" class="password form-control fs-12 p-12"
                                    placeholder="Password" id="passwordInput" name="password" required>
                                    <span style=" position: absolute;top: 4px;right: 10px;" onclick="showpass()"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            <div class="col-lg-6 fields">
                                <div style="position:relative">
                                    <input type="password" class="password_confirmation form-control fs-12 p-12"
                                    placeholder="Confirm password" id="confirmPasswordInput" name="password_confirmation" required>
                                    <span style="position: absolute;top: 4px;right: 10px;" onclick="showpassc()"><i class="fa fa-eye" aria-hidden="true"></i></span>
                                </div>
                            </div>
                            <div class="col-lg-12 d-flex">
                                <button type="button" class="text-decoration-none btncustrounded rounded-5 fs-12 text-white bg-pink submit_button" style="border:none;">Submit</button>                 
                                <div class="loader d-none" style="margin-top:5px;margin-left: 18px;"></div>
                            </div>
                        </form>
                    </div>
                </li>
                <li class="position-relative">
                    <span class="stepslist hide_password text-white cbn2">
                        <span class="three">3</span>
                        <img src="{{asset('assets/images/white-chck.png')}}" class="img-fluid imgdisplay2">
                    </span>
                    <div class="ps-5 clickbtn2 hide_password">
                        <h6 class="mb-1">Verify your account OTP <img style="float:right" class="collapse_icon" src="{{asset('assets/images/collapse.png')}}"></h6>
                        <p class="fs-12">
                            Make sure you enter a valid email address for verification
                        </p>
                    </div>
                    <div class="ps-5 collaspE2 mb-4">
                        <form class="d-flex align-items-center code_width">
                            <div>
                                <input type="text" maxlength="1" class="form-control inputheightwidt input1" disabled name="input1">
                            </div>
                            <div class="ms-3">
                                <input type="text" class="form-control inputheightwidt input2" disabled maxlength="1" name="input2">
                            </div>
                            <div class="ms-3">
                                <input type="text" class="form-control inputheightwidt input3" disabled maxlength="1" name="input3">
                            </div>
                            <div class="ms-3">
                                <input type="text" class="form-control inputheightwidt input4" disabled maxlength="1" name="input4">
                            </div>
                            <div class="ms-3">
                                <input type="text" class="form-control inputheightwidt input5" disabled maxlength="1" name="input5">
                            </div>
                            <div class="ms-3">
                                <input type="text" class="form-control inputheightwidt last_input input6" disabled maxlength="1" name="input6">
                            </div>
                            <div class="ms-3">
                             <button style="background:none;border:none" class="resend_button" type="button"> <i class="fa fa-repeat" style="color:#E82583" aria-hidden="true" disabled></i></button>
                         </div>
                         <div class="loader loder_code d-none" style="margin-top:5px;margin-left: 18px;"></div>
                     </form>

                 </div>
             </li>
             <li class="position-relative">
                <span class="stepslist hide_password text-white cbn3">
                    <span class="four">4</span>
                    <img src="{{asset('assets/images/white-chck.png')}}" class="img-fluid imgdisplay3">
                </span>
                <div class="ps-5 clickbtn3 hide_password">
                    <h6 class="mb-1">Enter payment details <img style="float:right" class="collapse_icon" src="{{asset('assets/images/collapse.png')}}"></h6>
                    <p class="fs-12">
                        Enter payment details for paystack for your payment
                    </p>
                </div>
                <div class="ps-5 collaspE3 mb-4">
                    <div class="d-flex align-items-center g-3">

                        <a  
                        class="text-decoration-none btncustrounded rounded-5 fs-12 ms-3 text-white bg-green-light pay">
                        Stripe
                    </a>
                    <a  
                    class="text-decoration-none btncustrounded rounded-5 fs-12 ms-3 text-white bg-green-light pay">
                    Paystack
                </a>
                <a  
                class="text-decoration-none btncustrounded rounded-5 fs-12 ms-3 text-white bg-green-light pay">
                Flutterwave
            </a>
            <a  
            class="text-decoration-none btncustrounded rounded-5 fs-12 ms-3 text-white bg-green-light pay">
            Paypal
        </a>
        <a href="#" class="btn btncustrounded skip_section rounded-5 fs-12 ms-3 text-white"
        style="background-color: var(--pink-color);">
        Skip
    </a>
</div>
<div id="preauthorizationResponse"></div>

</div>
</li>
<li class="position-relative">
    <span class="stepslist hide_password text-white cbn4">
        <span class="five">5</span>
        <img src="{{asset('assets/images/white-chck.png')}}" class="img-fluid imgdisplay4">
    </span>
    <div class="ps-5 clickbtn4 hide_password">
        <h6 class="mb-1">Start your 7 days trial <img style="float:right" class="collapse_icon" src="{{asset('assets/images/collapse.png')}}"></h6>
        <p class="fs-12 mb-0">
            Enjoy your seven days trial on this application
        </p>
    </div>
    <div class="ps-5 collaspE4 mb-4">
        <div class="form-check my-3">
            <input class="form-check-input mt-2" type="checkbox" value="" id="flexCheckDefault">
            <label class="form-check-label fs-12" for="flexCheckDefault">
                By clicking get started, you agree to our
                <a class="text-decoration-none">
                    terms and conditions
                </a>
            </label>
        </div>
        <span class="text-danger d-none">This field is required</span>
        <div class="d-flex align-items-center g-3">
            <button class="text-decoration-none btncustrounded rounded-5 fs-12 text-white bg-pink get_started"  style="border: none;">
                Get Started
            </button>
            <div class="loader final_loader d-none" style="margin-top:5px;margin-left: 18px;"></div>
        </div>
    </div>
</li>
</ul>
</div>  
</div>
</div>
</section>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Enter Card Details</h5>
        </div>
        <div class="modal-body">
            <div><span>Card will only be saved and will not be charged until after the 7-day trial period elapses.</span></div>
            <div class="flip-container" id="flip-toggle">
                <div class="flipper">
                    <div class="card front">
                        <div class="card-provider">
                            <div class="visa c-image">
                            </div>
                            <div class="mastercard c-image">
                            </div>
                        </div>
                        <div class="card-number-wrap">
                            <label class="input-title" for="c-number">
                                Card Number*
                            </label>
                            <input type="text" name="c-number" id="c-number"
                            class="input-field card-number faded-placeholder"
                            placeholder="Enter Card Number"
                            maxlength="16" required>
                        </div>
                        <div class="cardholder-name-wrap">
                            <label for="c-name" class="input-title cardholder-name faded-placeholder">
                                Name on card*
                            </label>
                            <input type="text" name="c-number" id="c-name"
                            class="input-field cardholder-name namecard faded-placeholder"
                            placeholder="Enter Name" required>
                        </div>
                        <div class="card-expiry-wrap">
                            <label class="input-title">Expiry Date*</label>
                            <div class="custom-dropdown">
                                <select name="month" id="month" class="input-field options faded-placeholder">
                                    <option value="">Month</option>
                                    <option value="01">Jan</option>
                                    <option value="02">Feb</option>
                                    <option value="03">Mar</option>
                                    <option value="04">Apr</option>
                                    <option value="05">May</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Jul</option>
                                    <option value="08">Aug</option>
                                    <option value="09">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dec</option>
                                </select>
                            </div>
                            <div class="custom-dropdown">
                                <input type="text" name="year" id="year"
                                class="input-field cardholder-name faded-placeholder"
                                placeholder="2025" required>
                            </div>
                        </div>
                    </div>
                    <div class="card back">
                        <div class="black-stripe"></div>
                        <div class="cvv-wrap">
                            <label for="cvv-code" class="input-title">CVV Code*</label>
                            <input type="text" class="input-field cvv-code" id="cvv-code" placeholder="000" maxlength="3" required>
                        </div>
                        <div class="disclaimer">
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="buttons">
                <button class="prev" disabled="disabled">Prev</button>
                <button class="next">Next</button>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary close" data-dismiss="modal">Close</button>
            <button id="card" type="button" class="btn btn-primary">Save</button>
        </div>
    </div>
</div>
</div>

<!-- js area -->
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.full.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/intlTelInput.min.js"></script>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            var countryCode1 = $('.selected-dial-code:first').text();
            var countryCode2 = $('.selected-dial-code:last').text();
            $("#phonecode").val(countryCode1);
            $("#phonecode2").val(countryCode2);
        }, 2000);

        // Initialize telephone input for phone
        initializeTelInput("#phone", "#error-msg", "#valid-msg", "#phonecode");

        // Initialize telephone input for phone2
        initializeTelInput("#phone2", "#error-msg2", "#valid-msg2", "#phonecode2");
    });

    function initializeTelInput(telInputSelector, errorMsgSelector, validMsgSelector, phoneCodeInputSelector) {
        var telInput = $(telInputSelector),
        errorMsg = $(errorMsgSelector),
        validMsg = $(validMsgSelector);

        telInput.intlTelInput({
            allowExtensions: true,
            formatOnDisplay: true,
            autoFormat: true,
            autoHideDialCode: true,
            autoPlaceholder: true,
            defaultCountry: "auto",
            ipinfoToken: "yolo",
            nationalMode: false,
            numberType: "MOBILE",
            preventInvalidNumbers: true,
            separateDialCode: true,
            initialCountry: "auto",
            geoIpLookup: function (callback) {
                $.get("https://ipinfo.io?token=88d8e99df13643", function () {
                }, "jsonp").always(function (resp) {
                    var countryCode = (resp && resp.country) ? resp.country : "";
                    callback(countryCode);
                });
            },
            utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/11.0.9/js/utils.js"
        });

        var reset = function () {
            telInput.removeClass("error");
            errorMsg.addClass("hide");
            validMsg.addClass("hide");
        };

        // on blur: validate
        telInput.blur(function () {
            reset();
            if ($.trim(telInput.val())) {
                if (telInput.intlTelInput("isValidNumber")) {
                    validMsg.removeClass("hide");
                } else {
                    telInput.addClass("error");
                    errorMsg.removeClass("hide");
                }
            }
        });

        // on keyup / change flag: reset
        telInput.keyup(function () {
            formatNumber(telInput, phoneCodeInputSelector);
        });
    }

    function formatNumber(telInput, phoneCodeInputSelector) {
        var number = telInput.val();
        var classf = $(".selected-flag > div").attr("class");
        var flag = classf.slice(-2);
        var formattedNumber = intlTelInputUtils.formatNumber(number, flag, intlTelInputUtils.numberFormat.INTERNATIONAL);
        telInput.val(formattedNumber.slice(formattedNumber.indexOf(' ') + 1, formattedNumber.length));
        $(phoneCodeInputSelector).val(flag); // Set the phone code value
    }
</script>


<script type="text/javascript">
    $(document).ready(function() {
        $(".select_fields").select2({
            width: '100%',
            placeholder: "Select  ",
            allowClear: true
        });
    });
    function showpass() {
        const passinput = document.getElementById('passwordInput');

        if (passinput.type === 'password') {
            passinput.type = 'text'
        } else {
            passinput.type = 'password'
        }
    }
    function showpassc() {
        const passinput = document.getElementById('confirmPasswordInput');

        if (passinput.type === 'password') {
            passinput.type = 'text'
        } else {
            passinput.type = 'password'
        }
    }
    $('.hide_password').on('click',function(){
        $('#confirmPasswordInput , #passwordInput').attr('type','password');
    });
    $(document).ready(function () {
        $('.clickbtn').on('click', function () {
            update_icon($(this));
            $(".collaspE").show();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.one').show();
            $('.two').show();
            $('.three').show();
            $('.four').show();
            $('.five').show();
            $('.imgdisplay').hide();
            $('.imgdisplay1').hide();
            $('.imgdisplay2').hide();
            $('.imgdisplay3').hide();
            $('.imgdisplay4').hide();

        })
        $('.clickbtn1').on('click', function () {
            update_icon($(this));
            $(".collaspE").hide();
            $(".collaspE1").show();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.imgdisplay').show();
            $('.one').hide();
            $('.imgdisplay2').hide();
            $('.three').show();

        })
        $('.clickbtn2').on('click', function () {
            update_icon($(this));
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").show();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.imgdisplay').show();
            $('.imgdisplay1').show();
            $('.two').hide();
            $('.one').hide();
        })
        $('.clickbtn3').on('click', function () {
            update_icon($(this));
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").show();
            $(".collaspE4").hide();
            $('.imgdisplay1').show();
            $('.imgdisplay2').show();
            $('.two').hide();
            $('.three').hide();
            $('.four').show();
            $('.imgdisplay3').hide();
        })
        $('.clickbtn4').on('click', function () {
            update_icon($(this));
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").show();
            $('.imgdisplay').show();
            $('.imgdisplay1').show();
            $('.imgdisplay2').show();
            $('.imgdisplay3').show();
            $('.one').hide();
            $('.two').hide();
            $('.three').hide();
            $('.four').hide();
        })

        $('.cbn').on('click', function () {
            update_icon($(this));
            $(".collaspE").show();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.one').show();
            $('.two').show();
            $('.three').show();
            $('.four').show();
            $('.five').show();
            $('.imgdisplay').hide();
            $('.imgdisplay1').hide();
            $('.imgdisplay2').hide();
            $('.imgdisplay3').hide();
            $('.imgdisplay4').hide();

        })
        $('.cbn1').on('click', function () {
            update_icon($(this));
            $(".collaspE").hide();
            $(".collaspE1").show();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.imgdisplay').show();
            $('.one').hide();
            $('.imgdisplay2').hide();
            $('.three').show();

        })
        $('.cbn2').on('click', function () {
            update_icon($(this));
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").show();
            $(".collaspE3").hide();
            $(".collaspE4").hide();
            $('.imgdisplay').show();
            $('.imgdisplay1').show();
            $('.two').hide();
            $('.one').hide();
        })
        $('.cbn3').on('click', function () {
            update_icon($(this));
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").show();
            $(".collaspE4").hide();
            $('.imgdisplay1').show();
            $('.imgdisplay2').show();
            $('.two').hide();
            $('.three').hide();
            $('.four').show();
            $('.imgdisplay3').hide();
        })
        $('.cbn4').on('click', function () {
            update_icon($(this));
            $(".collaspE").hide();
            $(".collaspE1").hide();
            $(".collaspE2").hide();
            $(".collaspE3").hide();
            $(".collaspE4").show();
            $('.imgdisplay').show();
            $('.imgdisplay1').show();
            $('.imgdisplay2').show();
            $('.imgdisplay3').show();
            $('.one').hide();
            $('.two').hide();
            $('.three').hide();
            $('.four').hide();
        })
        function update_icon(element) {
            $('.collapse_icon').each(function() {
                $(this).attr('src', '{{asset('assets/images/collapse.png')}}');
            });
            element.parent().find('.collapse_icon').attr('src', '{{asset('assets/images/collapsedown.png')}}');
        }

    })
</script>

<script>
    var swiper = new Swiper(".mySwiper", {
        slidesPerView: 1,
        spaceBetween: 10,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        breakpoints: {
            640: {
                slidesPerView: 2,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 20,
            },
        },
    });
</script>
<script>

</script>

<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>
<script>
    $('#getStartedBtn').on('click', function() {
        $('#dropdownContent').toggle();
    });
</script>

@if (session('toast_error'))
<script>
    var errors = {!! json_encode(session('toast_error')) !!};
    for (var i = 0; i < errors.length; i++) {
        toastr.error(errors[i], 'Validation Error');
    }
</script>
@endif
<script>
    @if (Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    @endif


    @if (Session::has('info'))
    toastr.info("{{ Session::get('info') }}");
    @endif


    @if (Session::has('warning'))
    toastr.warning("{{ Session::get('warning') }}");
    @endif
    @if (Session::has('message'))
    toastr.success("{{ Session::get('success') }}");
    @endif

    @if (Session::has('error'))
    toastr.error("{{ Session::get('error') }}");
    @endif
</script>
<script>
    $(document).on('click','.sizeimg' , function(){
        $(this).toggleClass('border-pink');
    })
    $('.next').on('click', function() {

        $('#flip-toggle').addClass('hover');
        $(this).attr('disabled', true);
        $('.prev').removeAttr('disabled');

    });


    /* prev view button */
    $('.prev').on('click', function() {

        $('#flip-toggle').removeClass('hover');
        $(this).attr('disabled', true);
        $('.next').removeAttr('disabled');

    });

    $('#card').on('click',function(){
        $('#myModal').modal('hide');
        $('.collaspE3').css('display','none');
        $('.collapse_icon').each(function() {
            $(this).attr('src', '{{asset('assets/images/collapse.png')}}');
        });
        $('.collaspE4').parent().find('.collapse_icon').attr('src', '{{asset('assets/images/collapsedown.png')}}');
        $('.collaspE4').css('display','block');
    })
    $('.select_module').on('click',function(){
        if($(this).prop('checked')){
            $('.sizeimg').addClass('border-pink');
        }else{
            $('.sizeimg').removeClass('border-pink');
        }
    })
    $('.pay').on('click', function() {
        $('#myModal').modal('show');
    })
    $(document).ready(function() {
        $(".submit_button").click(function(e) {
            e.preventDefault(); 
            if (validateForm()) {
                $(this).prop('disabled',true);
                $('.loader').removeClass('d-none');
                var modules = []; 
                $('.border-pink').each(function () {
                    modules.push($(this).attr('data-id'));
                });
                if (modules.length === 0) {
                    toastr.error('Please Select Module');
                    $(this).prop('disabled',false);
                    $('.loader').addClass('d-none');
                    return;
                }
                var formData = new FormData($("#register_form")[0]);
                formData.append('image', $('#inputGroupFile')[0].files[0]);
                formData.append('modules', modules);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST", 
                    url: "{{route('create.company')}}", 
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if(response.error){
                            toastr.error(response.error);
                            $('.submit_button').prop('disabled',false);
                            $('.loader').addClass('d-none');
                        }else{
                         $("#register_form :input, #register_form select").prop("disabled", true);
                         toastr.success('Otp Send Successfully on your E-mail.');
                         $('.loader').addClass('d-none');
                         $('.inputheightwidt').prop('disabled',false).val('');
                         $('#confirmPasswordInput , #passwordInput').attr('type','password');
                         $('.collaspE1').css('display','none');
                         $('.collapse_icon').each(function() {
                            $(this).attr('src', '{{asset('assets/images/collapse.png')}}');
                        });
                         $('.collaspE2').parent().find('.collapse_icon').attr('src', '{{asset('assets/images/collapsedown.png')}}');
                         $('.collaspE2').css('display','block');
                         $('.resend_button').prop('disabled',false);
                     }
                 },
                 error: function(xhr, status, error) {
                    toastr.error('An error occurred');
                    $('.submit_button').prop('disabled',false);
                    $('.loader').addClass('d-none');
                }
            });
            }
        });
    });

    function validateForm() {
        $(".error-message").remove();
        var valid = true;

        var companyName = $("input[name='c_name']").val();
        var industry = $("select[name='industry']").val();
        var employee_size = $("select[name='e_size']").val();
        var country = $("select[name='country']").val();
        var number = $("input[name='number']").val();
        var number2 = $("input[name='number2']").val();
        var email = $("input[name='email']").val();
        var company_email = $("input[name='company_email']").val();
        var name = $("input[name='name']").val();
        var website = $("input[name='website']").val();
        var revenue = $("select[name='revenue']").val();
        var address = $("input[name='address']").val();
        var password = $("input[name='password']").val();
        var password_confirmation = $("input[name='password_confirmation']").val();
        var logoInput = $("input[name='company_image']")[0];
        var logo = logoInput.files.length > 0 ? logoInput.files[0].name.trim() : "";

        if (logo === "") {
            valid = false;
            var errorMessage =$("<span class='error-message' style='color:red;font-size:12px;'>Logo Image is required</span>");
            $("input[name='company_image']").closest('.col-lg-6').append(errorMessage);
        }
        if (companyName.trim() === "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Company Name is required</span>").insertAfter("input[name='c_name']");
        }
        if (industry === "Select Industry") {
            valid = false;
            var errorMessage = $("<span class='error-message' style='color:red;font-size:12px;'>Industry is required</span>");
            $("select[name='industry']").closest('div').append(errorMessage);
        }
        if (employee_size === "Employee Size") {
            valid = false;
            var errorMessage = $("<span class='error-message' style='color:red;font-size:12px;'>Employee Size is required</span>");
            $("select[name='e_size']").closest('div').append(errorMessage);
        }
        if (country === "Country") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Country is required</span>").insertAfter("select[name='country']");
        }
        if (number.trim() === "") {
            valid = false;
            var errorMessage =  $("<span class='error-message' style='color:red;font-size:12px;'>Number is required</span>");
            $("input[name='number']").closest('.intl-tel-input').parent('div').append(errorMessage);
        }
        if (number2.trim() === "") {
            valid = false;
            var errorMessage =  $("<span class='error-message' style='color:red;font-size:12px;'>Number is required</span>");
            $("input[name='number2']").closest('.intl-tel-input').parent('div').append(errorMessage);
        }
        if (email.trim() === "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Email is required</span>").insertAfter("input[name='email']");
        } else if (!isValidEmail(email)) {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Please enter a valid email address</span>").insertAfter("input[name='email']");
        }
        if (company_email.trim() === "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Email is required</span>").insertAfter("input[name='company_email']");
        } else if (!isValidEmail(company_email)) {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Please enter a valid email address</span>").insertAfter("input[name='company_email']");
        }
        if (name.trim() === "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Name is required</span>").insertAfter("input[name='name']");
        }
        if (website.trim() === "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Website is required</span>").insertAfter("input[name='website']");
        }
        if (revenue === "Projected Revenue") {
            valid = false;
            var errorMessage = $("<span class='error-message' style='color:red;font-size:12px;'>Projected Revenue is required</span>");
            $("select[name='revenue']").closest('div').append(errorMessage);
        }
        if (address.trim() === "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Address is required</span>").insertAfter("input[name='address']");
        }
        var uppercaseRegex = /[A-Z]/;
        var lowercaseRegex = /[a-z]/;
        var specialCharRegex = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/;
        if (password.trim() === "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Password is required</span>").insertAfter("input[name='password']");
        } else if (password.length < 8 || !uppercaseRegex.test(password) || !lowercaseRegex.test(password) || !specialCharRegex.test(password)) {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Password must contain at least 8 characters, one uppercase letter, one lowercase letter, and one special character</span>").insertAfter("input[name='password']");
        }

        if (password_confirmation.trim() === "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Confirm Password is required</span>").insertAfter("input[name='password_confirmation']");
        }
        if (password !== password_confirmation && password_confirmation.trim() !== "") {
            valid = false;
            $("<span class='error-message' style='color:red;font-size:12px;'>Password and Confirm Password must match</span>").insertAfter("input[name='password_confirmation']");
        }

        return valid;
    }
    function isValidEmail(email) {
    // This regular expression checks for a basic email format, but you can adjust it as needed.
        var emailRegex = /^[A-Za-z0-9._%-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}$/;
        return emailRegex.test(email);
    }
</script>
<script>
    $('.form-control').on('input',function(){
        $(this).closest('.fields').find('.error-message').remove();
    })
    $('.form-select').on('change',function(){
        $(this).closest('.fields').find('.error-message').remove();
    })
</script>
<script>
    function restrictToSingleDigit(input) {
        if (input.value.length > 1) {
        input.value = input.value.slice(0, 1); // Only keep the first character
    }
}
document.addEventListener("DOMContentLoaded", function () {
    const otpFields = document.querySelectorAll(".inputheightwidt");

    otpFields.forEach(function (field, index) {
        field.addEventListener("input", function () {
            // Remove any non-numeric characters
            this.value = this.value.replace(/\D/g, "");

            if (this.value !== "") {
                if (index < otpFields.length - 1) {
                    otpFields[index + 1].focus();
                }
            }
        });

        field.addEventListener("keydown", function (e) {
            if (e.key === "Backspace" && this.value === "") {
                if (index > 0) {
                    otpFields[index - 1].focus();
                }
            }
        });
    });
});
$('.last_input').on('input',function() {
    var val = $(this).val();
    if(val !== ""){
        $('.loder_code').removeClass('d-none');
        var email = $('input[name="email"]').val();
        var field1 = $(".input1").val();
        var field2 = $(".input2").val();
        var field3 = $(".input3").val();
        var field4 = $(".input4").val();
        var field5 = $(".input5").val();
        var field6 = $(".input6").val();
        var otpValue = field1 + field2 + field3 + field4 + field5 + field6;
        $.ajax({
            url : "{{route('company.verify.email')}}",
            method : "GET",
            data : {
                code :otpValue,
                email : email,
            },
            success : function(response){
                if(response.success){
                    toastr.success('E-mail Verified Successfully');
                    $('.resend_button').prop('disabled',false);
                    $('.collaspE2').css('display','none');
                    $('.collapse_icon').each(function() {
                        $(this).attr('src', '{{asset('assets/images/collapse.png')}}');
                    });
                    $('.collaspE3').parent().find('.collapse_icon').attr('src', '{{asset('assets/images/collapsedown.png')}}');
                    $('.collaspE3').css('display','block');
                    $('.loder_code').removeClass('d-none');
                    $('.loder_code').addClass('d-none');
                    $('.inputheightwidt').prop('disabled',true);
                }else{
                    toastr.error(response.error);
                    $('.loder_code').addClass('d-none');

                }
            }

        })
    }
})
$('.resend_button').on('click',function(){
    $(this).prop('disabled',true);
    var email = $('input[name="email"]').val();
    $.ajax({
        url : "{{route('company.resend.email')}}",
        method : "GET",
        data : {
            email : email,
        },
        success : function(response){
            if(response.success){
                toastr.success('E-mail Resend Successfully');
                $('.resend_button').prop('disabled',false);
            }else{
                toastr.error(response.error);
                $('.resend_button').prop('disabled',false);
            }
        }

    })
})

$('.skip_section').on('click',function(){
    $('.collaspE3').css('display','none');
    $('.collapse_icon').each(function() {
        $(this).attr('src', '{{asset('assets/images/collapse.png')}}');
    });
    $('.collaspE4').parent().find('.collapse_icon').attr('src', '{{asset('assets/images/collapsedown.png')}}');
    $('.collaspE4').css('display','block');
})

$('.get_started').on('click' , function(){
    var current = $(this);
    $(this).prop('disabled',true);
    var email = $('input[name="email"]').val();
    var checked =  $('#flexCheckDefault');
    if (!checked.prop('checked')) {
        toastr.error('Please Select Checkbox');
        $(this).prop('disabled',false);
        return;
    }
    $('.final-loader').removeClass('d-none');
    $.ajax({
        url : "{{route('get_started')}}",
        method : "GET",
        data : {
            email : email,
        },
        success : function (response) {
           if (response.success) {
            $('.final-loader').addClass('d-none');
            toastr.success(response.success);
            window.location.href = "{{ route('company.dashboard') }}";
        } else if(response.exist){
            current.prop('disabled',false);
            $('.final-loader').addClass('d-none');
            toastr.error(response.exist);
            $('.collaspE4').css('display','none');
            $('.collapse_icon').each(function() {
                $(this).attr('src', '{{asset('assets/images/collapse.png')}}');
            });
            $('.collaspE1').parent().find('.collapse_icon').attr('src', '{{asset('assets/images/collapsedown.png')}}');
            $('.collaspE1').css('display','block');
            $("#register_form :input, #register_form select").prop("disabled", false);
        }else{
            current.prop('disabled',false);
            toastr.error(response.error);
            $('.final-loader').addClass('d-none');
        }
        $(this).prop('disabled',false);
    }
})
})


</script>
</body>

</html>