<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">
    <title>Reset Password</title>
    <link rel="stylesheet" type="text/css" href="{{asset('cleon_styling/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('cleon_styling/css/style.css')}}">
    <script src="https://kit.fontawesome.com/00b231478f.js" crossorigin="anonymous"></script>
</head>
<style>
    .margin_bottom{
        margin-bottom: 25px;
    }
</style>
<body>

    <section class="signup_section">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">
                <div class="signup_section_leftside">
                    <div>
                        <h1 class="hero_section_heading mb-3">Welcome back, Let’s get you into your account</h1>
                        <span class="pink-hr"></span>
                        <p class="text-white mt-4">Let’s solve all your HR problems</p>
                        <div class="mt-4">
                            <a href="#" class="text-decoration-none me-2 btn bg-pink py-2 text-white px-5">Explore</a>
                            <a href="#" class="text-decoration-none btn btn_seemore py-2 px-5">See More</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-6">

                <div class="position-relative p-4 mt-5 me-lg-5">
                    <div class="my-3">
                        <img src="{{asset('cleon_styling/image/logo.png')}}" class="img-fluid logo-size-log">
                    </div>
                    <div class="ps-4 mt-4">
                        <h4 class="fw-bold text-uppercase">Reset Password</h4>
                        <form method="POST" action="{{ route('company.password.update') }}">
                            @csrf
                            <div class="col-lg-8" >
                                <div class="mt-3">
                                    <div class="input-group border rounded-2 margin_bottom p-2 " style="position:relative;">
                                        <input type="email" class="form-control border-0 @error('email') is-invalid @enderror" name="email" value="{{ $email }}" readonly required
                                        placeholder="user.doe@officialemail.com"
                                        aria-label="user.doe@officialemail.com" aria-describedby="basic-addon2">
                                        <span class="input-group-text bg-transparent border-0" id="basic-addon2">
                                            <img src="{{asset('cleon_styling/image/at.png')}}" class="img-fluid">
                                        </span>
                                        @error('email')
                                        <span class="invalid-feedback" style="font-size:0.7rem;position: absolute;top: 50px;" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div> 
                            </div>
                            <div class="col-lg-8" >
                                <div class="mt-3">
                                    <div class="input-group border rounded-2 margin_bottom p-2">
                                        <input id="otp" type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" class="form-control border-0 passinput12" name="otp" required placeholder="Please Enter Otp"
                                        aria-label="otp" aria-describedby="basic-addon3" id="otp">
                                        <span class="input-group-text bg-transparent border-0" id="basic-addon3"
                                        >
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8" >
                            <div class="mt-3">
                                <div class="input-group border rounded-2 margin_bottom p-2" style="font-size:0.7rem;position:relative;">
                                    <input type="password" class="form-control passinput12 border-0 @error('password') is-invalid @enderror" name="password" required placeholder="New Password"
                                    aria-label="Password" aria-describedby="basic-addon3" id="passinput">
                                    <span class="input-group-text bg-transparent border-0" id="basic-addon3"
                                    >
                                    <img src="{{asset('cleon_styling/image/pass-eye.png')}}" class="img-fluid passinput">
                                </span>
                                @error('password')
                                <span class="invalid-feedback" style="font-size:0.7rem;position: absolute;top: 50px;" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8" >
                        <div class="mt-3">
                            <div class="input-group border rounded-2 margin_bottom p-2" style="font-size:0.7rem;position:relative;">
                                <input type="password" class="form-control passinput12 border-0 @error('password_confirmation') is-invalid @enderror" name="password_confirmation" required placeholder="Confirm New Password"
                                aria-label="Password" aria-describedby="basic-addon3" id="passinput">
                                <span class="input-group-text bg-transparent border-0" id="basic-addon3"
                                >
                                <img src="{{asset('cleon_styling/image/pass-eye.png')}}" class="img-fluid passinput">
                            </span>
                            @error('password')
                            <span class="invalid-feedback" style="font-size:0.7rem;position: absolute;top: 50px;" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 mt-2">
                    <div class="d-flex align-items-center justify-content-between">
                        <div>
                            <button
                            class="border-0 bg-pink rounded-3 btncust text-white d-flex align-items-center justify-content-between w-100" style="text-decoration: none;" type="submit">
                            <p class="mb-0 ps-2">
                                Reset Password
                            </p>
                            <div class="text-end ms-3">
                                <img src="{{asset('cleon_styling/image/submit-arrow.png')}}" class="img-fluid width-20">
                            </div>
                        </button>
                    </div>

                </div>
            </div>
            <div class="col-lg-8">
                <div class="mt-4">
                    <p>
                        Don’t have an account ?
                        <a href="{{route('company.register')}}"
                        class="text-decoration-none text-pink fw-bold">
                        Create an account
                    </a>
                </p>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="d-flex align-items-center justify-content-between">
                <a href="#" disabled class="text-decoration-none d-block">
                    <img src="{{asset('cleon_styling/image/login-google.png')}}" class="img-fluid">
                </a>
                <a href="#" disabled class="text-decoration-none d-block ms-3">
                    <img src="{{asset('cleon_styling/image/login-fb.png')}}" class="img-fluid">
                </a>
            </div>
        </div>
    </form>
</div>
</div>
</div>
</div>
</section>


<!-- js area -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script type="text/javascript" src="{{asset('cleon_styling/js/bootstrap.bundle.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>
<script>
    @if (Session::has('success'))
    toastr.success("{{ Session::get('success') }}");
    @endif


    @if (Session::has('info'))
    toastr.info("{{ Session::get('info') }}");
    @endif


    @if (Session::has('warning'))
    toastr.warning("{{ Session::get('warning') }}");
    @endif
    @if (Session::has('message'))
    toastr.success("{{ Session::get('success') }}");
    @endif

    @if (Session::has('error'))
    toastr.error("{{ Session::get('error') }}");
    @endif
</script>

@if (session('toast_error'))
<script>
    var errors = {!! json_encode(session('toast_error')) !!};
    for (var i = 0; i < errors.length; i++) {
        toastr.error(errors[i], 'Validation Error');
    }
</script>
@endif
<script>
    $('.passinput').on('click', function () {
        var input = $(this).closest('.input-group').find('.passinput12'); // Find the related input field
        if (input.prop('type') === 'password') {
            input.prop('type', 'text');
        } else {
            input.prop('type', 'password');
        }
    });
</script>


</body>

</html>

