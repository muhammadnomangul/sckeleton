@section('pageTitle')
Setup Claims
@endsection
@include('company.layout.header')
<style>
	.claim_edit_button:hover{
		background-color:#E82583;
		color: white;
	}
</style>
<!-- left side section start -->
<div class="mt-5 mb-3">
	<div class="row g-3">
		<div class="col-lg-12">
			<h5 class="mb-0">Create New Claim Type</h5>
			<form action="{{url('company/create/claim')}}" method="post">
				@csrf
				<div class="row g-3 align-items-center border_bottom pb-3">
					<div class="col-12 col-sm-12 col-md-6 col-lg-6">
						<div class="row g-3">
							<div class="col-12">
								<div class="input-group bg-input mt-4 p-1">
									<input type="text" name="title" class="form-control fs-14 bg-transparent border-0"
									placeholder="Claims Title" required>
								</div>
							</div>
						</div>
						<div class="row g-2">
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<div class="input-group bg-input mt-4 p-1">
									<input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" name="value" class="form-control fs-14 bg-transparent border-0"
									placeholder="Claim Cost Value (Naira)" required>
								</div>
							</div>
							<div class="col-12 col-sm-12 col-md-6 col-lg-6">
								<div class="input-group bg-input mt-4 p-1">
									<select  name="schedule" class="form-select fs-14 bg-transparent border-0"
									placeholder="Schedule" required>
									<option value="" selected>Claim Type Schedule</option>
									<option value="hourly">Hourly</option>
									<option value="daily">Daily</option>
									<option value="weekly">Weekly</option>
									<option value="monthly">Monthly</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-6 col-lg-5">
					<div class="input-group bg-input mt-4 p-1">
						<textarea name="desc" id="" rows="4" class="form-control fs-14 bg-transparent border-0"
						placeholder="Description" required></textarea>
					</div>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-1 text-center">
					<div class="row g-3">
						<div class="col-12 d-flex justify-content-center">
							<div class="mt-3 p-1">
								<button
								class="border-0 rounded-2 btn-create text-white px-3 d-block btncustplus">
								<img src="{{asset('styling/image/icons/+.png')}}" alt="" class="img-fluid ">
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>

<!-- cards start -->
<div class="row g-2 mt-5 appending_claim" style="margin-bottom:50px">
	@foreach($claims as $claim)
	<div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 col-xxl-4" style="position:relative;">
		<div class="card setup_cards">
			<div class="row no-gutters">
				<div class="col-4 col-sm-4 col-md-4 col-lg-4">
					<div class="setup_cards_left">
						<span class="bg-pink text-white description_modal" style="cursor: pointer;" data-id="{{$claim->desc}}">?</span>
					</div>
				</div>
				<div class="col-8 col-sm-8 col-md-8 col-lg-8">
					<div class="card-body" style="padding:1rem 0.5rem !important">
						<h5 class="fw-semibold text-capitalize">{{$claim->title}}</h5>
						<p class="fs-14">{{$claim->value}} / {{$claim->schedule}} </p>
					</div>
				</div>
			</div>
		</div>
		<div class="d-flex justify-content-end" style="position: absolute;top: 7px;right: 10px;">
			<button class="btn btn-link" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-expanded="false">
				<i class="fa fa-ellipsis-h" style="color:#E82583" aria-hidden="true"></i>
			</button>
			<ul class="dropdown-menu" style="background-color:white" aria-labelledby="dropdownMenuButton">
				<li class="edit_modal_claim claim_edit_button" data-id="{{$claim->id}}" style="padding: 4px 17px;cursor: pointer;">
					<span>Edit</span>
				</li>
				<li class="delete_claim claim_edit_button" data-id="{{$claim->id}}" style="padding: 4px 17px;cursor: pointer;">
					<span>Delete</span>
				</li>
				<form method="POST" action="{{ url('company/claim/delete/'.$claim->id) }}" class="claimed_claim">
					@csrf
				</form>
			</ul>
		</div>

	</div>
	@endforeach
</div>
@if ($claims->lastPage() > 1)
<div class="d-flex justify-content-end mt-1" style="padding-right:5%">
	<nav aria-label="Page navigation example">
		<ul class="pagination">
			@if ($claims->onFirstPage())
			<li class="page-item disabled">
				<span class="page-link" aria-disabled="true">&laquo;</span>
			</li>
			@else
			<li class="page-item">
				<a class="page-link" href="{{ $claims->previousPageUrl() }}" aria-label="Previous">
					<span aria-hidden="true">&laquo;</span>
				</a>
			</li>
			@endif
			@foreach ($claims->getUrlRange(1, $claims->lastPage()) as $page => $url)
			<li class="page-item {{ $page == $claims->currentPage() ? 'active' : '' }}">
				<a class="page-link" href="{{ $url }}">{{ $page }}</a>
			</li>
			@endforeach
			@if ($claims->hasMorePages())
			<li class="page-item">
				<a class="page-link" href="{{ $claims->nextPageUrl() }}" aria-label="Next">
					<span aria-hidden="true">&raquo;</span>
				</a>
			</li>
			@else
			<li class="page-item disabled">
				<span class="page-link" aria-disabled="true">&raquo;</span>
			</li>
			@endif
		</ul>
	</nav>
</div>
@endif
<div class="modal fade " id="claiming_modal" tabindex="-1" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content edit_claims">
			
		</div>
	</div>
</div>
<!-- cards end -->
</div>
<div class="modal fade " id="description_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Claim Description</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<p class="claim_description"></p>
			</div>
		</div>
	</div>
</div>

@section('js')
<script>
	$(document).on('click','.edit_modal_claim',function(){
		var id = $(this).attr('data-id');
		$.ajax({
			url : '{{url("company/claim/edit")}}' + '/' + id ,
			method : 'GEt',
			data:{

			},
			success:function(response){
				$('.edit_claims').empty().append(response.component);
				$('#claiming_modal').modal('show');
				var val = $('.from_date').val();
				$('.to_date').attr('min',val);
			}
		});
	});

</script>

<script>
	$(document).on('click', '.delete_claim', function(event) {
		event.preventDefault();
		var id = $(this).attr('data-id');
		Swal.fire({
			title: "Delete Record",
			text: "Are you sure, You Want to delete this Record!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes",
		}).then((result) => {
			if (result.isConfirmed) {
				$.ajax({
					url: "{{url('company/claim/delete')}}" + "/" + id,
					type: 'get',
					data: {},
					success: function(response) {
						toastr.success('Record Deleted Successfully');
						$('.appending_claim').empty().append(response.component);
					}
				});
			}
		});
	});
	$(document).on('click', '.delete_claim', function(event) {
		event.preventDefault();
		var id = $(this).attr('data-id');
		Swal.fire({
			title: "Delete Record",
			text: "Are you sure, You Want to delete this Record!",
			icon: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			confirmButtonText: "Yes",
		}).then((result) => {
			if (result.isConfirmed) {
				$(this).closest('.dropdown-menu').find('.claimed_claim').submit();
			}
		});
	});

</script>
<script>
	$(document).on('input','.from',function(){
		var val = $(this).val();
		$('.to').attr('min',val);
	})
	$(document).on('input','.to',function(){
		var val = $('.from').val();
		if(!val){
			toastr.error('Please Select Start Date First');
			$(this).val('');
		}
	})
	$(document).on('input','.from_date',function(){
		var val = $(this).val();
		$('.to_date').attr('min',val);
	})
	$(document).on('input','.to_date',function(){
		var val = $('.from_date').val();
		if(!val){
			toastr.error('Please Select Start Date First');
			$(this).val('');
		}
	})
	$(document).on({
		mouseenter: function() {
			var text = $(this).attr('data-id');
			$('.claim_description').text(text);
			$('#description_modal').modal('show');
		},
	}, '.description_modal');
</script>

@endsection
@include('company.layout.footer')
