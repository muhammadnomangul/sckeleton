@section('pageTitle')
Configure Window
@endsection
@include('company.layout.header')
<style>
	.claim_edit_button:hover{
		background-color:#E82583;
		color: white;
	}
</style>

<!-- left side section start -->
<div class="mt-5 mb-3">
	<div class="row g-3">
		<div class="col-lg-12">
			<h3 class="mb-0" style="margin-top : 10px;margin-bottom:30px">Configure Window</h3>
		</div>
		<div class="col-lg-12">
			<form>
				<div class="form-group">
					<label>Approval Type</label>
					<select class="form-select approval_type">
						<option value="">Select Approval Type</option>
						<option value="multi" @if(isset($type) && $type->type == 'multi') selected @endif>Multi-Level Approval</option>
						<option value="single" @if(isset($type) && $type->type == 'single') selected @endif>Single-level Approval</option>
					</select>
				</div>
			</form>
		</div>
		<div class="col-lg-12">
			<div id='calendar'></div>
		</div>
	</div>
	<div class="modal fade " id="window_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-md">
			<div class="modal-content">
				<div class="modal-header border-0">
					<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Configure Window</h1>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<form method="POST"action="{{route('company.set_window')}}">
						@csrf
						<div class="row">
							<div class="col-md-12">
								<label>Start Date</label>
								<input type="date" class="form-control start_date" name="start" min="{{Carbon\Carbon::now()->toDateString()}}" required>
							</div>
							<div class="col-md-12">
								<label>End Date</label>
								<input type="date" class="form-control end_date" name="end" required>
							</div>
							<div class="col-md-12 text-center mt-2 mb-2">
								<button class="btn btn-md" style="background-color:#E82583;color: white;">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	@section('js')
	<script src='https://cdn.jsdelivr.net/npm/fullcalendar/index.global.min.js'></script>
	<script>
		$('.start_date').on('change',function(){
			var val = $(this).val();
			$('.end_date').attr('min', val);
		})
		document.addEventListener('DOMContentLoaded', function() {
			var today = new Date();
			var startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
			var events = [];
			var modal = true;
			var set_windows = {!! json_encode($set_windows) !!};
			set_windows.forEach(function(set_window) {
				var endDate = new Date(set_window.end);
				endDate.setDate(endDate.getDate())
				events.push({
					id: set_window.id,
					title: 'Claim Window',
					start: set_window.start,
					end: endDate,
					extendedProps: {
						id: set_window.id
					}
				});
			});

			const calendarEl = document.getElementById('calendar')
			const calendar = new FullCalendar.Calendar(calendarEl, {
				initialView: 'dayGridMonth',
				validRange: {
					start: startOfMonth,
					end: null
				},
				events: events,
				dateClick: function(info) {
					var date = info.dateStr.substring(0, 10);
					var selectedDate = new Date(date);
					var today = new Date();
					var monthstartDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), 1);
					var monthendDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth() + 1, 0);
					var offset = monthstartDate.getTimezoneOffset();
					monthstartDate = new Date(monthstartDate.getTime() - offset * 60 * 1000);
					monthendDate = new Date(monthendDate.getTime() - offset * 60 * 1000);
					var monthExists = false;
					set_windows.forEach(function(set_window) {
						var startDate = new Date(set_window.start);
						if (selectedDate.getMonth() === startDate.getMonth()) {
							monthExists = true;
							return;
						}
					});
					if (monthExists) {
						toastr.error('You Cannot Add more than one window For a Month');
						return;
					}
					if (selectedDate.getMonth() === today.getMonth() && selectedDate.getFullYear() === today.getFullYear()) {
						$('.start_date').attr('min', today.toISOString().slice(0, 10));
					} else {
						$('.start_date').attr('min', monthstartDate.toISOString().slice(0, 10));
					}
					$('.start_date').attr('max', monthendDate.toISOString().slice(0, 10));
					$('.end_date').attr('min', date);
					$('.end_date').attr('max', monthendDate.toISOString().slice(0, 10));
					$('.start_date').val(date);
					$('.end_date').val(date);
					$('#window_modal').modal('show');
				}
			});
			calendar.render();
		});
			$('.approval_type').on('change',function(){
				var type = $(this).val();
				if(type == ""){
					return;
				}
				$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						method : "POST",
						url: "{{route('company.approval_type')}}",
						data : {
							type : type,
						},
						success : function(response){
							toastr.success('Type Updated');
						}
					})
			})

	</script>
	@endsection
	@include('company.layout.footer')
