@section('pageTitle')
Survey
@endsection
@include('company.layout.header')
<style>
	body {
		background-color: pink;
		overflow: scroll
	}

	#div0 {
		margin-top: 8%;
	}

	.div {
		display: flex;
		margin-left: 2%;
	}

	#div2 {
		margin-top: 2%;
		margin-left: 8%;
		padding-right: 10px;
		margin-bottom: 2%;
	}

	#div3 {
		margin-top: 2%;
		margin-bottom: 2%;
		font-size: 16px;
		font-weight: bolder;
	}

	.div4 {
		margin-left: 10%;
		border-left: 6px solid #E82485;
		height:auto;
		margin-bottom: 4%;
	}

	.div5 {
		color: black;
		font-weight: bolder;
		font-size: 23px;
		margin-left: 2%;
	}

	.div6 {
		margin-right: 5%;
	}

	input[type=submit] {
		background-color: #E82485;
		color: white;
		margin-left: 10%;
		border: none;
		border-radius: 5px;
		padding: 8px;
		width: 17%;
		font-size: 16px;
	}

	.skip_button {
		background-color: #E82485;
		color: white;
		margin-left: 1%;
		border: none;
		border-radius: 5px;
		padding: 10px;
		width: 17%;
		font-size: 16px;
	}

	.div7 {
		font-size: 16px;
	}

	a {
		text-decoration: none;
	}

	.container-cust {
		background-color: pink;
	}

	.main {
		background-color: pink;
	}
	.checkboxParent{
		margin-left: 2%;
	}
	.custom_navs.nav-tabs {
		border-bottom: unset !important;
	}

	.ah_skip_button12 {
		background-color: #E82485 !important;
		color: white;
		margin-left: 1%;
		border: none;
		border-radius: 5px;
		padding: 3px 12px;
		width: unset;
		font-size: 16px;
	}

	.ah_skip_button12:hover {
		color: #000 !important;
	}

	.main.ah_main {
		background-color: pink;
	}

	/* ahmad faraz new navtabs code start */
</style>
<div id="div0" class="ah_navbar_par" style="padding-bottom:50px">

	<div style="max-width: 600px; margin:auto;">
		<div>
			<h5 style="font-weight:700"><span class="current_tab">1</span> of {{$surveys->count()}}</h5>
		</div>
		<form method="POST" action="{{route('company.first_time')}}" onsubmit="return validate()">
			@csrf
			<div class="card p-3 " style="margin:auto;border:1px solid #e82583;border-radius:20px ;background-color:#f5d6dc;">
				<h4 class="p-3">Let's get to know you</h4>
				<div class="tab-content p-3" id="nav-tabContent">
					@foreach($surveys as $index=>$survey)
					<div class="tab-pane fade {{$index == 0 ? 'show active' : ''}} " id="nav-home{{$index}}" role="tabpanel" aria-labelledby="nav-home-tab{{$index}}">
						<div class="div4 ah_div4 mb-2" style="margin-left: unset;">
							<div class="div5">{{$survey->question}}</div>
							<div class="checkboxParent row mb-3">
								@foreach($survey->options as $optionIndex => $option)
								<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
									<div class="div6">
										<input type="checkbox" id="checkbox{{$optionIndex}}{{$index}}" value="{{$option->option}}" class="checked" name="answer[{{$survey->id}}][]">&nbsp;
										<label for="checkbox{{$optionIndex}}{{$index}}" class="div7">{{$option->option}}</label>
									</div>
								</div>
								@endforeach
							</div>
						</div>
						@if($loop->last)
						<div class="text-center ">
							<button class="ah_skip_button12" style="color:white;border:none;">Submit</button>
						</div>
						@endif
					</div>
					@endforeach
				</div>
			</div>
		</form>
		<nav class="mt-4">
			<div class="nav custom_navs nav-tabs mb-3 d-flex gap-4" id="nav-tab" role="tablist">
				@foreach($surveys as $index=>$survey)
				<button class="nav-link {{$index == 0 ? 'active' : ''}} ah_skip_button12 nav_button_toggle" id="nav-home-tab{{$index}}" data-bs-toggle="tab" data-bs-target="#nav-home{{$index}}" type="button" data-id="{{$index + 1}}" role="tab" aria-controls="nav-home{{$index}}" aria-selected="{{$index == 0 ? 'true' : 'false'}}" style="border:none;color:white;">{{$index + 1}}</button>
				@endforeach
				<a href="{{route('company.skip')}}">
					<button class="nav-link ah_skip_button12" style="color:white;border:none;">Skip</button>
				</a>
			</div>
		</nav>
	</div>
<!-- <div id="div0" class="ah_navbar_par" style="padding-bottom:50px">

	<div class="div smallViewcontent">
		<div id="div2"><img style="height:50px;width:50px;" src="{{asset('cleon_styling/image/icons/dashboardicon.png')}}"></div>
		<div id="div3" class="ah_firstPage_heading" >Hold on tight! We got you</div>
	</div>
	<form method="POST" action="{{route('company.first_time')}}" onsubmit="return validate()">
		@csrf
		@foreach($surveys as $index=>$survey)
		<div class="div4 ah_div4 mb-2">
			<div class="div5">{{$survey->question}}</div>
			<div class="checkboxParent row mb-3">
				@foreach($survey->options as $optionIndex => $option)
				<div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
					<div class="div6">  
						<input type="checkbox" id="checkbox{{$optionIndex}}{{$index}}" value="{{$option->option}}" class="checked" name="answer[{{$survey->id}}][]">&nbsp;
						<label for="checkbox{{$optionIndex}}{{$index}}" class="div7">{{$option->option}}</label>
					</div>
				</div>
				@endforeach
			</div>
		</div>
		@endforeach
		<div class="mb-2 ah_first_page_btn">
			<button type="submit" class="ah_skip_button1 text-light mb-2">I'm done, take me in</button>
			<a class="skip_button" href="{{route('company.skip')}}"><button class="btn btn-sm" style="color:white" type="button">Skip</button></a>
		</div>
	</form>
</div> -->
@section('js')
<script>
	$('.nav_button_toggle').on('click',function(){
		var count = $(this).attr('data-id');
		$('.current_tab').text(count);
	})
	$('.checked').on('click',function(){
		$(this).closest('.checkboxParent').find('.checked').prop('checked',false);
		$(this).prop('checked',true);
	})
	function validate() {
		var isValid = true;    
		$('.checkboxParent').each(function() {
			var isChecked = $(this).find('input[type="checkbox"]:checked').length > 0;
			if (!isChecked) {
				isValid = false;
				toastr.error('Please Answer all Questions');
				return false;
			}
		});

		return isValid;
	}

</script>
@endsection
@include('company.layout.footer')