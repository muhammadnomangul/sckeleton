		<div class="modal-header border-0">
			<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Edit Claim</h1>
			<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
		</div>
		<form method="POST" class="employee_claim_response" action="{{url('company/employee/claim/update')}}" enctype="multipart/form-data">
			@csrf
			<div class="modal-body">
				<div class="row g-3">
					<div class="col-12">
						<div class="input-group bg-input p-1">
							<select class="form-select status_select" name="status" required >
								<option value="approved" @if($claim->status == "approved") selected @endif>Approved</option>
								<option value="declined" @if($claim->status == "declined") selected @endif>Declined</option>
							</select>
						</div>
					</div>
					<div class="col-12 {{$claim->status == 'declined' ? '' : 'd-none'}}">
						<div class="input-group bg-input p-1">
							<input type="text" class="form-control reason" placeholder="Reason For Declining Claim" name="reason" {{$claim->status == 'declined' ? '' : 'disable'}} required>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="source" value="{{$src}}">
			<div class="modal-footer justify-content-center">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12">
						<button type="button"
						class="btn btn_create text-white d-block px-5 fw-semibold submit_employe_claim">Update Claim</button>
					</div>
				</div>
			</div>
		</form>