
@section('pageTitle')
Dashboard
@endsection
@include('company.layout.header')
<style>
	.filter.active{
		background-color: #E82583 !important;
		color: white !important;
	}
	.filtering.active{
		background-color: #E82583 !important;
		color: white !important;
	}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<div class="mt-4 mb-3">
	<div class="row g-3">
		<div class="col-lg-12">
			<div class="d-flex align-items-center flex-wrap" style="gap: 10px;">
				<a class="text-decoration-none text-secondary bg-grey rounded-5 btncust3 active text-center fs-12 filter px-3" data-id="month">
					Last month
				</a>
				<div>
					<div id="calendar"
					class="form-select formcust-select fs-12 btncust3 text-center filtering px-4 rounded-5 border-0"
					style="cursor: pointer; font-size: 0.6rem;color: black;background: #f5f5f5;">
					Custom Calendar
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="row g-3">
			<div class="col-xl-12 col-lg-12">
				<div class="row g-3">
					<!-- card one start -->
					<div class="col-md-12 col-lg-2">
						<div class="card border-0 shadow rounded-3 h-100 bg-pink">
							<div class="card-body pb-0">
								<!-- inner row 1 end -->
								<div>
									<div class="d-flex align-items-center">
										<img src="{{asset('styling/image/icons/total-claim.png')}}" class="img-fluid w-40s">
										<div class="ms-3">
											<p class="text-capitalize text-white mb-0 fs-14">Due Claims Types</p>
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer border-0 bg-transparent mt-5 mb-4">
								<h6 class="text-capitalize fw-bold text-white mb-0 fs-14 ">₦ <span class="total_value">15,254</span></h6>
							</div>
						</div>
					</div>
					<div class="col-lg-10">
						<div class="row g-3">
							<!-- card two start -->
							<div class="col-md-6 col-lg-4">
								<div class="card border-0 shadow rounded-3 h-100 light-pink-gray">
									<div class="card-body pb-0">
										<!-- inner row 1 end -->
										<div class="row g-3 justify-content-between">
											<div class="col-lg-6">
												<div class="d-flex align-items-center">
													<img src="{{asset('styling/image/icons/total-claim-req.png')}}"
													class="img-fluid w-40s">
													<div class="ms-3 mb-2">
														<p
														class="text-capitalize text-white mb-0 fs-14">
														Total Claim Requests
													</p>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div
											class="d-flex align-items-center justify-content-lg-end">
											<img src="{{asset('styling/image/icons/up-green.png')}}"
											class="img-fluid total_claim_green">
											<img src="{{asset('styling/image/icons/red-down.png')}}"
											class="img-fluid total_claim_red">
										</div>
									</div>
								</div>
							</div>
							<div class="card-footer border-0 bg-transparent mt-5 mb-4">
								<h6 class="text-capitalize fw-bold text-white mb-0 fs-14 total_claims"></h6>
							</div>
						</div>
					</div>
					<!-- card three start -->
					<div class="col-md-6 col-lg-4">
						<div class="card border-0 shadow rounded-3 h-100 bg-skyish">
							<div class="card-body pb-0">
								<!-- inner row 1 end -->
								<div class="row g-3 justify-content-between">
									<div class="col-lg-6">
										<div class="d-flex align-items-center">
											<img src="{{asset('styling/image/icons/total-approve-claim.png')}}"
											class="img-fluid w-40s">
											<div class="ms-mb-2">
												<p
												class="text-capitalize text-white mb-0 fs-14">
												Total Approved Claims
											</p>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div
									class="d-flex align-items-center justify-content-lg-end">
									<img src="{{asset('styling/image/icons/red-down.png')}}"
									class="img-fluid total_approve_red">
									<img src="{{asset('styling/image/icons/up-green.png')}}"
									class="img-fluid total_approve_green">
								</div>
							</div>
						</div>
					</div>
					<div class="card-footer border-0 bg-transparent mt-5 mb-4">
								<h6 class="text-capitalize fw-bold text-white mb-0 fs-14 approved_claim"></h6>
							</div>
				</div>
			</div>

			<!-- card four start -->
			<div class="col-md-6 col-lg-4">
				<div class="card border-0 shadow rounded-3 h-100 dark-greenish">
					<div class="card-body pb-0">
						<!-- inner row 1 end -->
						<div class="row g-3 justify-content-between">
							<div class="col-lg-6">
								<div class="d-flex align-items-center">
									<img src="{{asset('styling/image/icons/total-cancel-claim.png')}}"
									class="img-fluid w-40s">
									<div class="ms-3 mb-2">
										<p
										class="text-capitalize text-white mb-0 fs-14">
										Total Declined Claims
									</p>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div
							class="d-flex align-items-center justify-content-lg-end">
							<img src="{{asset('styling/image/icons/red-down.png')}}"
							class="img-fluid total_declined_red">
							<img src="{{asset('styling/image/icons/up-green.png')}}"
							class="img-fluid total_declined_green">
						</div>
					</div>
				</div>
			</div>
			<div class="card-footer border-0 bg-transparent mt-5 mb-4">
								<h6 class="text-capitalize fw-bold text-white mb-0 fs-14 declined_claim"></h6>
							</div>
		</div>
	</div>
	<!-- cards end -->
</div>
</div>

</div>
</div>
<div class="col-lg-3 col-xl-2">
	<div class="card shadow border-0 h-100 rounded-4">
		<div class="card-body">
			<h6 class="text-capitalize fw-bold mb-4 mt-2">Claim Types</h6>
			<ul class="navbar-nav ps-0 ulistcard">
				@forelse($claims as $claim)
				<li>
					<span>{{$claim->title}}</span>
				</li>
				@empty
				<li>
					<span>No Claim Found</span>
				</li>
				@endforelse
			</ul>
		</div>
	</div>
</div>
<div class="col-xl-5 col-lg-5">
	<div class="card shadow border-0 h-100 rounded-4">
		<div class="card-body position-relative">
			<div class="mb-2">
				<div id="chartContainer" style="height: 180px; width: 100%;"></div>
			</div>
		</div>
	</div>
</div>
<div class="col-xl-5 col-lg-4">
	<div class="card shadow border-0 h-100 rounded-4">
		<div class="card-body position-relative">
			<div class="mb-2">
				<div id="chartContainerone" style="height: 180px; width: 100%;"></div>
			</div>
		</div>
	</div>
</div>
<div class="col-lg-4">
	<div class="card shadow border-0 rounded-4 mt-3">
		<div class="card-body position-relative">
			<div class="mb-2">
				<h6 class="text-capitalize fw-bold mb-4 text-center">
					Trend of Total Claim Requested Amount
				</h6>
				<div id="chartContainertwo" style="height: 200px; width: 100%;"></div>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-4">
	<div class="card shadow border-0 rounded-4 mt-3">
		<div class="card-body position-relative">
			<div class="mb-2">
				<h6 class="text-capitalize fw-bold mb-4 text-center">
					Trend of Total Approved Claims Amount
				</h6>
				<div id="chartContainerthree" style="height: 200px; width: 100%;"></div>
			</div>
		</div>
	</div>
</div>

<div class="col-lg-4">
	<div class="card shadow border-0 rounded-4 mt-3">
		<div class="card-body position-relative">
			<div class="mb-2">
				<h6 class="text-capitalize fw-bold mb-4 text-center">
					Trend of Total Declined Claims Amount
				</h6>
				<div id="chartContainerfour" style="height: 200px; width: 100%;"></div>
			</div>
		</div>
	</div>
</div>


</div>
</div>
</div>
</div>
<div class="modal fade" tabindex="-1" id="kt_modal_1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Date Range</h5>
				<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
					<span class="svg-icon fs-2x"></span>
				</div>
			</div>
			<div class="modal-body">
				<div class="mb-0">
					<label for="" class="form-label">Select date</label>
					<input class="form-control form-control-solid" placeholder="Pick date" id="kt_datepicker_10"/>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-light" data-bs-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary apply_date_filter">Apply</button>
			</div>
		</div>
	</div>
</div>
<!--end::Modal-->
@section('js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<script>
	$("#kt_datepicker_10").daterangepicker();
	$('#calendar').on('click',function(){
		$('#kt_modal_1').modal('show');
	})
	$(document).ready(function() {
		var data="month";
		update_chart(data);
	});
	$('.apply_date_filter').on('click',function(){
		var data = $('#kt_datepicker_10').val();
		update_chart(data);
		$('#kt_modal_1').modal('hide');
	})
	function update_chart(data){
		$.ajax({
			url: '{{route("company.chart_data")}}',
			method: 'GET',
			data: {
				data : data
			},
			success: function(response) {
				console.log(response);
				var approvedClaims = response.approved;
				var declinedClaims = response.declined;
				$('.approved_claim_amount').text(response.approve_amount);
				$('.approved_claim').text(response.approved);
				$('.declined_claim_amount').text(response.decline_amount);
				$('.declined_claim').text(response.declined);
				$('.total_claims').text(response.claim);
				$('.total_value').text(response.total_amount);
				$('.total_declined_request').text(Math.abs(response.declined_trend_all - response.declined));
				$('.total_approved_request').text(Math.abs(response.approve_trend_all - response.approved));
				$('.total_claim_request').text(Math.abs(response.claim_trend_all - response.claim));
				if(response.declined_trend_all > response.declined){
					$('.total_declined_green').addClass('d-none');
					$('.total_declined_red').removeClass('d-none');
				}else{
					$('.total_declined_green').removeClass('d-none');
					$('.total_declined_red').addClass('d-none');
				}
				if(response.approve_trend_all > response.approved){
					$('.total_approve_green').addClass('d-none');
					$('.total_approve_red').removeClass('d-none');
				}else{
					$('.total_approve_green').removeClass('d-none');
					$('.total_approve_red').addClass('d-none');
				}
				if(response.claim_trend_all > response.claim){
					$('.total_claim_green').addClass('d-none');
					$('.total_claim_red').removeClass('d-none');
				}else{
					$('.total_claim_green').removeClass('d-none');
					$('.total_claim_red').addClass('d-none');
				}
				CanvasJS.addColorSet("greenShades",
					[
						"#E82583",
						"#009896",
						]);
				console.log(approvedClaims,declinedClaims);
				var chartData = [];

				if (approvedClaims == 0 && declinedClaims == 0) {
					chartData.push({ y: 1, label: "No Claim Found" });
				} else {
					chartData.push({ y: approvedClaims, label: "Total Approved Claims" });
					chartData.push({ y: declinedClaims, label: "Total Declined Claims" });
				}

				var chart = new CanvasJS.Chart("chartContainer", {
					colorSet: "greenShades",
					animationEnabled: true,
					data: [{
						type: "doughnut",
						startAngle: 60,
						indexLabelFontSize: 10,
						indexLabel: "{label} - #percent%",
						indexLabelLineDashType: "dash",
						toolTipContent: "<b>{label}:</b> {y} (#percent%)",
						dataPoints: chartData
					}]
				});

				chart.render();

				var chartone = new CanvasJS.Chart("chartContainerone", {
					colorSet: "oneDoughnut",
					animationEnabled: true,
					data: [{
						type: "doughnut",
						startAngle: 60,
						indexLabelFontSize: 10,
						indexLabel: "{label} - #percent%",
						indexLabelLineDashType: "dash",
						toolTipContent: "<b>{label}:</b> {y} (#percent%)",
						dataPoints: chartData

					}]
				});
				chartone.render();
				var dataPoints = [];

				for (var i = 0; i < response.datapoints.length; i++) {
					dataPoints.push({
						x: new Date(response.datapoints[i].x),
						y: response.datapoints[i].y
					});
				}
				var charttwo = new CanvasJS.Chart("chartContainertwo", {
					animationEnabled: true,
					theme: "light",
					axisY: {
						valueFormatString: "#0,.",
						suffix: "k"
					},
					toolTip: {
						shared: true
					},
					data: [
					{
						type: "stackedArea",
						color: '#B2DF8A',
						toolTipContent: "<span style=\"color:#C0504E\"><strong>{name}: </strong></span> {y}<br><b>Total:<b> #total",
						dataPoints: dataPoints,
					}]
				});
				charttwo.render();


				var approved_points = [];

				for (var i = 0; i < response.approve_points.length; i++) {
					approved_points.push({
						x: new Date(response.approve_points[i].x),
						y: response.approve_points[i].y
					});
				}
				var chartthree = new CanvasJS.Chart("chartContainerthree", {
					animationEnabled: true,
					theme: "light",
					axisY: {
						valueFormatString: "#0,.",
						suffix: "k"
					},
					toolTip: {
						shared: true
					},
					data: [
					{
						type: "stackedArea",
						color: '#009896',
						dataPoints: approved_points,

					}]
				});
				chartthree.render();


				var declined_points = [];

				for (var i = 0; i < response.declined_points.length; i++) {
					declined_points.push({
						x: new Date(response.declined_points[i].x),
						y: response.declined_points[i].y
					});
				}

				var chartfour = new CanvasJS.Chart("chartContainerfour", {
					animationEnabled: true,
					theme: "light",
					axisY: {
						valueFormatString: "#0,.",
						suffix: "k"
					},
					toolTip: {
						shared: true
					},
					data: [
					{
						type: "stackedArea",
						color: '#E82583',
						dataPoints: declined_points,
					}]
				});
				chartfour.render();
			},
			error: function() {
				console.error("Failed to fetch data");
			}
		});
}
$('.filter').on('click',function(){
	$('.filter').removeClass('active');
	$('.filtering').removeClass('active');
	$(this).addClass('active');
	var data = $(this).attr('data-id');
	update_chart(data);		
})

$('.filtering').on('click',function(){
	$('.filter').removeClass('active');
	$(this).addClass('active');	
})

$(document).on('click', '.calendar-dates-day', function () {
	var day = $(this).text();
    var monthYear = $('.calendar-title-text').text().trim(); // Remove leading/trailing spaces
    var month = monthYear.split(' - ')[0]; // Split the string to get the month
    var year = parseInt(monthYear.split(' - ')[1]); // Split the string to get the year and convert to integer

    var parsedDate = Date.parse(month + ' ' + day + ',' + year);

    if (!isNaN(parsedDate)) {
        // Format the date
    	var formattedDate = new Date(parsedDate).toLocaleDateString('en-US', {
    		day: 'numeric',
    		month: 'long',
    		year: 'numeric',
    	});
    	update_chart(formattedDate);
    } else {
    	alert('Invalid date format');
    }
});


</script>
@endsection

@include('company.layout.footer')
