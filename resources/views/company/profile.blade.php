	@section('pageTitle')
Profile
@endsection

@include('company.layout.header')
<style>
	.dropify-wrapper {
		width: 100% !important;
		height: 100% !important;
	}
	.dropify-clear{
		display: none !important;
	}
	.select2-container--default{
		width: 100% !important;
	}
	.dropify-render img{
		height: 100% !important;
		width: 100% !important;
	}
</style>

<!-- left side section start -->
<div class="mt-5 mb-3">
	<div class="row g-3">
		<div class="col-lg-12" style="margin-top:40px">
		    		    <h1>Edit Your Profile</h1>
			<form class=" row g-3" method="POST" action="{{route('company.profile')}}" enctype="multipart/form-data">
				@csrf
				<div class="col-lg-3">
					<div class="input-group mt-2 p-1 d-flex justify-content-center" style="width:100%; height: 100%;">
						<input type="file" name="image" class="dropify" data-default-file="{{ asset($user->image ?? '')}}"  accept="image/*" />
						<div class="preview" name="image"></div>   
					</div>
				</div>
				<div class="col-lg-9 row">
					<div class="col-lg-12" style="margin-bottom:10px">
						<label>First Name</label>
						<div class="input-group bg-input mt-2 p-1">
							<input type="text" placeholder="First Name" name="first_name" value="{{$user->name}}" class="form-control fs-14 bg-transparent border-0">
						</div>
					</div>
					<div class="col-lg-12" style="margin-bottom:10px">
						<label>Last Name</label>
						<div class="input-group bg-input mt-2 p-1">
							<input type="text" placeholder="Last Name" name="last_name" value="{{$user->l_name}}" class="form-control fs-14 bg-transparent border-0">
						</div>
					</div>
					<div class="col-lg-12" style="margin-bottom:10px">
						<label>Email</label>
						<div class="input-group bg-input mt-2 p-1">
							<input type="email" placeholder="Email" name="email" value="{{$user->email}}" class="form-control fs-14 bg-transparent border-0">
						</div>
					</div>
					<div class="col-lg-12" style="margin-bottom:10px">
						<label>Phone</label>
						<div class="input-group bg-input mt-2 p-1">
							<input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" placeholder="Phone" name="phone" value="{{$user->phone}}" class="form-control fs-14 bg-transparent border-0">
						</div>
					</div>
				</div>
				<div class="col-md-12 d-flex justify-content-center">
					<div class="mt-3" style="width:30%">
						<button class="text-decoration-none bg-pink fs-13 fw-bold text-center rounded-3 d-block text-white p-3 w-100">
							Submit
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<!-- left side section end -->
</div>
</div>
@include('company.layout.footer')
