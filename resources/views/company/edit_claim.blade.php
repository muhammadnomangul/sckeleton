			<div class="modal-header border-0">
				<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Edit Claim Type</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form method="POST" action="{{url('company/claim/update/'.$claim->id)}}" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
					<div class="row g-3">
						<div class="col-12">
							<label>Title</label>
							<div class="input-group bg-input p-1">
								<input type="text" name="title" value="{{$claim->title}}" class="form-control fs-12 bg-transparent border-0"
								placeholder="Enter Title" required>
							</div>
						</div>
						<div class="col-12">
							<label>Value</label>
							<div class="input-group bg-input p-1">
								<input type="text" oninput="this.value = this.value.replace(/[^0-9]/g, '')" name="value" value="{{$claim->value}}" class="form-control fs-12 bg-transparent border-0" placeholder="Enter Value" required>
							</div>
						</div>
						<div class="col-12">
							<label>Schedule</label>
							<div class="input-group bg-input p-1">
								<select name="schedule" class="form-select fs-12 bg-transparent border-0" placeholder="Enter Schedule" required>
									<option value="">Claim Type Schedule</option>
									<option value="hourly" @if($claim->schedule == "hourly") selected @endif>Hourly</option>
									<option value="daily" @if($claim->schedule == "daily") selected @endif>Daily</option>
									<option value="weekly" @if($claim->schedule == "weekly") selected @endif>Weekly</option>
									<option value="monthly" @if($claim->schedule == "monthly") selected @endif>Monthly</option>
								</select>
							</div>
						</div>
						<div class="col-12">	
							<label>Description</label>
							<div class="input-group bg-input p-1">
								<textarea name="desc" id="" rows="4" class="form-control fs-14 bg-transparent border-0"
								placeholder="Description" required>{{$claim->desc}}</textarea>
							</div>
						</div>
					</div>
					
				</div>
				<div class="modal-footer justify-content-center">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12">
							<button type="submit"
							class="btn btn_create text-white d-block px-5 fw-semibold">Update Claim</button>
						</div>
					</div>
				</div>
			</form>