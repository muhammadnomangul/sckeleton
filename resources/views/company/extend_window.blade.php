@section('pageTitle')
Extend Claim Window
@endsection
@include('company.layout.header')
<style>
	.claim_edit_button:hover{
		background-color:#E82583;
		color: white;
	}
</style>

<!-- left side section start -->
<div class="mt-5 mb-3">
	<div class="row g-3">
		<div class="col-lg-12">
			<h3 class="mb-0" style="margin-top : 10px;margin-bottom:30px">Extend Claim Window</h3>
			<div id='calendar'></div>

		</div>
	</div>
	<div class="modal fade " id="window_modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-md">
			<div class="modal-content">
				<div class="modal-header border-0">
					<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Update Window</h1>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body">
					<form method="POST"action="{{route('company.extend_window_claim')}}">
						@csrf
						<input type="hidden" name="id" class="extend_id">
						<div class="row">
							<div class="col-md-12">
								<label>Start Date</label>
								<input type="date" class="form-control start_date" name="start" min="{{Carbon\Carbon::now()->toDateString()}}" required>
							</div>
							<div class="col-md-12">
								<label>End Date</label>
								<input type="date" class="form-control end_date" name="end" required>
							</div>
							<div class="col-md-12 text-center mt-2 mb-2">
								<button class="btn btn-md" style="background-color:#E82583;color: white;">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	@section('js')
	<script src='https://cdn.jsdelivr.net/npm/fullcalendar/index.global.min.js'></script>
	<script>

		document.addEventListener('DOMContentLoaded', function() {
			var today = new Date();
			var startOfMonth = new Date(today.getFullYear(), today.getMonth(), 1);
			var events = [];
			var modal = true;
			var set_windows = {!! json_encode($set_windows) !!};
			set_windows.forEach(function(set_window) {
				var endDate = new Date(set_window.end);
				endDate.setDate(endDate.getDate())
				events.push({
					id: set_window.id,
					title: 'Claim Window',
					start: set_window.start,
					end: endDate,
					extendedProps: {
						id: set_window.id
					}
				});
			});

			const calendarEl = document.getElementById('calendar')
			const calendar = new FullCalendar.Calendar(calendarEl, {
				initialView: 'dayGridMonth',
				validRange: {
					start: startOfMonth,
					end: null
				},
				events: events,
				editable: true, // Enable event dragging
				eventDrop: function(event) {
					var id = event.event._def.publicId;
					var start = new Date(event.event.start);
					var end = new Date(event.event.end);
					start.setDate(start.getDate() + 1);
					var startStr = start.toISOString().substring(0, 10);
					var endStr = end.toISOString().substring(0, 10);
					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						}
					});
					$.ajax({
						method : "POST",
						url: "{{route('company.extend_window')}}",
						data : {
							id : id,
							start : startStr,
							end : endStr,
						},
						success : function(response){
							console.log('Event Update');
							toastr.success('Window Updated');
						}
					})
				},
				dateClick: function(info) {
					toastr.error('You Cannot Add a New Window From Here');
				},
				eventClick: function(info) {
					var id = info.event._def.publicId;
					$('.extend_id').val(id);
					var start = new Date(info.event.start);
					var end = new Date(info.event.end);
					start.setDate(start.getDate() + 1);
					var startStr = start.toISOString().substring(0, 10);
					var endStr = end.toISOString().substring(0, 10);
					var selectedDate = new Date(start);
					var monthstartDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), 1);
					var monthendDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth() + 1, 0);
					var offset = monthstartDate.getTimezoneOffset();
					monthstartDate = new Date(monthstartDate.getTime() - offset * 60 * 1000);
					monthendDate = new Date(monthendDate.getTime() - offset * 60 * 1000);
					if (selectedDate.getMonth() === today.getMonth() && selectedDate.getFullYear() === today.getFullYear()) {
						$('.start_date').attr('min', today.toISOString().slice(0, 10));
					} else {
						$('.start_date').attr('min', monthstartDate.toISOString().slice(0, 10));
					}
					$('.start_date').attr('max', monthendDate.toISOString().slice(0, 10));
					$('.end_date').attr('min', startStr);
					$('.end_date').attr('max', monthendDate.toISOString().slice(0, 10));
					$('.start_date').val(startStr);
					$('.end_date').val(endStr);
					$('#window_modal').modal('show');
				},
				eventAllow: function(dropInfo, draggedEvent) {
					var firstDateOfMonth = new Date(dropInfo.start.getFullYear(), dropInfo.start.getMonth(), 1);
					var lastDateOfMonth = new Date(dropInfo.start.getFullYear(), dropInfo.start.getMonth() + 1, 0);
					var isStartAfterFirstDate = dropInfo.start >= firstDateOfMonth;
					var isEndBeforeOrOnLastDate = dropInfo.end.getFullYear() === lastDateOfMonth.getFullYear() &&
					dropInfo.end.getMonth() === lastDateOfMonth.getMonth() &&
					dropInfo.end.getDate() <= lastDateOfMonth.getDate();

					return isStartAfterFirstDate && isEndBeforeOrOnLastDate;
				}
			});
			calendar.render();
		});

	</script>
	@endsection
	@include('company.layout.footer')
