@section('pageTitle')
Get Started
@endsection
@include('company.layout.header')
<style>
.title_img {
    width: 75%;
    margin: auto;
}
@media(min-width:601px) and (max-width:768px) {
    .multi_img img {
        display: none;
    }
}
@media(min-width:481px) and (max-width:767px) {
        .row_reverse {
        display: flex;
        flex-direction: column-reverse;
    }
}
@media(min-width:481px) and (max-width:600px) {
    .multi_img {
        display: none;
    }
    .title_img {
        width: 75%;
        margin: 30px auto 0 auto;
    }
}

@media(min-width:321px) and (max-width:480px) {
    .checkboxParent {
        display: flex;
        flex-direction: column;
    }
    .multi_img {
        display: none;
    }

    .row_reverse {
        display: flex !important;
        flex-direction: column-reverse !important;
    }

    .title_img {
        width: 75%;
        margin: 30px auto 0 auto;
    }
}

@media(max-width:320px) {
    .multi_img img {
        display: none;
    }
    .row_reverse {
        display: flex;
        flex-direction: column-reverse !important;
    }
    .title_img {
        width: 75%;
        margin: 30px auto 0 auto;
    }
}
</style>
<div class="row" style="margin-bottom: 100px;position: relative;">
	<div class="col-md-12 ">
		<div class="multi_img">
			<img class="" style="float:right;width:64%" src="{{asset('cleon_styling/image/bg-multi.png')}}">
		</div>
	</div>
	<div class="col-md-12 row row_reverse" style="position:absolute;top:15.5% ">
		<div class="col-md-5" style="margin-top:13%;">
		<div class="text-center">
				<h3 style="font-weight: 600;">Welcome to Cleontime</h3>
				<h1 style="font-weight:700">Are you ready to plug the gaps in employee claims?</h1>
				<div style="margin-top:15px">
					<a href="{{route('company.setup_claim')}}"><button class="btn btn-md" style="background-color: #f9dbe0;padding: 13px 17px"><img src="{{asset('cleon_styling/image/customer-support.png')}}">I'm in, take me there</button></a>
				</div>
			</div>
		</div>
		<div class="col-md-7">
			<div class="title_img">
				<img class="img-fluid"  src="{{asset('cleon_styling/image/get_start.png')}}">
			</div>
			
		</div>
	</div>
</div>

@include('company.layout.footer')