	</div>
</div>
<div class="modal fade" id="decline_notification" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-md">
		<div class="modal-content">
			<div class="modal-header border-0">
				<h1 class="modal-title fs-4 fw-semibold text-center" id="exampleModalLabel">Decline Claim</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<form method="POST" action="" class="decline_notification_form" enctype="multipart/form-data">
				@csrf
				<div class="modal-body">
					<div class="row g-3">
						<div class="col-12">
							<label>Reason</label>
							<div class="input-group bg-input p-1">
								<input type="text" name="reason" class="form-control fs-12 bg-transparent border-0"
								placeholder="Reason For Claim Declining">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer justify-content-center">
					<div class="row">
						<div class="col-12 col-sm-12 col-md-12">
							<button type="submit"
							class="btn btn_create text-white d-block px-5 fw-semibold">Submit</button>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10.14.0/dist/sweetalert2.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script> -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> -->

	<!-- calendar script start  -->
	<script src='https://unpkg.com/dayjs@1.8.21/dayjs.min.js'></script>
	<script src="{{asset('styling/js/calendar-script.js')}}"></script>
	<!-- calendar script end  -->

	<!-- js area -->
	<script type="text/javascript" src="{{asset('styling/js/jquery-3.6.0.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('styling/js/bootstrap.bundle.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('styling/js/my_script.js')}}"></script>
	<script src="https://cdn.canvasjs.com/canvasjs.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>
	<script type="text/javascript" src="{{asset('styling/js/dropdown.js')}}"></script>
	<!-- Bottom slider script -->
	<!-- <script src="{{asset('styling/Slide/slider.js')}}"></script> -->
	<!-- <script src="{{asset('styling/Notification/noty.js')}}"></script> -->
	<script src="{{asset('styling/Notification/profile1.js')}}"></script>
	<script src="{{asset('styling/Notification/profile2.js')}}"></script>
	<script src="{{asset('styling/Notification/profile3.js')}}"></script>

	<link href="https://cdn.jsdelivr.net/npm/toastr@2.1.4/build/toastr.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/toastr@2.1.4/toastr.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/dropify/dist/js/dropify.min.js"></script>

	<script>
		$('.dropbtn1').on('click',function(){
			$(this).parent().find('.dropdown-content').toggle('show');
		})
	</script>
	<script>
		@if (Session::has('success'))
		toastr.success("{{ Session::get('success') }}");
		@endif


		@if (Session::has('info'))
		toastr.info("{{ Session::get('info') }}");
		@endif


		@if (Session::has('warning'))
		toastr.warning("{{ Session::get('warning') }}");
		@endif
		@if (Session::has('message'))
		toastr.success("{{ Session::get('success') }}");
		@endif

		@if (Session::has('error'))
		toastr.error("{{ Session::get('error') }}");
		@endif
	</script>

	@if (session('toast_error'))
	<script>
		var errors = {!! json_encode(session('toast_error')) !!};
		for (var i = 0; i < errors.length; i++) {
			toastr.error(errors[i], 'Validation Error');
		}
	</script>
	@endif

	<script type="text/javascript">

		// menu toggle
		let toggle = document.querySelector('.toggle');
		let navigation = document.querySelector('.navigation');
		let main = document.querySelector('.main');
		let img = document.querySelector('.img');
		let img1 = document.querySelector('.img1');





		// add hovered class in selected list item

		let list = document.querySelectorAll('.navigation li');
		function activeLink() {
			list.forEach((item) =>
				item.classList.remove('hovered'));
			this.classList.add('hovered');
		}
		list.forEach((item) =>
			item.addEventListener('mouseover', activeLink)
			);

		let off = function () {
			$('#calendar').toggleClass('display');
		}
		document.querySelector('#filter').addEventListener('click', off);

		$(document).ready(function () {
			$('#colheadone').click();
			$('#claim_table').DataTable({
				"paging": false, 
				"searching": false,
				"ordering": true, 
				"info": false, 
				"order": [],
			});
		});

	</script>

	<script>
		window.onload = function () {


			CanvasJS.addColorSet("greenShades",
				[
					"#E82583",
					"#009896",
					]);

			CanvasJS.addColorSet("oneDoughnut",
				[

					"#694BDB",
					"#FF7777",
					]);

			CanvasJS.addColorSet("twoDoughnut",
				[

					"#694BDB",
					"#009896",
					]);
		};
	</script>


	<!-- filter range slider js -->
	<script type="text/javascript">
		var slider = document.getElementById("slider");
		var selector = document.getElementById("selector");
		var selectvalue = document.getElementById("Selectvalue");
		var progressbar = document.getElementById("progressbar");

		selectvalue.innerHTML = slider.value + '%';

		slider.oninput = function () {
			selectvalue.innerHTML = this.value + '%';
			selector.style.left = this.value + "%";
			progressbar.style.width = this.value + "%";
		}
	</script>
	<script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>

	<script>
		$(document).ready(function(){
			$('.dropify').dropify();
			$('#logout-form').attr('action' ,'{{route("logout")}}');
			$('#logout-form').append('@csrf');
		})
	</script>
	@yield('js')
	<script>
		$('.toogle_navigation').on('click',function(){
			$('.navigation').toggleClass('active');
			$('.main').toggleClass('active');
			$('.img, .imgsize').toggleClass('active');
			if($('.navigation').hasClass('active')){
				$('.open_navigation').removeClass('d-none');
				$('.close_navigation').addClass('d-none');
			}else{
				$('.open_navigation').addClass('d-none');
				$('.close_navigation').removeClass('d-none');
			}	
			$.ajax({
				method : 'GET',
				url : '{{route("company.sidebar")}}',
				success:function(response){
					
				}
			})
		})
	</script>
</body>

</html>