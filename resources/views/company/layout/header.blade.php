<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" href="{{asset('assets/images/favicon.ico')}}">
	<title>@yield('pageTitle')</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" type="text/css" href="{{asset('styling/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('styling/css/style.css')}}">
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<link rel="stylesheet" type="text/css" href="{{asset('styling/css/jquery.dataTables.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/doc_calendar.css')}}">
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/sweetalert2@10.14.0/dist/sweetalert2.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/dropify/dist/css/dropify.min.css">
	<style type="text/css">
		.form-control:focus {
			box-shadow: none !important;
		}

		.input-group-text ion-icon {
			font-size: 24px;
		}

		tr {
			white-space: nowrap;
		}

		.display {
			display: none;
		}
		.dropdown-divider {
			border-top: 1px solid #ccc;
			margin: 5px 0;
		}
		.switchProfile{
			cursor: pointer;
		}
		.colorchangefour{
			cursor : pointer;
		}
		.dropdown-content{
			min-width : 180px !important;
		}
		.switch-profile-header {
			font-weight: bold;
			margin-bottom: 5px;
			color: #555;
			margin-right: 60px;
		}
		.notification_count{
			position: absolute;
			top: 2px;
			z-index: 999;
			right: 7%;
			background: #E82583;
			color: white;
			border-radius: 55px;
			padding: 3px 7px;
			font-size: 10px;
		}
		.switchProfile img{
			height : unset !important;
			width : unset !important;
			border : unset !important;
			border-radius : unset !important;		    
		}
		@media (max-width: 992px) {
			.notif_parent {
				justify-content : end !important;
				margin-right : 7px;
			}
		}
	</style>
</head>

<body>

	<div class="container-cust">
		<div class="navigation {{Auth::user()->sidebar == 0 ? 'active' : ''}}">
			<ul class="ps-0 mb-0">
				<span style="float:right;margin-right: 3px;margin-top: 3px;"><img style="height: 20px;width: 20px" src="{{asset('assets/side/left.png')}}" class="close_navigation toogle_navigation {{Auth::user()->sidebar == 0 ? 'd-none' : ''}}"><img style="height: 20px;width: 20px" src="{{asset('assets/side/right.png')}}" class="open_navigation toogle_navigation {{Auth::user()->sidebar == 0 ? '' : 'd-none'}}"></span>
				<li>
					<a href="{{url('/')}}" class="d-block">
						<span class="img text-center {{Auth::user()->sidebar == 0 ? 'active' : ''}}">
							<img src="{{asset('styling/image/cir-logo.png')}}" class="img-fluid">
						</span>
						<span class="imgsize img1 fw-bold {{Auth::user()->sidebar == 0 ? 'active' : ''}}">
							<img src="{{asset('styling/image/logo.png')}}" class="img-fluid">
						</span>
					</a>
				</li>
				<li class=" {{Request::is('company/dashboard') ? 'active':''}}">
					<div class="colorchangeone rounded-5">
						<a href="{{route('company.dashboard')}}" >
							<span class="icon hr-twon">
								<img src="{{  Request::is('company/dashboard') ? asset('assets/side/dashboard-pink.png') : asset('assets/side/dashboard-white.png')}}" class="img-fluid">
							</span>
							<span class="title">Dashboard </span>
						</a>
					</div>
				</li>
				<li class=" {{Request::is('company/get-started') ? 'active':''}}">
					<div class="colorchangeone rounded-5">
						<a href="{{route('company.start')}}" >
							<span class="icon hr-twon">
								<img src="{{  Request::is('company/get-started') ? asset('assets/side/startPink.png') : asset('assets/side/startWhite.png')}}" class="img-fluid">
							</span>
							<span class="title">Get Started</span>
						</a>
					</div>
				</li>
				@php
				$access = can_access('Manage_Claim_Type');
				@endphp
				@if($access)
				<li class="{{Request::is('company/setup/claim') ? 'active':''}}">
					<a href="{{route('company.setup_claim')}}">
						<span class="icon hr-nine">
							<img src="{{  Request::is('company/setup/claim') ? asset('assets/side/claim.png') : asset('styling/image/icons/cleontime.png') }}" class="img-fluid">
						</span>
						<span class="title">Manage Claim Type</span>
					</a>
				</li>
				@endif
				<li class="{{Request::is('company/manage/claim') ? 'active':''}}">
					<a href="{{route('company.manage_claim')}}">
						<span class="icon hr-nine">
							<img src="{{  Request::is('company/manage/claim') ? asset('assets/side/data-pink.png') : asset('assets/side/data-white.png')}}" class="img-fluid">
						</span>
						<span class="title">Manage Claim Data</span>
					</a>
				</li>
				<li class="{{Request::is('company/set-window') ? 'active':''}}">
					<a href="{{route('company.set')}}">
						<span class="icon hr-nine">
							<img src="{{  Request::is('company/set-window') ? asset('assets/side/set-pink.png') : asset('assets/side/set-white.png')}}" class="img-fluid">
						</span>
						<span class="title">Configure Window</span>
					</a>
				</li>
				<li class="{{Request::is('company/extend-window') ? 'active':''}}">
					<a href="{{route('company.extend')}}">
						<span class="icon hr-nine">
							<img src="{{  Request::is('company/extend-window') ? asset('assets/side/extended-pink.png') : asset('assets/side/extended-white.png')}}" class="img-fluid">
						</span>
						<span class="title">Extend Window</span>
					</a>
				</li>
			</ul>
		</div>
		<div class="main pe-2 {{Auth::user()->sidebar == 0 ? 'active' : ''}}">
			<div class="row g-3 align-items-center">
				<div class="col-lg-10 pb-4 border-bottom border-2">
					<div class="row g-3 justify-content-between">
						<div class="col-lg-4">
							<div class="d-flex align-items-center justify-content-between">
								<div class="toggle topbar">
									<div
									class="d-flex align-items-center flex-wrap justify-content-lg-start justify-content-center mt-3">
									<img src="{{asset('cleon_styling/image/icons/dashboardicon.png')}}" class="img-fluid">
									<h5 class="mb-0 ms-2 text-capitalize">@yield('pageTitle')</h5>
								</div>
							</div>
							<div
							class="img-circle mt-2 flex-shrink-0 d-flex d-lg-none align-items-center justify-content-center">
							<img  src="{{asset(Auth::user()->image ?? 'assets/images/avatar.webp')}}" style="height : 50px" class="img-fluid rounded-pill w-25s dropbtn1" >
							<div class="dropdown-content" style="top:85px;z-index:99999999999;right:0px" id="profiledrop">
								<a href="{{ route('company.profile') }}" style="font-size:14px"><i class="fas fa-user"></i> My Profile</a>
								<a href="{{ route('company.onBoard') }}" style="font-size:14px"><i class="fa fa-user-plus" aria-hidden="true"></i>OnBoard Employee</a>
								<div class="switch-profile-header">Switch Module</div>
								<a class="switchProfile" data-title="Internal Control" style="padding: 10px 10px" style="font-size:14px" data-id="https://internalcontrol.cleonhrdev.com.ng/"><img style="padding-right: 4px" src="{{asset('assets/side/internal.png')}}">Internal Control</a>
								<a class="switchProfile" data-title="HR Administration" style="padding: 10px 10px" style="font-size:14px" data-id="https://administration.cleonhrdev.com.ng/login"><img style="padding-right: 4px" src="{{asset('assets/side/hr.png')}}">HR Administration</a>
								<a class="switchProfile" data-title="Payroll" style="padding: 10px 10px" style="font-size:14px" data-id="https://payroll.cleonhr.com/public/"><img style="padding-right: 4px" src="{{asset('assets/side/salary.png')}}">Payroll</a>
								<a class="switchProfile" data-title="Cleon Time" style="padding: 10px 10px" style="font-size:14px" data-id="https://cleontime.cleonhrdev.com.ng/"><img style="padding-right: 4px" src="{{asset('assets/side/recruit.png')}}">Recruitment</a>
								<!-- <div class="dropdown-divider"></div> -->
								<a href="{{ route('company.password') }}" style="font-size:14px"><i class="fas fa-key"></i> Change Password</a>
								<a href="" style="font-size:14px" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
								<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
									@csrf
								</form>
							</div> 
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-8">
					<div class="input-group rounded-5 bg-light-gray border-pink p-1 ps-3 pe-3 mt-3">
						<input type="text" class="form-control border-0 bg-transparent"
						placeholder="Search here..." aria-label="Search here..."
						aria-describedby="basic-addon2">
						<span class="input-group-text border-0 bg-transparent" id="basic-addon2">
							<a href="{{route('company.search')}}"><img src="{{asset('cleon_styling/image/icons/search.png')}}" class="img-fluid" style="width: 30px;"></a>
						</span>
					</div>
				</div>
				@php
				$count = Countnoti();
				@endphp
				<div class="col-lg-2 col-4 ">
					<div class="d-flex align-items-center justify-content-center mt-4 mt-lg-3 notif_parent" style="margin-left:1rem !important">
						<a href="{{route('company.notification')}}"><div id="noty-id" style="position: relative;">
							<span class="notification_count">{{$count}}</span>
							<div class="circle-bg d-flex align-items-center justify-content-center" id="tapps">
								<div class="notification">
									<i class="fas fa-bell" ></i>
								</div></div></div></a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="position-absolute posrelt text-end d-none d-lg-block" style="height:160px !important">
						<img src="{{asset('cleon_styling/image/icons/shap.png')}}" class="img-fluid imgsize1">
						<div class="userdiv" style="top:33px !important;">
							<div class="d-flex align-items-center ms-3 dropdown1">
								<img  src="{{asset(Auth::user()->image ?? 'assets/images/avatar.webp')}}" style="height : 50px" class="img-fluid rounded-pill w-25s dropbtn1">
								<div class="dropdown-content" style="top:50px;z-index:99999999999;right:0px" id="profiledrop">
									<a href="{{ route('company.profile') }}" style="font-size:14px"><i class="fas fa-user"></i> My Profile</a>
									<a href="{{ route('company.onBoard') }}" style="font-size:14px"><i class="fa fa-user-plus" aria-hidden="true"></i>OnBoard Employee</a>
									<div class="switch-profile-header">Switch Module</div>
									<a class="switchProfile" data-title="Internal Control" style="padding: 10px 10px" style="font-size:14px" data-id="https://internalcontrol.cleonhrdev.com.ng/"><img style="padding-right: 4px" src="{{asset('assets/side/internal.png')}}">Internal Control</a>
									<a class="switchProfile" data-title="HR Administration" style="padding: 10px 10px" style="font-size:14px" data-id="https://administration.cleonhrdev.com.ng/login"><img style="padding-right: 4px" src="{{asset('assets/side/hr.png')}}">HR Administration</a>
									<a class="switchProfile" data-title="Payroll" style="padding: 10px 10px" style="font-size:14px" data-id="https://payroll.cleonhr.com/public/"><img style="padding-right: 4px" src="{{asset('assets/side/salary.png')}}">Payroll</a>
									<a class="switchProfile" data-title="Cleon Time" style="padding: 10px 10px" style="font-size:14px" data-id="https://cleontime.cleonhrdev.com.ng/"><img style="padding-right: 4px" src="{{asset('assets/side/recruit.png')}}">Recruitment</a>
									<!-- <div class="dropdown-divider"></div> -->
									<a href="{{ route('company.password') }}" style="font-size:14px"><i class="fas fa-key"></i> Change Password</a>
									<a href="" style="font-size:14px" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="logout"><i class="fas fa-sign-out-alt"></i> Logout</a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
										@csrf
									</form>
								</div> 
								<div class="ms-2">
									<span class="fw-bold d-block" style="font-size:11px !important">{{Auth::user()->name}} {{Auth::user()->l_name}} ({{Auth::user()->c_name}})</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
